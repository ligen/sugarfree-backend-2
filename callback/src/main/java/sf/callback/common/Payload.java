package sf.callback.common;

public class Payload {

	private Object object;

	public Payload(Object object) {
		this.object = object;
	}

	public Object getObject() {
		return object;
	}

}
