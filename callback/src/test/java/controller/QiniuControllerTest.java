package controller;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import sf.callback.controller.QiniuController;
import sf.core.entity.TfImage;
import sf.core.mapper.TfImageMapper;
import sf.core.mapper.TuMemberMapper;
import sf.core.service.StorageService;

@RunWith(MockitoJUnitRunner.class)
public class QiniuControllerTest {

	@Mock
	TfImageMapper imageMapper;

	@Mock
	TuMemberMapper memberMapper;

	@Mock
	StorageService storageService;

	@InjectMocks
	QiniuController controller;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void uploadImage() throws Exception {

		String key = "test.png";
		String type = "1";
		String xid = "1";

		mockMvc.perform(
				MockMvcRequestBuilders.post("/qiniu/image").param("key", key).param("type", type).param("xid", xid))
				.andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

		verify(imageMapper).insert(any(TfImage.class));

		// Echo echo = new Gson().fromJson(response, Echo.class);
		// assertTrue(fetchKey.equals(echo.getKey()));
		// assertTrue(echo.getPayload().getImage().getUrl().endsWith(fetchKey));
	}

	@Test
	public void uploadAvatar() throws Exception {
		String key = "test.png";
		char type = 'M';
		int xid = 1;

		mockMvc.perform(MockMvcRequestBuilders.post("/qiniu/avatar").param("key", key)
				.param("type", String.valueOf(type)).param("xid", String.valueOf(xid))).andExpect(status().isOk())
				.andReturn().getResponse().getContentAsString();

		// verify(memberMapper).updateAvatar(1,
		// StorageService.DOMAIN_QINIU_MEDIA + String.join("_",
		// String.valueOf(type), String.valueOf(xid), key));
		// verify(storageService).deleteRemoteAvatar(type, xid);
	}
}
