package controller;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import sf.callback.controller.PingxxController;
import sf.core.entity.TfOrder;
import sf.core.entity.TuMember;
import sf.core.mapper.TfOrderMapper;
import sf.core.mapper.TuMemberMapper;

@RunWith(MockitoJUnitRunner.class)
public class PingxxControllerTest {
	
	@Mock
	TuMemberMapper memberMapper;
	
	@Mock TfOrderMapper orderMapper;
	
	@InjectMocks PingxxController controller;
	
	private MockMvc mockMvc;  
	
	@Before
	public void setUp(){
		mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}
	
	@Test
	public void notify_success() throws Exception {
		
		TuMember member = new TuMember();
		member.setId(8888);
		when(memberMapper.selectByPrimaryKey(anyInt())).thenReturn(member);
		
		TfOrder order = new TfOrder();
		order.setMemberid(member.getId());
		order.setBeans(1);
		when(orderMapper.selectByPrimaryKey(anyString())).thenReturn(order);
		
		byte[] body = IOUtils.toByteArray(this.getClass().getResourceAsStream("/charge"));
		
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.
				post("/pingxx")
				.contentType(MediaType.APPLICATION_JSON)
				.header("x-pingplusplus-signature", 
						IOUtils.toString(this.getClass().getResourceAsStream("/sign.right")))
				.content(body);
		
		mockMvc.perform(requestBuilder)
				.andExpect(status().isOk())
				.andReturn()
				.getResponse()
				.getContentAsString();
	}
	
	@Test
	public void notify_with_wrong_signature() throws Exception {
		byte[] body = IOUtils.toByteArray(this.getClass().getResourceAsStream("/charge"));
		
		MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.
				post("/pingxx")
				.contentType(MediaType.APPLICATION_JSON)
				.header("x-pingplusplus-signature", 
						IOUtils.toString(this.getClass().getResourceAsStream("/sign.wrong")))
				.content(body);
		mockMvc.perform(requestBuilder).andExpect(status().isInternalServerError());
	}
	
	@Test
	public void testName() throws Exception {
		System.out.println((long)1<<31);
	}
	
}
