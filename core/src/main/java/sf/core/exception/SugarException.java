package sf.core.exception;

import javax.servlet.http.HttpServletResponse;

public class SugarException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int code;

	private String message;

	public SugarException(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public SugarException(String message) {
		this(HttpServletResponse.SC_BAD_REQUEST, message);
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
