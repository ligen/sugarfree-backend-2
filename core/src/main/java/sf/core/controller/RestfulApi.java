package sf.core.controller;

import java.util.Collection;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ligen on 15-11-30.
 */
public interface RestfulApi<T> {

	@RequestMapping(value = "", method = RequestMethod.GET)
	Collection<T> get();

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	T get(@PathVariable Integer id);

	@RequestMapping(value = "", method = RequestMethod.POST)
	T post(@RequestBody T entity);

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	T post(@PathVariable Integer id, @RequestBody T entity);

}
