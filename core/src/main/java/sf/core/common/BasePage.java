package sf.core.common;

import java.io.Serializable;

/**
 * 基础分页
 * 
 * @author wb
 *
 */
public class BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* 请求页数 */
	private int page;

	/* 每页大小 */
	private int limit;

	/* 分页总数 */
	private long totalSize;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

}
