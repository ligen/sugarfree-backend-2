package sf.core.common;

import java.util.Map;

public class RedisMessageEntity {
	public static final int BOOK_PRIVATE_LESSON_SUCCESS = 1;// 约课成功消息(私教
	public static final int BOOK_GROUP_LESSON_SUCCESS = 2;// 约课成功消息(团课)
	public static final int CANCEL_LESSON_SUCCESS_BY_MEMBER = 3;// 取消课程消息(学员取消)
	public static final int CANCEL_LESSON_SUCCESS_BY_GYM = 4;// 取消课程消息(工作室取消)
	public static final int CANCEL_LESSON_SUCCESS_BY_COACH = 5;// 取消课程消息(教练取消)
	public static final int VERIFY_LESSON_SUCCESS = 6;// 签到消息
	public static final int COMMENT_SUCCESS = 7;// 提交评价
	public static final int NEET_TO_COMMENT = 8;// 未提交评价
	public static final int MEMBER_TOO_LONG_NOT_USE_APP = 9;// 用户5天未操作APP时推送
	public static final int MEMBER_LOGIN_SUCCESS = 10;// 用户登录app
	public static final int MEMBER_LOGIN_IN_OTHER_PLACE = 11;// 用户异地登陆
	public static final int REMIND_MEMBER_TO_LESSON = 12;// 开课前两小时提醒
	public static final int MEMBER_SUGGEST_SUCCESS = 13;// 用户提交意见反馈
	public static final int PAY_SUCCESS = 14;// 充值糖豆消息
	public static final int GET_COUPON_SUCCESS = 15;// 领取*场馆体验豆消息
	public static final int DIY_MESSAGE_FOR_MEMBER = 16;// 无糖每日一推
	public static final int COACH_TOO_LONG_NOT_USE_APP = 17;// 教练5天未操作APP时推送
	public static final int COACH_LOGIN_SUCCESS = 18;// 教练登录app
	public static final int COACH_LOGIN_IN_OTHER_PLACE = 19;// 教练异地登陆
	public static final int COACH_SUGGEST_SUCCESS = 20;// 教练反馈成功
	public static final int BIND_GYM_SUCCESS = 21;// 绑定工作室成功
	public static final int CANCEL_LESSON_BY_GYM = 22;// 工作室端取消课程
	public static final int BOOK_PACKAGE_LESSON_SUCCESS = 23;// 预约课程包成功消息

	public static final String ENTITY_RECEIPT = "receipt";// receipt id
	public static final String ENTITY_MEMBER = "member";// member id
	public static final String ENTITY_ORDER = "order";// order id
	public static final String ENTITY_COUPON = "coupon";// coupon id
	public static final String ENTITY_DIY_MESSAGE = "diy_message";// 每日一推消息
	public static final String ENTITY_COACH = "coach";// coach id
	public static final String ENTITY_PAKCAGE_LESSON = "package_lesson";// packageLesson
																		// id
	public static final String ENTITY_DEVICE_TOKEN = "device_token";
	public static final String ENTITY_GYM = "gym";// gym id
	public static final String ENTITY_LESSON = "lesson";// lesson id
	public static final String ENTITY_PRIVATE_PACKAGE = "pri_package";// pri_package

	private Map<String, Object> entities;
	private int messageType;

	public RedisMessageEntity(Map<String, Object> entities, int messageType) {
		this.entities = entities;
		this.messageType = messageType;
	}

	public RedisMessageEntity() {
	}

	public Map<String, Object> getEntities() {
		return entities;
	}

	public void setEntities(Map<String, Object> entities) {
		this.entities = entities;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
}
