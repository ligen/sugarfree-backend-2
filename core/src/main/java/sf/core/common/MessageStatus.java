package sf.core.common;

public class MessageStatus {

	public static final int ENTITY_TYPE_RECEIPT = 1;
	public static final int ENTITY_TYPE_LESSON = 2;
	public static final int ENTITY_TYPE_COACH = 3;
	public static final int ENTITY_TYPE_PACKAGE_LESSON = 4;

	public static int TYPE_MEMBER = 1;
	public static int TYPE_COACH = 2;
	public static int TYPE_GYM = 3;
	public static int TYPE_ADMIN = 4;

	public static int MESSAGE_TYPE_SYS = 2;
	public static int MESSAGE_TYPE_USER = 1;

	/* tu_admin service user */
	public static int SERVICE_USERID_SYSTEM = 5;
	public static int SERVICE_USERID_BOOKED = 6;
	public static int SERVICE_USERID_VERIFIED = 7;

}
