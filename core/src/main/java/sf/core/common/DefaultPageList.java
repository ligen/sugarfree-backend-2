package sf.core.common;

import java.io.Serializable;
import java.util.List;

/**
 * 携带分页数据集合
 * 
 * @author wb
 *
 * @param <T>
 */
public class DefaultPageList<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BasePage page;

	private List<T> list;

	public DefaultPageList(BasePage page, List<T> list) {
		super();
		this.page = page;
		this.list = list;
	}

	public DefaultPageList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BasePage getPage() {
		return page;
	}

	public void setPage(BasePage page) {
		this.page = page;
	}

	public List<T> getList() {
		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
	}

}
