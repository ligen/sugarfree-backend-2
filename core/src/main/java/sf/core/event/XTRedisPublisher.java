package sf.core.event;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import sf.core.common.RedisMessageEntity;
import sf.core.entity.TuMember;
import sf.core.mapper.TuMemberMapper;

@Service
public class XTRedisPublisher {
	Logger logger = LoggerFactory.getLogger(XTRedisPublisher.class);

	public static final String CHANNEL = "sugar_free_message";

	@Autowired
	StringRedisTemplate redisTemplate;

	@Autowired
	private TuMemberMapper memberMapper;

	public void sendMessage(RedisMessageEntity redisMessage) {
		Gson gson = new Gson();
		String channel = CHANNEL;
		String message = gson.toJson(redisMessage);
		redisTemplate.convertAndSend(channel, message);
		logger.debug("message = " + message);
	}

	public void sendPrivateBookLessonSuccessMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.BOOK_PRIVATE_LESSON_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendGroupeBookLessonSuccessMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.BOOK_GROUP_LESSON_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendCancelLessonByMemberMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.CANCEL_LESSON_SUCCESS_BY_MEMBER);
		sendMessage(redisMessage);
	}

	public void sendCancelLessonByGymMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.CANCEL_LESSON_SUCCESS_BY_GYM);
		sendMessage(redisMessage);
	}

	public void sendCancelLessonByGym(Integer lessonId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_LESSON, lessonId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.CANCEL_LESSON_BY_GYM);
		sendMessage(redisMessage);
	}

	public void sendCancelLessonByCoachMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.CANCEL_LESSON_SUCCESS_BY_COACH);
		sendMessage(redisMessage);
	}

	public void sendVerifyLessonSuccessMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.VERIFY_LESSON_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendCommentSuccessMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.COMMENT_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendNeedToCommentMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.NEET_TO_COMMENT);
		sendMessage(redisMessage);
	}

	public void sendMemberTooLongNotUseAPPMessage(Integer memberId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_MEMBER, memberId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.MEMBER_TOO_LONG_NOT_USE_APP);
		sendMessage(redisMessage);
	}

	public void sendMemberLoginSuccessMessage(Integer memberId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_MEMBER, memberId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.MEMBER_LOGIN_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendMemberLoginInOtherPlaceMessage(String deviceToken) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_DEVICE_TOKEN, deviceToken);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.MEMBER_LOGIN_IN_OTHER_PLACE);
		sendMessage(redisMessage);
	}

	public void sendRemindMemberToLessonMessage(Integer receiptId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_RECEIPT, receiptId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.REMIND_MEMBER_TO_LESSON);
		sendMessage(redisMessage);
	}

	public void sendMemberSuggestMessageMessage(Integer memberId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_MEMBER, memberId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.MEMBER_SUGGEST_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendPaySuccessMessage(Integer memberId, Integer orderId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_MEMBER, memberId);
		entities.put(RedisMessageEntity.ENTITY_ORDER, orderId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.PAY_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendGetCouponSuccessMessage(Integer memberId, Integer couponId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_MEMBER, memberId);
		entities.put(RedisMessageEntity.ENTITY_COUPON, couponId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.GET_COUPON_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendMemberDiyMessage(String diyMessage) {
		List<TuMember> members = memberMapper.selectAll();
		for (TuMember member : members) {
			Map<String, Object> entities = new HashMap<String, Object>();
			entities.put(RedisMessageEntity.ENTITY_MEMBER, member.getId());
			entities.put(RedisMessageEntity.ENTITY_DIY_MESSAGE, diyMessage);
			RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
					RedisMessageEntity.DIY_MESSAGE_FOR_MEMBER);
			sendMessage(redisMessage);
		}
	}

	public void sendCoachSuggestMessageMessage(Integer coachId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_COACH, coachId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.MEMBER_SUGGEST_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendCoachBindGymSucessMessage(Integer gymId, Integer coachId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_GYM, gymId);
		entities.put(RedisMessageEntity.ENTITY_COACH, coachId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.BIND_GYM_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendCoachLoginSuccessMessage(Integer coachId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_COACH, coachId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities, RedisMessageEntity.COACH_LOGIN_SUCCESS);
		sendMessage(redisMessage);
	}

	public void sendCoachTooLongNotUseAPPMessage(Integer coachId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_COACH, coachId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.COACH_TOO_LONG_NOT_USE_APP);
		sendMessage(redisMessage);
	}

	public void sendCoachLoginInOtherPlaceMessage(String deviceToken) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_DEVICE_TOKEN, deviceToken);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.COACH_LOGIN_IN_OTHER_PLACE);
		sendMessage(redisMessage);
	}

	public void sendPackageLessonBookSuccessMessage(Integer packageLessonId, Integer memberId) {
		Map<String, Object> entities = new HashMap<String, Object>();
		entities.put(RedisMessageEntity.ENTITY_PAKCAGE_LESSON, packageLessonId);
		entities.put(RedisMessageEntity.ENTITY_MEMBER, memberId);
		RedisMessageEntity redisMessage = new RedisMessageEntity(entities,
				RedisMessageEntity.BOOK_PACKAGE_LESSON_SUCCESS);
		sendMessage(redisMessage);
	}
}
