package sf.core.event;

import sf.core.entity.TfReceipt;

/**
 * Created by ligen on 15-10-30.
 */
public interface SugarEvent {

	String RECEIPT_STATUS_CHANGED = "receipt_status_changed";

	void receipt_status_changed(TfReceipt receipt);
}
