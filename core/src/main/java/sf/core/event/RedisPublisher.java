package sf.core.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import sf.core.entity.TfReceipt;

/**
 * Created by ligen on 15-10-30.
 */

@Service
public class RedisPublisher implements SugarEvent {

	@Autowired
	private StringRedisTemplate redisTemplate;

	public void receipt_status_changed(TfReceipt receipt) {
		Gson gson = new Gson();
		redisTemplate.convertAndSend(RECEIPT_STATUS_CHANGED, gson.toJson(receipt));
	}
}
