package sf.core.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

public class NumberUtil {

	public final static double ONE_HUNDRED_DOUBLE = 100.0;
	public final static long ONE_HUNDRED_LONG = 100;
	public final static int ONE_HUNDRED_INT = 100;

	private final static int LEN = 30;

	public static String int2bits(long value) {
		List<Long> list = new ArrayList<Long>();

		for (int i = LEN; i >= 0; i--) {

			long num = 1 << i;

			if ((value & num) > 0) {

				list.add(num);
			}
		}

		return StringUtils.join(list, ',');
	}

	public static String generateSerailNumber() {

		return (DateUtil.format(new Date(), DateUtil.millisecond_formatter) + RandomStringUtils.randomNumeric(4));
	}

}
