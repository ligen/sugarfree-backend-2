package sf.core.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static class Period {

		Date begin;
		Date end;

		public Period(Date begin, Date end) {
			super();
			this.begin = begin;
			this.end = end;
		}

		public Date getBegin() {
			return begin;
		}

		public Date getEnd() {
			return end;
		}
	}

	public static DateFormat default_formatter = new SimpleDateFormat("yyyyMMddHHmm");
	public static DateFormat unix_formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static DateFormat millisecond_formatter = new SimpleDateFormat("yyMMddHHmmssSSS");
	public static DateFormat second_formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	public static DateFormat readable_formatter = new SimpleDateFormat("MM月dd日HH:mm");
	public static DateFormat simpledate_formatter = new SimpleDateFormat("yyyyMMdd");

	private static Calendar calendar = Calendar.getInstance();

	public final static String TIME_SEPARATE = ":";

	public static String format(Date date) {
		return default_formatter.format(date);
	}

	public static String format(Date date, DateFormat formatter) {
		return formatter.format(date);
	}

	public static Date parse(String date) throws ParseException {
		return default_formatter.parse(date);
	}

	public static Date parse(String date, DateFormat format) throws ParseException {
		return format.parse(date);
	}

	public static Date add(Date date, int field, int amount) {
		calendar.setTime(date);
		calendar.add(field, amount);
		return calendar.getTime();
	}

	public static int diff(Date begin, Date end, int field) {
		calendar.setTime(begin);
		int fieldBegin = calendar.get(field);
		calendar.setTime(end);
		int fieldEnd = calendar.get(field);
		return fieldEnd - fieldBegin;
	}

	public static Date set(Date date, int field, int value) {
		calendar.setTime(date);
		calendar.set(field, value);
		return calendar.getTime();
	}

	public static long now() {
		return System.currentTimeMillis() / 1000;
	}

	public static Date parse(int dayOfWeek, int minuteOfDay, int whichWeek) {
		calendar.setTime(new Date());
		int dayOfWeekNow = calendar.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == 1) {
			dayOfWeek = 8;
		}
		if (dayOfWeekNow == 1) {
			dayOfWeekNow = 8;
		}
		int dayDiff = dayOfWeek - dayOfWeekNow + 7 * whichWeek;
		calendar.add(Calendar.DAY_OF_YEAR, dayDiff);
		calendar.set(Calendar.HOUR_OF_DAY, minuteOfDay / 60);
		calendar.set(Calendar.MINUTE, minuteOfDay % 60);
		return calendar.getTime();
	}

	public static Period getPeriodOfToday(Date beginTime, int durationInMinute, Date earliest) throws ParseException {
		Date begin = beginTime;
		Date end = DateUtil.add(begin, Calendar.MINUTE, durationInMinute);

		if (earliest.after(beginTime)) {
			begin = earliest;
		} else {
			begin = beginTime;
		}

		if (DateUtil.diff(begin, end, Calendar.DAY_OF_YEAR) != 0) {
			end = endOfDay(begin);
		}

		return new Period(begin, end);
	}

	public static Date beginOfDay(Date date) {
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.MINUTE, 1);
		return calendar.getTime();
	}

	public static Date endOfDay(Date date) {
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	public static Date getNextDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, +1);
		date = calendar.getTime();
		return date;
	}

	public static int getDayOfWeek(Date date) {
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	public static int getHourOfDay(Date date) {
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinuteOfDay(Date date) {
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}

	public static int getNextDayOfWeek(Date date, int next) {
		calendar.setTime(date);
		calendar.add(Calendar.DATE, next);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}

	public static String diyFormat(Date date, String pattern) {
		DateFormat sf = new SimpleDateFormat(pattern);
		return sf.format(date);
	}

	public static Date diyParse(String date, String pattern) {
		DateFormat sf = new SimpleDateFormat(pattern);
		try {
			return sf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Date addHours(Date sourseDate, int hours) {
		calendar.setTime(sourseDate);
		calendar.add(Calendar.HOUR_OF_DAY, hours);
		return calendar.getTime();
	}

	public static Date getFirstDayOfWeek(Date date) {
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (dayOfWeek == 0) {
			dayOfWeek = 7;
		}
		calendar.add(Calendar.DATE, -dayOfWeek + 1);
		return calendar.getTime();
	}

	public static Date getLastDayOfWeek(Date date) {
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;
		if (dayOfWeek == 0) {
			dayOfWeek = 7;
		}
		calendar.add(Calendar.DATE, -dayOfWeek + 7);
		return calendar.getTime();
	}
}
