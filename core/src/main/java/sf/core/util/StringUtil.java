package sf.core.util;

import java.util.UUID;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

public class StringUtil {

	public static String hashPwd(String password, String salt) {
		return DigestUtils.md5Hex(password + salt);
	}

	public static String uuid() {
		return StringUtils.join(UUID.randomUUID().toString().split("-")).toUpperCase();
	}
}
