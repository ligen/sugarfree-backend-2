package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_package")
public class TfPackage {
	/**
	 * 编号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 场馆编号
	 */
	@Column(name = "gymId")
	private Integer gymid;

	/**
	 * 教练Id
	 */
	@Column(name = "coachId")
	private Integer coachid;

	/**
	 * 课程包类型(1,训练营 2,私教包)
	 */
	@Column(name = "classType")
	private Integer classtype;

	/**
	 * 课程类型
	 */
	@Column(name = "sportType")
	private Integer sporttype;

	/**
	 * 课程包标题
	 */
	private String title;

	/**
	 * 课程包描述
	 */
	private String description;

	/**
	 * 价格
	 */
	@Column(name = "priceInt")
	private Integer priceint;

	/**
	 * 节数
	 */
	private Integer lessons;

	/**
	 * 人数上限
	 */
	@Column(name = "numberLimit")
	private Integer numberlimit;

	/**
	 * 实际人数
	 */
	@Column(name = "realNumber")
	private Integer realnumber;

	/**
	 * 报名开始时间
	 */
	@Column(name = "beginTime")
	private Date begintime;

	/**
	 * 报名截止时间
	 */
	@Column(name = "endTime")
	private Date endtime;

	/**
	 * LOGO
	 */
	private String logo;

	/**
	 * 分享URL
	 */
	@Column(name = "shareUrl")
	private String shareurl;

	/**
	 * 获取编号
	 *
	 * @return id - 编号
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置编号
	 *
	 * @param id
	 *            编号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取场馆编号
	 *
	 * @return gymId - 场馆编号
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * 设置场馆编号
	 *
	 * @param gymid
	 *            场馆编号
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	/**
	 * 获取教练Id
	 *
	 * @return coachId - 教练Id
	 */
	public Integer getCoachid() {
		return coachid;
	}

	/**
	 * 设置教练Id
	 *
	 * @param coachid
	 *            教练Id
	 */
	public void setCoachid(Integer coachid) {
		this.coachid = coachid;
	}

	/**
	 * 获取课程包类型(1,训练营 2,私教包)
	 *
	 * @return classType - 课程包类型(1,训练营 2,私教包)
	 */
	public Integer getClasstype() {
		return classtype;
	}

	/**
	 * 设置课程包类型(1,训练营 2,私教包)
	 *
	 * @param classtype
	 *            课程包类型(1,训练营 2,私教包)
	 */
	public void setClasstype(Integer classtype) {
		this.classtype = classtype;
	}

	/**
	 * 获取课程类型
	 *
	 * @return sportType - 课程类型
	 */
	public Integer getSporttype() {
		return sporttype;
	}

	/**
	 * 设置课程类型
	 *
	 * @param sporttype
	 *            课程类型
	 */
	public void setSporttype(Integer sporttype) {
		this.sporttype = sporttype;
	}

	/**
	 * 获取课程包标题
	 *
	 * @return title - 课程包标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置课程包标题
	 *
	 * @param title
	 *            课程包标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取课程包描述
	 *
	 * @return description - 课程包描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置课程包描述
	 *
	 * @param description
	 *            课程包描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取价格
	 *
	 * @return priceInt - 价格
	 */
	public Integer getPriceint() {
		return priceint;
	}

	/**
	 * 设置价格
	 *
	 * @param priceint
	 *            价格
	 */
	public void setPriceint(Integer priceint) {
		this.priceint = priceint;
	}

	/**
	 * 获取节数
	 *
	 * @return lessons - 节数
	 */
	public Integer getLessons() {
		return lessons;
	}

	/**
	 * 设置节数
	 *
	 * @param lessons
	 *            节数
	 */
	public void setLessons(Integer lessons) {
		this.lessons = lessons;
	}

	/**
	 * 获取人数上限
	 *
	 * @return numberLimit - 人数上限
	 */
	public Integer getNumberlimit() {
		return numberlimit;
	}

	/**
	 * 设置人数上限
	 *
	 * @param numberlimit
	 *            人数上限
	 */
	public void setNumberlimit(Integer numberlimit) {
		this.numberlimit = numberlimit;
	}

	/**
	 * 获取实际人数
	 *
	 * @return realNumber - 实际人数
	 */
	public Integer getRealnumber() {
		return realnumber;
	}

	/**
	 * 设置实际人数
	 *
	 * @param realnumber
	 *            实际人数
	 */
	public void setRealnumber(Integer realnumber) {
		this.realnumber = realnumber;
	}

	/**
	 * 获取报名开始时间
	 *
	 * @return beginTime - 报名开始时间
	 */
	public Date getBegintime() {
		return begintime;
	}

	/**
	 * 设置报名开始时间
	 *
	 * @param begintime
	 *            报名开始时间
	 */
	public void setBegintime(Date begintime) {
		this.begintime = begintime;
	}

	/**
	 * 获取报名截止时间
	 *
	 * @return endTime - 报名截止时间
	 */
	public Date getEndtime() {
		return endtime;
	}

	/**
	 * 设置报名截止时间
	 *
	 * @param endtime
	 *            报名截止时间
	 */
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	/**
	 * 获取LOGO
	 *
	 * @return logo - LOGO
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * 设置LOGO
	 *
	 * @param logo
	 *            LOGO
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * 获取分享URL
	 *
	 * @return shareUrl - 分享URL
	 */
	public String getShareurl() {
		return shareurl;
	}

	/**
	 * 设置分享URL
	 *
	 * @param shareurl
	 *            分享URL
	 */
	public void setShareurl(String shareurl) {
		this.shareurl = shareurl;
	}
}