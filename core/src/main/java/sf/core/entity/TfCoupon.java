package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_coupon")
public class TfCoupon {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "giftId")
	private Integer giftid;

	@Column(name = "gymId")
	private Integer gymid;

	@Column(name = "beansInt")
	private Integer beansint;

	@Column(name = "memberId")
	private Integer memberid;

	private Byte status;

	@Column(name = "receivedTime")
	private Date receivedtime;

	@Column(name = "expiredTime")
	private Date expiredtime;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return giftId
	 */
	public Integer getGiftid() {
		return giftid;
	}

	/**
	 * @param giftid
	 */
	public void setGiftid(Integer giftid) {
		this.giftid = giftid;
	}

	/**
	 * @return gymId
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * @param gymid
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	/**
	 * @return beansInt
	 */
	public Integer getBeansint() {
		return beansint;
	}

	/**
	 * @param beansint
	 */
	public void setBeansint(Integer beansint) {
		this.beansint = beansint;
	}

	/**
	 * @return memberId
	 */
	public Integer getMemberid() {
		return memberid;
	}

	/**
	 * @param memberid
	 */
	public void setMemberid(Integer memberid) {
		this.memberid = memberid;
	}

	/**
	 * @return status
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * @return receivedTime
	 */
	public Date getReceivedtime() {
		return receivedtime;
	}

	/**
	 * @param receivedtime
	 */
	public void setReceivedtime(Date receivedtime) {
		this.receivedtime = receivedtime;
	}

	/**
	 * @return expiredTime
	 */
	public Date getExpiredtime() {
		return expiredtime;
	}

	/**
	 * @param expiredtime
	 */
	public void setExpiredtime(Date expiredtime) {
		this.expiredtime = expiredtime;
	}
}