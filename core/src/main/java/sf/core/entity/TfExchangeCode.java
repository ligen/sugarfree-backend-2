package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_exchange_code")
public class TfExchangeCode {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private String code;

	private Byte status;

	@Column(name = "memberId")
	private Integer memberid;

	@Column(name = "beansInt")
	private Integer beansint;

	@Column(name = "launchTime")
	private Date launchtime;

	@Column(name = "expiredTime")
	private Date expiredtime;

	@Column(name = "receivedTime")
	private Date receivedtime;

	@Column(name = "validPeriod")
	private Integer validperiod;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return status
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * @return memberId
	 */
	public Integer getMemberid() {
		return memberid;
	}

	/**
	 * @param memberid
	 */
	public void setMemberid(Integer memberid) {
		this.memberid = memberid;
	}

	/**
	 * @return beansInt
	 */
	public Integer getBeansint() {
		return beansint;
	}

	/**
	 * @param beansint
	 */
	public void setBeansint(Integer beansint) {
		this.beansint = beansint;
	}

	/**
	 * @return launchTime
	 */
	public Date getLaunchtime() {
		return launchtime;
	}

	/**
	 * @param launchtime
	 */
	public void setLaunchtime(Date launchtime) {
		this.launchtime = launchtime;
	}

	/**
	 * @return expiredTime
	 */
	public Date getExpiredtime() {
		return expiredtime;
	}

	/**
	 * @param expiredtime
	 */
	public void setExpiredtime(Date expiredtime) {
		this.expiredtime = expiredtime;
	}

	/**
	 * @return receivedTime
	 */
	public Date getReceivedtime() {
		return receivedtime;
	}

	/**
	 * @param receivedtime
	 */
	public void setReceivedtime(Date receivedtime) {
		this.receivedtime = receivedtime;
	}

	/**
	 * @return validPeriod
	 */
	public Integer getValidperiod() {
		return validperiod;
	}

	/**
	 * @param validperiod
	 */
	public void setValidperiod(Integer validperiod) {
		this.validperiod = validperiod;
	}
}