package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_course")
public class TfCourse {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "classType")
	private Byte classtype;

	@Column(name = "sportType")
	private Integer sporttype;

	@Column(name = "gymId")
	private Integer gymid;

	@Column(name = "coachId")
	private Integer coachid;

	@Column(name = "priceInt")
	private Integer priceint;

	@Column(name = "numberLimit")
	private Integer numberlimit;

	private String title;

	private String description;

	private Byte rate;

	private Byte deleted;

	@Column(name = "commentCount")
	private Integer commentcount;

	private Date createtime;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return classType
	 */
	public Byte getClasstype() {
		return classtype;
	}

	/**
	 * @param classtype
	 */
	public void setClasstype(Byte classtype) {
		this.classtype = classtype;
	}

	/**
	 * @return sportType
	 */
	public Integer getSporttype() {
		return sporttype;
	}

	/**
	 * @param sporttype
	 */
	public void setSporttype(Integer sporttype) {
		this.sporttype = sporttype;
	}

	/**
	 * @return gymId
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * @param gymid
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	/**
	 * @return coachId
	 */
	public Integer getCoachid() {
		return coachid;
	}

	/**
	 * @param coachid
	 */
	public void setCoachid(Integer coachid) {
		this.coachid = coachid;
	}

	/**
	 * @return priceInt
	 */
	public Integer getPriceint() {
		return priceint;
	}

	/**
	 * @param priceint
	 */
	public void setPriceint(Integer priceint) {
		this.priceint = priceint;
	}

	/**
	 * @return numberLimit
	 */
	public Integer getNumberlimit() {
		return numberlimit;
	}

	/**
	 * @param numberlimit
	 */
	public void setNumberlimit(Integer numberlimit) {
		this.numberlimit = numberlimit;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return rate
	 */
	public Byte getRate() {
		return rate;
	}

	/**
	 * @param rate
	 */
	public void setRate(Byte rate) {
		this.rate = rate;
	}

	/**
	 * @return deleted
	 */
	public Byte getDeleted() {
		return deleted;
	}

	/**
	 * @param deleted
	 */
	public void setDeleted(Byte deleted) {
		this.deleted = deleted;
	}

	/**
	 * @return commentCount
	 */
	public Integer getCommentcount() {
		return commentcount;
	}

	/**
	 * @param commentcount
	 */
	public void setCommentcount(Integer commentcount) {
		this.commentcount = commentcount;
	}

	/**
	 * @return createtime
	 */
	public Date getCreatetime() {
		return createtime;
	}

	/**
	 * @param createtime
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
}