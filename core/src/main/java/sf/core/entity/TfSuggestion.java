package sf.core.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_suggestion")
public class TfSuggestion {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Integer uid;

	private String content;

	@Column(name = "createdTime")
	private String createdtime;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return uid
	 */
	public Integer getUid() {
		return uid;
	}

	/**
	 * @param uid
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
	 * @return content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return createdTime
	 */
	public String getCreatedtime() {
		return createdtime;
	}

	/**
	 * @param createdtime
	 */
	public void setCreatedtime(String createdtime) {
		this.createdtime = createdtime;
	}
}