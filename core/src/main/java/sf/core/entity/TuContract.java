package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tu_contract")
public class TuContract {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "coachId")
	private Integer coachid;

	private Byte status;

	@Column(name = "coachType")
	private Byte coachtype;

	@Column(name = "createdTime")
	private Date createdtime;

	@Column(name = "gymId")
	private Integer gymid;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return coachId
	 */
	public Integer getCoachid() {
		return coachid;
	}

	/**
	 * @param coachid
	 */
	public void setCoachid(Integer coachid) {
		this.coachid = coachid;
	}

	/**
	 * @return status
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * @return coachType
	 */
	public Byte getCoachtype() {
		return coachtype;
	}

	/**
	 * @param coachtype
	 */
	public void setCoachtype(Byte coachtype) {
		this.coachtype = coachtype;
	}

	/**
	 * @return createdTime
	 */
	public Date getCreatedtime() {
		return createdtime;
	}

	/**
	 * @param createdtime
	 */
	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	/**
	 * @return gymId
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * @param gymid
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}
}