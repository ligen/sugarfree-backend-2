package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_lesson")
public class TfLesson {
	/**
	 * id
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 课程标题
	 */
	private String title;

	/**
	 * 课程描述
	 */
	private String description;

	/**
	 * 开始时间
	 */
	@Column(name = "beginTime")
	private Date begintime;

	/**
	 * 截止时间
	 */
	@Column(name = "endTime")
	private Date endtime;

	/**
	 * 课程Id
	 */
	@Column(name = "courseId")
	private Integer courseid;

	/**
	 * 课程包Id
	 */
	@Column(name = "packageId")
	private Integer packageid;

	/**
	 * 价格(私交包)
	 */
	private Integer price;

	/**
	 * 节数(私交包)
	 */
	private Integer lessons;

	/**
	 * 图片
	 */
	private String image;

	/**
	 * 获取id
	 *
	 * @return id - id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置id
	 *
	 * @param id
	 *            id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取课程标题
	 *
	 * @return title - 课程标题
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置课程标题
	 *
	 * @param title
	 *            课程标题
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取课程描述
	 *
	 * @return description - 课程描述
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * 设置课程描述
	 *
	 * @param description
	 *            课程描述
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * 获取开始时间
	 *
	 * @return beginTime - 开始时间
	 */
	public Date getBegintime() {
		return begintime;
	}

	/**
	 * 设置开始时间
	 *
	 * @param begintime
	 *            开始时间
	 */
	public void setBegintime(Date begintime) {
		this.begintime = begintime;
	}

	/**
	 * 获取截止时间
	 *
	 * @return endTime - 截止时间
	 */
	public Date getEndtime() {
		return endtime;
	}

	/**
	 * 设置截止时间
	 *
	 * @param endtime
	 *            截止时间
	 */
	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

	/**
	 * 获取课程Id
	 *
	 * @return courseId - 课程Id
	 */
	public Integer getCourseid() {
		return courseid;
	}

	/**
	 * 设置课程Id
	 *
	 * @param courseid
	 *            课程Id
	 */
	public void setCourseid(Integer courseid) {
		this.courseid = courseid;
	}

	/**
	 * 获取课程包Id
	 *
	 * @return packageId - 课程包Id
	 */
	public Integer getPackageid() {
		return packageid;
	}

	/**
	 * 设置课程包Id
	 *
	 * @param packageid
	 *            课程包Id
	 */
	public void setPackageid(Integer packageid) {
		this.packageid = packageid;
	}

	/**
	 * 获取价格(私交包)
	 *
	 * @return price - 价格(私交包)
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * 设置价格(私交包)
	 *
	 * @param price
	 *            价格(私交包)
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

	/**
	 * 获取节数(私交包)
	 *
	 * @return lessons - 节数(私交包)
	 */
	public Integer getLessons() {
		return lessons;
	}

	/**
	 * 设置节数(私交包)
	 *
	 * @param lessons
	 *            节数(私交包)
	 */
	public void setLessons(Integer lessons) {
		this.lessons = lessons;
	}

	/**
	 * 获取图片
	 *
	 * @return image - 图片
	 */
	public String getImage() {
		return image;
	}

	/**
	 * 设置图片
	 *
	 * @param image
	 *            图片
	 */
	public void setImage(String image) {
		this.image = image;
	}
}