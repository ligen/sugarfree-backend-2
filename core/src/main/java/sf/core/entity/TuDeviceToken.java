package sf.core.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tu_device_token")
public class TuDeviceToken {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Byte type;

	private Integer xid;

	@Column(name = "deviceToken")
	private String devicetoken;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return type
	 */
	public Byte getType() {
		return type;
	}

	/**
	 * @param type
	 */
	public void setType(Byte type) {
		this.type = type;
	}

	/**
	 * @return xid
	 */
	public Integer getXid() {
		return xid;
	}

	/**
	 * @param xid
	 */
	public void setXid(Integer xid) {
		this.xid = xid;
	}

	/**
	 * @return deviceToken
	 */
	public String getDevicetoken() {
		return devicetoken;
	}

	/**
	 * @param devicetoken
	 */
	public void setDevicetoken(String devicetoken) {
		this.devicetoken = devicetoken;
	}
}