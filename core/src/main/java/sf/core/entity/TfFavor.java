package sf.core.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_favor")
public class TfFavor {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Integer uid;

	private Byte type;

	private Integer xid;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return uid
	 */
	public Integer getUid() {
		return uid;
	}

	/**
	 * @param uid
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
	 * @return type
	 */
	public Byte getType() {
		return type;
	}

	/**
	 * @param type
	 */
	public void setType(Byte type) {
		this.type = type;
	}

	/**
	 * @return xid
	 */
	public Integer getXid() {
		return xid;
	}

	/**
	 * @param xid
	 */
	public void setXid(Integer xid) {
		this.xid = xid;
	}
}