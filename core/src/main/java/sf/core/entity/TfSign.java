package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_sign")
public class TfSign {
	/**
	 * 编号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 用户Id
	 */
	private Integer uid;

	/**
	 * 订单号
	 */
	private Integer rid;

	/**
	 * 签到码
	 */
	private Integer code;

	/**
	 * 签到时间
	 */
	@Column(name = "signDate")
	private Date signdate;

	/**
	 * 是否签到(0,否 1,签到)
	 */
	@Column(name = "isSign")
	private Integer issign;

	@Column(name = "lessonId")
	private Integer lessonid;

	/**
	 * 获取编号
	 *
	 * @return id - 编号
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置编号
	 *
	 * @param id
	 *            编号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取用户Id
	 *
	 * @return uid - 用户Id
	 */
	public Integer getUid() {
		return uid;
	}

	/**
	 * 设置用户Id
	 *
	 * @param uid
	 *            用户Id
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
	 * 获取订单号
	 *
	 * @return rid - 订单号
	 */
	public Integer getRid() {
		return rid;
	}

	/**
	 * 设置订单号
	 *
	 * @param rid
	 *            订单号
	 */
	public void setRid(Integer rid) {
		this.rid = rid;
	}

	/**
	 * 获取签到码
	 *
	 * @return code - 签到码
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * 设置签到码
	 *
	 * @param code
	 *            签到码
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * 获取签到时间
	 *
	 * @return signDate - 签到时间
	 */
	public Date getSigndate() {
		return signdate;
	}

	/**
	 * 设置签到时间
	 *
	 * @param signdate
	 *            签到时间
	 */
	public void setSigndate(Date signdate) {
		this.signdate = signdate;
	}

	/**
	 * 获取是否签到(0,否 1,签到)
	 *
	 * @return isSign - 是否签到(0,否 1,签到)
	 */
	public Integer getIssign() {
		return issign;
	}

	/**
	 * 设置是否签到(0,否 1,签到)
	 *
	 * @param issign
	 *            是否签到(0,否 1,签到)
	 */
	public void setIssign(Integer issign) {
		this.issign = issign;
	}

	/**
	 * @return lessonId
	 */
	public Integer getLessonid() {
		return lessonid;
	}

	/**
	 * @param lessonid
	 */
	public void setLessonid(Integer lessonid) {
		this.lessonid = lessonid;
	}
}