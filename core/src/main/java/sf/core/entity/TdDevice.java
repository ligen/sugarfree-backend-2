package sf.core.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "td_device")
public class TdDevice {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Integer value;

	private String name;

	private String thumbnail;

	private String darkthumbnail;

	private Byte status;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * @param value
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return thumbnail
	 */
	public String getThumbnail() {
		return thumbnail;
	}

	/**
	 * @param thumbnail
	 */
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	/**
	 * @return darkthumbnail
	 */
	public String getDarkthumbnail() {
		return darkthumbnail;
	}

	/**
	 * @param darkthumbnail
	 */
	public void setDarkthumbnail(String darkthumbnail) {
		this.darkthumbnail = darkthumbnail;
	}

	/**
	 * @return status
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}
}