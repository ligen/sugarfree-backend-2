package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tu_gym")
public class TuGym {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private String address;

	private String name;

	private Integer contactid;

	private String logo;

	@Column(name = "beansInt")
	private Integer beansint;

	@Column(name = "sportTypes")
	private Integer sporttypes;

	private Double lat;

	private Double lng;

	private Byte rate;

	@Column(name = "giftId")
	private Integer giftid;

	@Column(name = "diyId")
	private Integer diyid;

	@Column(name = "openTime")
	private Integer opentime;

	@Column(name = "closeTime")
	private Integer closetime;

	private String description;

	@Column(name = "regionId")
	private Integer regionid;

	private Integer devices;

	private String password;

	private String account;

	private String payaccount;

	private Byte status;

	@Column(name = "commentCount")
	private Integer commentcount;

	@Column(name = "likedCount")
	private Integer likedcount;

	@Column(name = "isSetHoliday")
	private Byte issetholiday;

	@Column(name = "holidayBeginDate")
	private Date holidaybegindate;

	@Column(name = "holidayEndDate")
	private Date holidayenddate;

	/**
	 * 分享URL
	 */
	@Column(name = "shareUrl")
	private String shareurl;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return contactid
	 */
	public Integer getContactid() {
		return contactid;
	}

	/**
	 * @param contactid
	 */
	public void setContactid(Integer contactid) {
		this.contactid = contactid;
	}

	/**
	 * @return logo
	 */
	public String getLogo() {
		return logo;
	}

	/**
	 * @param logo
	 */
	public void setLogo(String logo) {
		this.logo = logo;
	}

	/**
	 * @return beansInt
	 */
	public Integer getBeansint() {
		return beansint;
	}

	/**
	 * @param beansint
	 */
	public void setBeansint(Integer beansint) {
		this.beansint = beansint;
	}

	/**
	 * @return sportTypes
	 */
	public Integer getSporttypes() {
		return sporttypes;
	}

	/**
	 * @param sporttypes
	 */
	public void setSporttypes(Integer sporttypes) {
		this.sporttypes = sporttypes;
	}

	/**
	 * @return lat
	 */
	public Double getLat() {
		return lat;
	}

	/**
	 * @param lat
	 */
	public void setLat(Double lat) {
		this.lat = lat;
	}

	/**
	 * @return lng
	 */
	public Double getLng() {
		return lng;
	}

	/**
	 * @param lng
	 */
	public void setLng(Double lng) {
		this.lng = lng;
	}

	/**
	 * @return rate
	 */
	public Byte getRate() {
		return rate;
	}

	/**
	 * @param rate
	 */
	public void setRate(Byte rate) {
		this.rate = rate;
	}

	/**
	 * @return giftId
	 */
	public Integer getGiftid() {
		return giftid;
	}

	/**
	 * @param giftid
	 */
	public void setGiftid(Integer giftid) {
		this.giftid = giftid;
	}

	/**
	 * @return diyId
	 */
	public Integer getDiyid() {
		return diyid;
	}

	/**
	 * @param diyid
	 */
	public void setDiyid(Integer diyid) {
		this.diyid = diyid;
	}

	/**
	 * @return openTime
	 */
	public Integer getOpentime() {
		return opentime;
	}

	/**
	 * @param opentime
	 */
	public void setOpentime(Integer opentime) {
		this.opentime = opentime;
	}

	/**
	 * @return closeTime
	 */
	public Integer getClosetime() {
		return closetime;
	}

	/**
	 * @param closetime
	 */
	public void setClosetime(Integer closetime) {
		this.closetime = closetime;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return regionId
	 */
	public Integer getRegionid() {
		return regionid;
	}

	/**
	 * @param regionid
	 */
	public void setRegionid(Integer regionid) {
		this.regionid = regionid;
	}

	/**
	 * @return devices
	 */
	public Integer getDevices() {
		return devices;
	}

	/**
	 * @param devices
	 */
	public void setDevices(Integer devices) {
		this.devices = devices;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return payaccount
	 */
	public String getPayaccount() {
		return payaccount;
	}

	/**
	 * @param payaccount
	 */
	public void setPayaccount(String payaccount) {
		this.payaccount = payaccount;
	}

	/**
	 * @return status
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * @return commentCount
	 */
	public Integer getCommentcount() {
		return commentcount;
	}

	/**
	 * @param commentcount
	 */
	public void setCommentcount(Integer commentcount) {
		this.commentcount = commentcount;
	}

	/**
	 * @return likedCount
	 */
	public Integer getLikedcount() {
		return likedcount;
	}

	/**
	 * @param likedcount
	 */
	public void setLikedcount(Integer likedcount) {
		this.likedcount = likedcount;
	}

	/**
	 * @return isSetHoliday
	 */
	public Byte getIssetholiday() {
		return issetholiday;
	}

	/**
	 * @param issetholiday
	 */
	public void setIssetholiday(Byte issetholiday) {
		this.issetholiday = issetholiday;
	}

	/**
	 * @return holidayBeginDate
	 */
	public Date getHolidaybegindate() {
		return holidaybegindate;
	}

	/**
	 * @param holidaybegindate
	 */
	public void setHolidaybegindate(Date holidaybegindate) {
		this.holidaybegindate = holidaybegindate;
	}

	/**
	 * @return holidayEndDate
	 */
	public Date getHolidayenddate() {
		return holidayenddate;
	}

	/**
	 * @param holidayenddate
	 */
	public void setHolidayenddate(Date holidayenddate) {
		this.holidayenddate = holidayenddate;
	}

	/**
	 * 获取分享URL
	 *
	 * @return shareUrl - 分享URL
	 */
	public String getShareurl() {
		return shareurl;
	}

	/**
	 * 设置分享URL
	 *
	 * @param shareurl
	 *            分享URL
	 */
	public void setShareurl(String shareurl) {
		this.shareurl = shareurl;
	}
}