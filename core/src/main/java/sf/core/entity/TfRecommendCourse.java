package sf.core.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_recommend_course")
public class TfRecommendCourse {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Date createdtime;

	private Date modifytime;

	private String title;

	private String description;

	private String image2;

	private Byte position;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return createdtime
	 */
	public Date getCreatedtime() {
		return createdtime;
	}

	/**
	 * @param createdtime
	 */
	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	/**
	 * @return modifytime
	 */
	public Date getModifytime() {
		return modifytime;
	}

	/**
	 * @param modifytime
	 */
	public void setModifytime(Date modifytime) {
		this.modifytime = modifytime;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return image2
	 */
	public String getImage2() {
		return image2;
	}

	/**
	 * @param image2
	 */
	public void setImage2(String image2) {
		this.image2 = image2;
	}

	/**
	 * @return position
	 */
	public Byte getPosition() {
		return position;
	}

	/**
	 * @param position
	 */
	public void setPosition(Byte position) {
		this.position = position;
	}
}