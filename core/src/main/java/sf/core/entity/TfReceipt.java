package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_receipt")
public class TfReceipt {
	/**
	 * 订单号
	 */
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 课程包Id
	 */
	@Column(name = "packageId")
	private Integer packageid;

	/**
	 * 学员Id
	 */
	@Column(name = "memberId")
	private Integer memberid;

	/**
	 * 场馆Id
	 */
	@Column(name = "gymId")
	private Integer gymid;

	/**
	 * 订单状态(0,未支付,1,已支付,3,待上课,4,已完成.5已取消)
	 */
	private Integer status;

	/**
	 * 订单类型
	 */
	private Integer type;

	/**
	 * 价格
	 */
	private Integer number;

	/**
	 * 取消订单原因
	 */
	private String reason;

	/**
	 * 优惠券Id
	 */
	@Column(name = "couponId")
	private Integer couponid;

	/**
	 * 优惠券支付价格数量
	 */
	@Column(name = "paidInCouponInt")
	private Integer paidincouponint;

	/**
	 * 糖豆支付价格数量
	 */
	@Column(name = "paidInBeanInt")
	private Integer paidinbeanint;

	/**
	 * 创建时间
	 */
	@Column(name = "createTime")
	private Date createtime;

	/**
	 * 支付时间
	 */
	@Column(name = "paidTime")
	private Date paidtime;

	/**
	 * 节数
	 */
	private Integer lessons;

	/**
	 * 签到次数
	 */
	private Integer sign;

	@Column(name = "lessonId")
	private Integer lessonid;

	/**
	 * 获取订单号
	 *
	 * @return id - 订单号
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * 设置订单号
	 *
	 * @param id
	 *            订单号
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取课程包Id
	 *
	 * @return packageId - 课程包Id
	 */
	public Integer getPackageid() {
		return packageid;
	}

	/**
	 * 设置课程包Id
	 *
	 * @param packageid
	 *            课程包Id
	 */
	public void setPackageid(Integer packageid) {
		this.packageid = packageid;
	}

	/**
	 * 获取学员Id
	 *
	 * @return memberId - 学员Id
	 */
	public Integer getMemberid() {
		return memberid;
	}

	/**
	 * 设置学员Id
	 *
	 * @param memberid
	 *            学员Id
	 */
	public void setMemberid(Integer memberid) {
		this.memberid = memberid;
	}

	/**
	 * 获取场馆Id
	 *
	 * @return gymId - 场馆Id
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * 设置场馆Id
	 *
	 * @param gymid
	 *            场馆Id
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	/**
	 * 获取订单状态(0,未支付,1,已支付,3,待上课,4,已完成.5已取消)
	 *
	 * @return status - 订单状态(0,未支付,1,已支付,3,待上课,4,已完成.5已取消)
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * 设置订单状态(0,未支付,1,已支付,3,待上课,4,已完成.5已取消)
	 *
	 * @param status
	 *            订单状态(0,未支付,1,已支付,3,待上课,4,已完成.5已取消)
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * 获取订单类型
	 *
	 * @return type - 订单类型
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 设置订单类型
	 *
	 * @param type
	 *            订单类型
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 获取价格
	 *
	 * @return number - 价格
	 */
	public Integer getNumber() {
		return number;
	}

	/**
	 * 设置价格
	 *
	 * @param number
	 *            价格
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * 获取取消订单原因
	 *
	 * @return reason - 取消订单原因
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * 设置取消订单原因
	 *
	 * @param reason
	 *            取消订单原因
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * 获取优惠券Id
	 *
	 * @return couponId - 优惠券Id
	 */
	public Integer getCouponid() {
		return couponid;
	}

	/**
	 * 设置优惠券Id
	 *
	 * @param couponid
	 *            优惠券Id
	 */
	public void setCouponid(Integer couponid) {
		this.couponid = couponid;
	}

	/**
	 * 获取优惠券支付价格数量
	 *
	 * @return paidInCouponInt - 优惠券支付价格数量
	 */
	public Integer getPaidincouponint() {
		return paidincouponint;
	}

	/**
	 * 设置优惠券支付价格数量
	 *
	 * @param paidincouponint
	 *            优惠券支付价格数量
	 */
	public void setPaidincouponint(Integer paidincouponint) {
		this.paidincouponint = paidincouponint;
	}

	/**
	 * 获取糖豆支付价格数量
	 *
	 * @return paidInBeanInt - 糖豆支付价格数量
	 */
	public Integer getPaidinbeanint() {
		return paidinbeanint;
	}

	/**
	 * 设置糖豆支付价格数量
	 *
	 * @param paidinbeanint
	 *            糖豆支付价格数量
	 */
	public void setPaidinbeanint(Integer paidinbeanint) {
		this.paidinbeanint = paidinbeanint;
	}

	/**
	 * 获取创建时间
	 *
	 * @return createTime - 创建时间
	 */
	public Date getCreatetime() {
		return createtime;
	}

	/**
	 * 设置创建时间
	 *
	 * @param createtime
	 *            创建时间
	 */
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	/**
	 * 获取支付时间
	 *
	 * @return paidTime - 支付时间
	 */
	public Date getPaidtime() {
		return paidtime;
	}

	/**
	 * 设置支付时间
	 *
	 * @param paidtime
	 *            支付时间
	 */
	public void setPaidtime(Date paidtime) {
		this.paidtime = paidtime;
	}

	/**
	 * 获取节数
	 *
	 * @return lessons - 节数
	 */
	public Integer getLessons() {
		return lessons;
	}

	/**
	 * 设置节数
	 *
	 * @param lessons
	 *            节数
	 */
	public void setLessons(Integer lessons) {
		this.lessons = lessons;
	}

	/**
	 * 获取签到次数
	 *
	 * @return sign - 签到次数
	 */
	public Integer getSign() {
		return sign;
	}

	/**
	 * 设置签到次数
	 *
	 * @param sign
	 *            签到次数
	 */
	public void setSign(Integer sign) {
		this.sign = sign;
	}

	/**
	 * @return lessonId
	 */
	public Integer getLessonid() {
		return lessonid;
	}

	/**
	 * @param lessonid
	 */
	public void setLessonid(Integer lessonid) {
		this.lessonid = lessonid;
	}
}