package sf.core.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "td_sport")
public class TdSport {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "classType")
	private Byte classtype;

	private Integer value;

	private String name;

	private String icon;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return classType
	 */
	public Byte getClasstype() {
		return classtype;
	}

	/**
	 * @param classtype
	 */
	public void setClasstype(Byte classtype) {
		this.classtype = classtype;
	}

	/**
	 * @return value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * @param value
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}
}