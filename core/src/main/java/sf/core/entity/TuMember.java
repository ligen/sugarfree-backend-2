package sf.core.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tu_member")
public class TuMember {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Long mobile;

	private String name;

	private String password;

	private String avatar;

	private Double beans;

	private Byte gender;

	private String address;

	@Column(name = "createdTime")
	private Date createdtime;

	private Short weight;

	private Short height;

	private Date birthday;

	/**
	 * 邀请人
	 */
	private Integer invitatior;

	/**
	 * 注册时间
	 */
	@Column(name = "createDate")
	private Date createdate;

	/**
	 * 身体指标
	 */
	private BigDecimal bmi;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return mobile
	 */
	public Long getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 */
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return beans
	 */
	public Double getBeans() {
		return beans;
	}

	/**
	 * @param beans
	 */
	public void setBeans(Double beans) {
		this.beans = beans;
	}

	/**
	 * @return gender
	 */
	public Byte getGender() {
		return gender;
	}

	/**
	 * @param gender
	 */
	public void setGender(Byte gender) {
		this.gender = gender;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return createdTime
	 */
	public Date getCreatedtime() {
		return createdtime;
	}

	/**
	 * @param createdtime
	 */
	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	/**
	 * @return weight
	 */
	public Short getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 */
	public void setWeight(Short weight) {
		this.weight = weight;
	}

	/**
	 * @return height
	 */
	public Short getHeight() {
		return height;
	}

	/**
	 * @param height
	 */
	public void setHeight(Short height) {
		this.height = height;
	}

	/**
	 * @return birthday
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * @param birthday
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * 获取邀请人
	 *
	 * @return invitatior - 邀请人
	 */
	public Integer getInvitatior() {
		return invitatior;
	}

	/**
	 * 设置邀请人
	 *
	 * @param invitatior
	 *            邀请人
	 */
	public void setInvitatior(Integer invitatior) {
		this.invitatior = invitatior;
	}

	/**
	 * 获取注册时间
	 *
	 * @return createDate - 注册时间
	 */
	public Date getCreatedate() {
		return createdate;
	}

	/**
	 * 设置注册时间
	 *
	 * @param createdate
	 *            注册时间
	 */
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	/**
	 * 获取身体指标
	 *
	 * @return bmi - 身体指标
	 */
	public BigDecimal getBmi() {
		return bmi;
	}

	/**
	 * 设置身体指标
	 *
	 * @param bmi
	 *            身体指标
	 */
	public void setBmi(BigDecimal bmi) {
		this.bmi = bmi;
	}
}