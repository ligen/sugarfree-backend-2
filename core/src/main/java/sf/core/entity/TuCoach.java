package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tu_coach")
public class TuCoach {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Date time;

	private String address;

	private Long mobile;

	private String name;

	private String password;

	private String avatar;

	@Column(name = "teachHistory")
	private Integer teachhistory;

	private String description;

	private Byte age;

	private Byte rate;

	private Byte gender;

	private Integer skills;

	@Column(name = "certificateStr")
	private String certificatestr;

	private Integer levels;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @param time
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * @return address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return mobile
	 */
	public Long getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 */
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return avatar
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * @param avatar
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * @return teachHistory
	 */
	public Integer getTeachhistory() {
		return teachhistory;
	}

	/**
	 * @param teachhistory
	 */
	public void setTeachhistory(Integer teachhistory) {
		this.teachhistory = teachhistory;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return age
	 */
	public Byte getAge() {
		return age;
	}

	/**
	 * @param age
	 */
	public void setAge(Byte age) {
		this.age = age;
	}

	/**
	 * @return rate
	 */
	public Byte getRate() {
		return rate;
	}

	/**
	 * @param rate
	 */
	public void setRate(Byte rate) {
		this.rate = rate;
	}

	/**
	 * @return gender
	 */
	public Byte getGender() {
		return gender;
	}

	/**
	 * @param gender
	 */
	public void setGender(Byte gender) {
		this.gender = gender;
	}

	/**
	 * @return skills
	 */
	public Integer getSkills() {
		return skills;
	}

	/**
	 * @param skills
	 */
	public void setSkills(Integer skills) {
		this.skills = skills;
	}

	/**
	 * @return certificateStr
	 */
	public String getCertificatestr() {
		return certificatestr;
	}

	/**
	 * @param certificatestr
	 */
	public void setCertificatestr(String certificatestr) {
		this.certificatestr = certificatestr;
	}

	/**
	 * @return levels
	 */
	public Integer getLevels() {
		return levels;
	}

	/**
	 * @param levels
	 */
	public void setLevels(Integer levels) {
		this.levels = levels;
	}
}