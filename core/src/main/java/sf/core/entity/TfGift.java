package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_gift")
public class TfGift {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "beansNow")
	private Integer beansnow;

	@Column(name = "launchTime")
	private Date launchtime;

	@Column(name = "expiredTime")
	private Date expiredtime;

	@Column(name = "gymId")
	private Integer gymid;

	@Column(name = "beansTotal")
	private Integer beanstotal;

	@Column(name = "beansPer")
	private Integer beansper;

	@Column(name = "validPeriod")
	private Integer validperiod;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return beansNow
	 */
	public Integer getBeansnow() {
		return beansnow;
	}

	/**
	 * @param beansnow
	 */
	public void setBeansnow(Integer beansnow) {
		this.beansnow = beansnow;
	}

	/**
	 * @return launchTime
	 */
	public Date getLaunchtime() {
		return launchtime;
	}

	/**
	 * @param launchtime
	 */
	public void setLaunchtime(Date launchtime) {
		this.launchtime = launchtime;
	}

	/**
	 * @return expiredTime
	 */
	public Date getExpiredtime() {
		return expiredtime;
	}

	/**
	 * @param expiredtime
	 */
	public void setExpiredtime(Date expiredtime) {
		this.expiredtime = expiredtime;
	}

	/**
	 * @return gymId
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * @param gymid
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	/**
	 * @return beansTotal
	 */
	public Integer getBeanstotal() {
		return beanstotal;
	}

	/**
	 * @param beanstotal
	 */
	public void setBeanstotal(Integer beanstotal) {
		this.beanstotal = beanstotal;
	}

	/**
	 * @return beansPer
	 */
	public Integer getBeansper() {
		return beansper;
	}

	/**
	 * @param beansper
	 */
	public void setBeansper(Integer beansper) {
		this.beansper = beansper;
	}

	/**
	 * @return validPeriod
	 */
	public Integer getValidperiod() {
		return validperiod;
	}

	/**
	 * @param validperiod
	 */
	public void setValidperiod(Integer validperiod) {
		this.validperiod = validperiod;
	}
}