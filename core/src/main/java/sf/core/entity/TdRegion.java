package sf.core.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "td_region")
public class TdRegion {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "shengShi")
	private String shengshi;

	@Column(name = "shiQuXian")
	private String shiquxian;

	@Column(name = "shangQuan")
	private String shangquan;

	private Byte weight;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return shengShi
	 */
	public String getShengshi() {
		return shengshi;
	}

	/**
	 * @param shengshi
	 */
	public void setShengshi(String shengshi) {
		this.shengshi = shengshi;
	}

	/**
	 * @return shiQuXian
	 */
	public String getShiquxian() {
		return shiquxian;
	}

	/**
	 * @param shiquxian
	 */
	public void setShiquxian(String shiquxian) {
		this.shiquxian = shiquxian;
	}

	/**
	 * @return shangQuan
	 */
	public String getShangquan() {
		return shangquan;
	}

	/**
	 * @param shangquan
	 */
	public void setShangquan(String shangquan) {
		this.shangquan = shangquan;
	}

	/**
	 * @return weight
	 */
	public Byte getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 */
	public void setWeight(Byte weight) {
		this.weight = weight;
	}
}