package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_message")
public class TfMessage {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	@Column(name = "fromId")
	private Integer fromid;

	@Column(name = "fromType")
	private Byte fromtype;

	@Column(name = "toType")
	private Byte totype;

	@Column(name = "toId")
	private Integer toid;

	private String text;

	private Date time;

	@Column(name = "messageType")
	private Integer messagetype;

	private Integer type;

	private Integer xid;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return fromId
	 */
	public Integer getFromid() {
		return fromid;
	}

	/**
	 * @param fromid
	 */
	public void setFromid(Integer fromid) {
		this.fromid = fromid;
	}

	/**
	 * @return fromType
	 */
	public Byte getFromtype() {
		return fromtype;
	}

	/**
	 * @param fromtype
	 */
	public void setFromtype(Byte fromtype) {
		this.fromtype = fromtype;
	}

	/**
	 * @return toType
	 */
	public Byte getTotype() {
		return totype;
	}

	/**
	 * @param totype
	 */
	public void setTotype(Byte totype) {
		this.totype = totype;
	}

	/**
	 * @return toId
	 */
	public Integer getToid() {
		return toid;
	}

	/**
	 * @param toid
	 */
	public void setToid(Integer toid) {
		this.toid = toid;
	}

	/**
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @param time
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * @return messageType
	 */
	public Integer getMessagetype() {
		return messagetype;
	}

	/**
	 * @param messagetype
	 */
	public void setMessagetype(Integer messagetype) {
		this.messagetype = messagetype;
	}

	/**
	 * @return type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * @param type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * @return xid
	 */
	public Integer getXid() {
		return xid;
	}

	/**
	 * @param xid
	 */
	public void setXid(Integer xid) {
		this.xid = xid;
	}
}