package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_comment")
public class TfComment {
	/**
	 * 订单号
	 */
	@Id
	private Integer rid;

	/**
	 * 学员Id
	 */
	private Integer uid;

	/**
	 * 评论内容
	 */
	private Integer content;

	/**
	 * 评论时间
	 */
	@Column(name = "createDate")
	private Date createdate;

	/**
	 * 课程包Id
	 */
	@Column(name = "packageId")
	private Integer packageid;

	/**
	 * 场馆Id
	 */
	@Column(name = "gymId")
	private Integer gymid;

	/**
	 * 评分等级
	 */
	private Integer liked;

	/**
	 * 获取订单号
	 *
	 * @return rid - 订单号
	 */
	public Integer getRid() {
		return rid;
	}

	/**
	 * 设置订单号
	 *
	 * @param rid
	 *            订单号
	 */
	public void setRid(Integer rid) {
		this.rid = rid;
	}

	/**
	 * 获取学员Id
	 *
	 * @return uid - 学员Id
	 */
	public Integer getUid() {
		return uid;
	}

	/**
	 * 设置学员Id
	 *
	 * @param uid
	 *            学员Id
	 */
	public void setUid(Integer uid) {
		this.uid = uid;
	}

	/**
	 * 获取评论内容
	 *
	 * @return content - 评论内容
	 */
	public Integer getContent() {
		return content;
	}

	/**
	 * 设置评论内容
	 *
	 * @param content
	 *            评论内容
	 */
	public void setContent(Integer content) {
		this.content = content;
	}

	/**
	 * 获取评论时间
	 *
	 * @return createDate - 评论时间
	 */
	public Date getCreatedate() {
		return createdate;
	}

	/**
	 * 设置评论时间
	 *
	 * @param createdate
	 *            评论时间
	 */
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	/**
	 * 获取课程包Id
	 *
	 * @return packageId - 课程包Id
	 */
	public Integer getPackageid() {
		return packageid;
	}

	/**
	 * 设置课程包Id
	 *
	 * @param packageid
	 *            课程包Id
	 */
	public void setPackageid(Integer packageid) {
		this.packageid = packageid;
	}

	/**
	 * 获取场馆Id
	 *
	 * @return gymId - 场馆Id
	 */
	public Integer getGymid() {
		return gymid;
	}

	/**
	 * 设置场馆Id
	 *
	 * @param gymid
	 *            场馆Id
	 */
	public void setGymid(Integer gymid) {
		this.gymid = gymid;
	}

	/**
	 * 获取评分等级
	 *
	 * @return liked - 评分等级
	 */
	public Integer getLiked() {
		return liked;
	}

	/**
	 * 设置评分等级
	 *
	 * @param liked
	 *            评分等级
	 */
	public void setLiked(Integer liked) {
		this.liked = liked;
	}
}