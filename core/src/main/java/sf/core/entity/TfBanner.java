package sf.core.entity;

import java.util.Date;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_banner")
public class TfBanner {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private String url;

	private String image;

	private String title;

	private String description;

	private String shareurl;

	private String shareimage;

	private Byte isshow;

	private Date createdtime;

	private Date modifytime;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return shareurl
	 */
	public String getShareurl() {
		return shareurl;
	}

	/**
	 * @param shareurl
	 */
	public void setShareurl(String shareurl) {
		this.shareurl = shareurl;
	}

	/**
	 * @return shareimage
	 */
	public String getShareimage() {
		return shareimage;
	}

	/**
	 * @param shareimage
	 */
	public void setShareimage(String shareimage) {
		this.shareimage = shareimage;
	}

	/**
	 * @return isshow
	 */
	public Byte getIsshow() {
		return isshow;
	}

	/**
	 * @param isshow
	 */
	public void setIsshow(Byte isshow) {
		this.isshow = isshow;
	}

	/**
	 * @return createdtime
	 */
	public Date getCreatedtime() {
		return createdtime;
	}

	/**
	 * @param createdtime
	 */
	public void setCreatedtime(Date createdtime) {
		this.createdtime = createdtime;
	}

	/**
	 * @return modifytime
	 */
	public Date getModifytime() {
		return modifytime;
	}

	/**
	 * @param modifytime
	 */
	public void setModifytime(Date modifytime) {
		this.modifytime = modifytime;
	}
}