package sf.core.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_order")
public class TfOrder {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Integer cost;

	private Integer beans;

	@Column(name = "memberId")
	private Integer memberid;

	private Integer status;

	private String subject;

	private String memo;

	private Date time;

	@Column(name = "serialNumber")
	private Long serialnumber;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return cost
	 */
	public Integer getCost() {
		return cost;
	}

	/**
	 * @param cost
	 */
	public void setCost(Integer cost) {
		this.cost = cost;
	}

	/**
	 * @return beans
	 */
	public Integer getBeans() {
		return beans;
	}

	/**
	 * @param beans
	 */
	public void setBeans(Integer beans) {
		this.beans = beans;
	}

	/**
	 * @return memberId
	 */
	public Integer getMemberid() {
		return memberid;
	}

	/**
	 * @param memberid
	 */
	public void setMemberid(Integer memberid) {
		this.memberid = memberid;
	}

	/**
	 * @return status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * @param subject
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * @return memo
	 */
	public String getMemo() {
		return memo;
	}

	/**
	 * @param memo
	 */
	public void setMemo(String memo) {
		this.memo = memo;
	}

	/**
	 * @return time
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * @param time
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * @return serialNumber
	 */
	public Long getSerialnumber() {
		return serialnumber;
	}

	/**
	 * @param serialnumber
	 */
	public void setSerialnumber(Long serialnumber) {
		this.serialnumber = serialnumber;
	}
}