package sf.core.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_image")
public class TfImage {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	private Byte type;

	private Integer xid;

	private String url;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return type
	 */
	public Byte getType() {
		return type;
	}

	/**
	 * @param type
	 */
	public void setType(Byte type) {
		this.type = type;
	}

	/**
	 * @return xid
	 */
	public Integer getXid() {
		return xid;
	}

	/**
	 * @param xid
	 */
	public void setXid(Integer xid) {
		this.xid = xid;
	}

	/**
	 * @return url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url
	 */
	public void setUrl(String url) {
		this.url = url;
	}
}