package sf.core.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tf_level")
public class TfLevel {
	@Id
	@GeneratedValue(generator = "JDBC")
	private Integer id;

	/**
	 * 名称
	 */
	private String title;

	/**
	 * 值
	 */
	private Integer value;

	/**
	 * 等级
	 */
	private Integer level;

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 获取名称
	 *
	 * @return title - 名称
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * 设置名称
	 *
	 * @param title
	 *            名称
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * 获取值
	 *
	 * @return value - 值
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * 设置值
	 *
	 * @param value
	 *            值
	 */
	public void setValue(Integer value) {
		this.value = value;
	}

	/**
	 * 获取等级
	 *
	 * @return level - 等级
	 */
	public Integer getLevel() {
		return level;
	}

	/**
	 * 设置等级
	 *
	 * @param level
	 *            等级
	 */
	public void setLevel(Integer level) {
		this.level = level;
	}
}