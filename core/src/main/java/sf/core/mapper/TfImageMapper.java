package sf.core.mapper;

import sf.core.entity.TfImage;
import tk.mybatis.mapper.common.Mapper;

public interface TfImageMapper extends Mapper<TfImage> {
}