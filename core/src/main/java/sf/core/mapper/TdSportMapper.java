package sf.core.mapper;

import sf.core.entity.TdSport;
import tk.mybatis.mapper.common.Mapper;

public interface TdSportMapper extends Mapper<TdSport> {
}