package sf.core.mapper;

import sf.core.entity.TfOrder;
import tk.mybatis.mapper.common.Mapper;

public interface TfOrderMapper extends Mapper<TfOrder> {
}