package sf.core.mapper;

import sf.core.entity.TfReceipt;
import tk.mybatis.mapper.common.Mapper;

public interface TfReceiptMapper extends Mapper<TfReceipt> {
}