package sf.core.mapper;

import sf.core.entity.TfMessage;
import tk.mybatis.mapper.common.Mapper;

public interface TfMessageMapper extends Mapper<TfMessage> {
}