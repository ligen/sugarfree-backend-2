package sf.core.mapper;

import sf.core.entity.TfFavor;
import tk.mybatis.mapper.common.Mapper;

public interface TfFavorMapper extends Mapper<TfFavor> {
}