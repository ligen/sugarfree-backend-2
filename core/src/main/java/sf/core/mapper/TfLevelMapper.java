package sf.core.mapper;

import sf.core.entity.TfLevel;
import tk.mybatis.mapper.common.Mapper;

public interface TfLevelMapper extends Mapper<TfLevel> {
}