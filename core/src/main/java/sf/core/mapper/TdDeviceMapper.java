package sf.core.mapper;

import sf.core.entity.TdDevice;
import tk.mybatis.mapper.common.Mapper;

public interface TdDeviceMapper extends Mapper<TdDevice> {
}