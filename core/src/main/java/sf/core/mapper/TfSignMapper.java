package sf.core.mapper;

import sf.core.entity.TfSign;
import tk.mybatis.mapper.common.Mapper;

public interface TfSignMapper extends Mapper<TfSign> {
}