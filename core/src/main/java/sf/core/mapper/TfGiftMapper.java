package sf.core.mapper;

import sf.core.entity.TfGift;
import tk.mybatis.mapper.common.Mapper;

public interface TfGiftMapper extends Mapper<TfGift> {
}