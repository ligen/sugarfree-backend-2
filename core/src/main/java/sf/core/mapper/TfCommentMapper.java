package sf.core.mapper;

import sf.core.entity.TfComment;
import tk.mybatis.mapper.common.Mapper;

public interface TfCommentMapper extends Mapper<TfComment> {
}