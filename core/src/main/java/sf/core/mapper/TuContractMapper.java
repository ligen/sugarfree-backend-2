package sf.core.mapper;

import sf.core.entity.TuContract;
import tk.mybatis.mapper.common.Mapper;

public interface TuContractMapper extends Mapper<TuContract> {
}