package sf.core.mapper;

import sf.core.entity.TuAdmin;
import tk.mybatis.mapper.common.Mapper;

public interface TuAdminMapper extends Mapper<TuAdmin> {
}