package sf.core.mapper;

import sf.core.entity.TuDeviceToken;
import tk.mybatis.mapper.common.Mapper;

public interface TuDeviceTokenMapper extends Mapper<TuDeviceToken> {
}