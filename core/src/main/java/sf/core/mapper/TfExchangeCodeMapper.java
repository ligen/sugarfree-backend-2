package sf.core.mapper;

import sf.core.entity.TfExchangeCode;
import tk.mybatis.mapper.common.Mapper;

public interface TfExchangeCodeMapper extends Mapper<TfExchangeCode> {
}