package sf.core.mapper;

import sf.core.entity.TfCourse;
import tk.mybatis.mapper.common.Mapper;

public interface TfCourseMapper extends Mapper<TfCourse> {
}