package sf.core.mapper;

import sf.core.entity.TuMember;
import tk.mybatis.mapper.common.Mapper;

public interface TuMemberMapper extends Mapper<TuMember> {
}