package sf.core.mapper;

import sf.core.entity.TuCoach;
import tk.mybatis.mapper.common.Mapper;

public interface TuCoachMapper extends Mapper<TuCoach> {
}