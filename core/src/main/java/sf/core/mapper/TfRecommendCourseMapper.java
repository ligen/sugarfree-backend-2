package sf.core.mapper;

import sf.core.entity.TfRecommendCourse;
import tk.mybatis.mapper.common.Mapper;

public interface TfRecommendCourseMapper extends Mapper<TfRecommendCourse> {
}