package sf.core.mapper;

import sf.core.entity.TfSuggestion;
import tk.mybatis.mapper.common.Mapper;

public interface TfSuggestionMapper extends Mapper<TfSuggestion> {
}