package sf.core.mapper;

import sf.core.entity.TfCoupon;
import tk.mybatis.mapper.common.Mapper;

public interface TfCouponMapper extends Mapper<TfCoupon> {
}