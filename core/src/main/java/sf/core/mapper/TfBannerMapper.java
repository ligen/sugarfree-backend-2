package sf.core.mapper;

import sf.core.entity.TfBanner;
import tk.mybatis.mapper.common.Mapper;

public interface TfBannerMapper extends Mapper<TfBanner> {
}