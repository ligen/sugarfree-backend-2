package sf.core.mapper;

import sf.core.entity.TdRegion;
import tk.mybatis.mapper.common.Mapper;

public interface TdRegionMapper extends Mapper<TdRegion> {
}