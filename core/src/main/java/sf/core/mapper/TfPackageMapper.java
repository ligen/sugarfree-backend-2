package sf.core.mapper;

import sf.core.entity.TfPackage;
import tk.mybatis.mapper.common.Mapper;

public interface TfPackageMapper extends Mapper<TfPackage> {
}