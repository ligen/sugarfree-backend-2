package sf.core.mapper;

import sf.core.entity.TuGym;
import tk.mybatis.mapper.common.Mapper;

public interface TuGymMapper extends Mapper<TuGym> {
}