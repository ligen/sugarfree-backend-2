package sf.core.mapper;

import sf.core.entity.TfLesson;
import tk.mybatis.mapper.common.Mapper;

public interface TfLessonMapper extends Mapper<TfLesson> {
}