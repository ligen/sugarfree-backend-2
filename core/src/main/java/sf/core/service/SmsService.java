package sf.core.service;

import sf.core.exception.SugarException;

/**
 * Created by ligen on 16-3-3.
 */
public interface SmsService {
	void sendVerificationCode(String mobile, String code) throws SugarException;

	void sendNotificationText(String mobile, String tailNumber, String action, String time, String title)
			throws SugarException;

}
