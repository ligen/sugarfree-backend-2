package sf.core.service;

public interface StorageService {

	String BUCKET_MEDIA = "media";

	char TYPE_GYM = 'G';
	char TYPE_MEMBER = 'M';
	char TYPE_COACH = 'C';
	char TYPE_ADMIN = 'A';

	String DOMAIN_QINIU_MEDIA = "http://7xlk50.com2.z0.glb.qiniucdn.com/";

	String getUploadImageToken();

	String getUploadImageToken(int imageType, int xid);

	String getUploadAvatarToken(char avatarType, int xid);

}
