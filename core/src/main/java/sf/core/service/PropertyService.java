package sf.core.service;

/**
 * Created by sugarfreeone on 15/12/3.
 */
public interface PropertyService {

	String getAppId(int deviceType);

	int getUnitPrice();
}
