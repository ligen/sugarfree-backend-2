package sf.core.service.impl;

import java.io.Serializable;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import sf.core.service.KeyValueService;

/**
 * Created by ligen on 16-3-3.
 */
@Service
public class KeyValueServiceImpl implements KeyValueService {

	@Resource
	RedisTemplate<Serializable, Serializable> redisTemplate;

	public void set(String key, String value) {
		redisTemplate.opsForValue().set(key, value);
	}

	public void set(String key, String value, long timeout, TimeUnit unit) {
		redisTemplate.opsForValue().set(key, value, timeout, unit);
	}

	public String get(String key) {
		return redisTemplate.opsForValue().get(key).toString();
	}

	public void delete(String key) {
		redisTemplate.delete(key);
	}

	public boolean hasKey(String key) {
		return redisTemplate.hasKey(key);
	}

	public long ttl(String key) {
		return redisTemplate.getExpire(key);
	}

}
