package sf.core.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cn.jpush.api.JPushClient;
import cn.jpush.api.common.resp.APIConnectionException;
import cn.jpush.api.common.resp.APIRequestException;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.Notification;
import sf.core.common.MessageStatus;
import sf.core.service.IPush;

@Service
public class JiGuangPush implements IPush {

	private static final String MEMBER_APPKEY = "da87309728e11338002ca134";
	private static final String MEMBER_MASTER_SECRET = "9eaff21fc583e3d4aa22921a";
	private static final String COACH_APPKEY = "59e380c0d2839543b4dc315a";
	private static final String COACH_MASTER_SECRET = "a4aefd98487e8f9148df2251";
	private Logger LOG;

	public JiGuangPush() {
		LOG = LoggerFactory.getLogger(JiGuangPush.class);
	}

	public JPushClient buildJPushClient(int userType) {
		String appkey = null;
		String master_secret = null;
		if (userType == MessageStatus.TYPE_MEMBER) {
			LOG.debug("usertype=member");
			appkey = MEMBER_APPKEY;
			master_secret = MEMBER_MASTER_SECRET;
		} else if (userType == MessageStatus.TYPE_COACH) {
			LOG.debug("usertype=coach");
			appkey = COACH_APPKEY;
			master_secret = COACH_MASTER_SECRET;
		}

		if (appkey == null || master_secret == null) {
			LOG.error("invalid usertype");
			return null;
		}
		JPushClient jPushClient = new JPushClient(master_secret, appkey, 3);
		return jPushClient;
	}

	@Override
	public void unicast(String deviceToken, int userType, String message) {
		try {
			PushPayload pushPayload = PushPayload.newBuilder().setPlatform(Platform.all())
					.setAudience(Audience.registrationId(deviceToken)).setNotification(Notification.alert(message))
					.setOptions(Options.newBuilder().setApnsProduction(true).build()).build();
			JPushClient jPushClient = buildJPushClient(userType);
			jPushClient.sendPush(pushPayload);
		} catch (APIConnectionException e) {
			LOG.error("jpush error");
			LOG.error("Connection error, should retry later", e);
		} catch (APIRequestException e) {
			LOG.error("jpush error");
			LOG.error("Should review the error, and fix the request", e);
			LOG.info("HTTP Status: " + e.getStatus());
			LOG.info("Error Code: " + e.getErrorCode());
			LOG.info("Error Message: " + e.getErrorMessage());
		} catch (Exception e) {
			LOG.error("jpush error", e);
		}
	}

	@Override
	public void broadcast(int userType, String message) {
		try {
			JPushClient jPushClient = buildJPushClient(userType);
			jPushClient.sendPush(PushPayload.alertAll(message));
		} catch (APIConnectionException e) {
			LOG.error("jpush error");
			LOG.error("Connection error, should retry later", e);
		} catch (APIRequestException e) {
			LOG.error("jpush error");
			LOG.error("Should review the error, and fix the request", e);
			LOG.info("HTTP Status: " + e.getStatus());
			LOG.info("Error Code: " + e.getErrorCode());
			LOG.info("Error Message: " + e.getErrorMessage());
		} catch (Exception e) {
			LOG.error("jpush error", e);
		}
	}

	@Override
	public void groupcast(List<String> deviceTokens, int userType, String message) {
		try {
			PushPayload pushPayload = PushPayload.newBuilder().setPlatform(Platform.all())
					.setAudience(Audience.registrationId(deviceTokens)).setNotification(Notification.alert(message))
					.setOptions(Options.newBuilder().setApnsProduction(true).build()).build();
			JPushClient jPushClient = buildJPushClient(userType);
			jPushClient.sendPush(pushPayload);
		} catch (APIConnectionException e) {
			LOG.error("jpush error");
			LOG.error("Connection error, should retry later", e);
		} catch (APIRequestException e) {
			LOG.error("jpush error");
			LOG.error("Should review the error, and fix the request", e);
			LOG.info("HTTP Status: " + e.getStatus());
			LOG.info("Error Code: " + e.getErrorCode());
			LOG.info("Error Message: " + e.getErrorMessage());
		} catch (Exception e) {
			LOG.error("jpush error", e);
		}
	}
}
