package sf.core.service.impl;

import javax.annotation.Resource;

import sf.core.mapper.TfExchangeCodeMapper;
import sf.core.service.PropertyService;

/**
 * Created by sugarfreeone on 15/12/3.
 */
public class PropertyServiceImpl implements PropertyService {

	@Resource
	private TfExchangeCodeMapper exchangeCodeMapper;

	public static final int DEVICE_TYPE_IOS = 0;
	public static final int DEVICE_TYPE_ANDROID = 1;
	public static final int DEVICE_TYPE_WEXIN = 2;

	private String appIdAndroid;
	private String appIdIOS;
	private String appIdWexin;
	private int unitPrice;

	public String getAppIdAndroid() {
		return appIdAndroid;
	}

	public void setAppIdAndroid(String appIdAndroid) {
		this.appIdAndroid = appIdAndroid;
	}

	public String getAppIdIOS() {
		return appIdIOS;
	}

	public void setAppIdIOS(String appIdIOS) {
		this.appIdIOS = appIdIOS;
	}

	@Override
	public int getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}

	@Override
	public String getAppId(int deviceType) {

		if (deviceType == DEVICE_TYPE_IOS) {
			return appIdIOS;
		} else if (deviceType == DEVICE_TYPE_ANDROID) {
			return appIdAndroid;
		} else if (deviceType == DEVICE_TYPE_WEXIN) {
			return appIdWexin;
		} else {
			return null;
		}

	}

	public String getAppIdWexin() {
		return appIdWexin;
	}

	public void setAppIdWexin(String appIdWexin) {
		this.appIdWexin = appIdWexin;
	}
}
