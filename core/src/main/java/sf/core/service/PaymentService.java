package sf.core.service;

import com.pingplusplus.model.Charge;

import sf.core.exception.SugarException;

/**
 * Created by ligen on 16-3-8.
 */
public interface PaymentService {
	int DEVICE_TYPE_IOS = 0;
	int DEVICE_TYPE_ANDROID = 1;

	int getUnitPrice();

	String getAppId(int deviceType) throws SugarException;

	Charge charge(int amount, String channel, Long sn, String subject, int deviceType, String openId)
			throws SugarException;
}
