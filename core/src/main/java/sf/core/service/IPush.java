package sf.core.service;

import java.util.List;

public interface IPush {

	public void unicast(String deviceToken, int userType, String message) throws Exception;

	public void broadcast(int userType, String message) throws Exception;

	public void groupcast(List<String> deviceTokens, int userType, String message) throws Exception;

}
