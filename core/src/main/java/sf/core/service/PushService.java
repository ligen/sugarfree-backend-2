package sf.core.service;

import java.util.List;

public interface PushService {

	void saveDeviceToken(Integer userType, int xid, String token);

	String retrieveDeviceToken(int userType, int xid);

	void sendFeedbackMessage(int userType, int xid, String message);

	void sendTradeMessage(int userType, int xid, String message);

	void sendWelcomeMessage(int userType, int xid, String message);

	void sendSysMessage(int userType, int xid, String message, boolean needPush);

	public void sendUserMessage(int from_type, int from_id, int to_type, int to_id, String message, int entityType,
			int entityId, boolean needPush);

	public void unicast(String deviceToken, int userType, String message) throws Exception;

	public void broadcast(int userType, String message) throws Exception;

	public void groupcast(List<String> deviceTokens, int userType, String message) throws Exception;

	void sendBroadcastMessage(int userType, String message);

	void sendUnicastMessage(int fromType, int fromId, int toType, int toId, String message, boolean needPush);

	void sendGroupcastMessage(int fromType, int fromId, int toType, List<Integer> toIds, String message,
			boolean needPush);

}
