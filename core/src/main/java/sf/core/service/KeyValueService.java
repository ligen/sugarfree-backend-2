package sf.core.service;

import java.util.concurrent.TimeUnit;

/**
 * Created by ligen on 16-3-3.
 */
public interface KeyValueService {

	void set(String key, String value);

	void set(String key, String value, long timeout, TimeUnit unit);

	String get(String key);

	void delete(String key);

	boolean hasKey(String key);

	long ttl(String key);

}
