package sf.core.service;

import com.nimbusds.jose.JOSEException;
import sf.core.exception.SugarException;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by ligen on 16-3-11.
 */
public interface AuthService<T> {

	String createToken(Integer sub, Date issueTime, Date expireTime) throws JOSEException;

	T getSubject(HttpServletRequest request) throws SugarException;
}
