package sf.core.service;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import sf.core.service.impl.AuthServiceImpl;

/**
 * Created by ligen on 16-3-11.
 */
public class AuthServiceTest {

    AuthService<Integer> authService;

    @Before
    public void setUp(){
        authService = new AuthServiceImpl();
    }

    @Test
    public void test_create_and_decode_token() throws Exception{
        Integer sub = 123;
        String token = authService.createToken(
                sub,
                DateTime.now().toDate(),
                DateTime.now().plus(14).toDate()
                );
        Assert.assertNotNull(token);
        Assert.assertNotEquals(sub, token);

        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", token);

        Assert.assertEquals(sub, authService.getSubject(request));

    }
}
