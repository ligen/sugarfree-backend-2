package sf.member.common;

/**
 * Created by wb on 2016/3/4.
 */
public class ImageStatus {

	public static final int TYPE_MEMBER = 1;
	public static final int TYPE_COACH = 2;
	public static final int TYPE_GYM = 3;
	public static final int TYPE_COURSE = 4;
	public static final int TYPE_CERTIFICATE = 5;
	public static final int TYPE_RECOMMEND_COURSE = 6;
	public static final int TYPE_PACKAGE_LESSON = 7;
	public static final int TYPE_TRAIN_COMMENT_IMAGE = 8; // 评论图片
	public static final int TYPE_PRIVATE_COMMENT_IMAGE = 9; // 私教包评论图片

	public static final int GYM_ENVOLOP = 9; // 场馆封面

}
