package sf.member.common;

public enum PackageType {

	TRAIN_PACKAGE(1, "训练营"), PRIVATE_PACKAGE(2, "私教包");

	private Integer index;

	private String desc;

	private PackageType(int index, String desc) {
		this.index = index;
		this.desc = desc;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static PackageType getInstance(int status) {
		PackageType[] allStatus = PackageType.values();
		for (PackageType ws : allStatus) {
			if (ws.getIndex() == status) {
				return ws;
			}
		}
		throw new IllegalArgumentException("status值非法，没有找到相应套餐类型");
	}

}
