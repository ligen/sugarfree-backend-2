package sf.member.common;

/**
 * Created by wb on 2016/3/3.
 */
public enum OrderStatus {

	WAIT_PAYED(0, "WAIT_PAYED"), PAYED(1, "PAYED"), WAIT_BOOKED(3, "WAIT_BOOKED"), OVERED(4, "OVERED"), CANNEL(5,
			"CANNEL");

	private int index;

	private String desc;

	private OrderStatus(int index, String desc) {
		this.index = index;
		this.desc = desc;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public static OrderStatus getInstance(int status) {
		OrderStatus[] allStatus = OrderStatus.values();
		for (OrderStatus ws : allStatus) {
			if (ws.getIndex() == status) {
				return ws;
			}
		}
		throw new IllegalArgumentException("status值非法，没有找到相应状态");
	}
}
