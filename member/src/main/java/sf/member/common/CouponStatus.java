package sf.member.common;

/**
 * Created by wb on 2016/3/3.
 */
public enum CouponStatus {

	WAIT_USE(0, "WAIT_USE"), EXPIRED(1, "EXPIRED"), USED(3, "USED");

	private int index;

	private String desc;

	private CouponStatus(int index, String desc) {
		this.index = index;
		this.desc = desc;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
