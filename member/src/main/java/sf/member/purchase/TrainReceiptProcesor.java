package sf.member.purchase;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sf.core.entity.TfLesson;
import sf.core.entity.TfPackage;
import sf.core.entity.TfReceipt;
import sf.core.entity.TfSign;
import sf.core.exception.SugarException;
import sf.core.mapper.TfLessonMapper;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TfSignMapper;
import sf.member.common.OrderStatus;
import sf.member.common.PackageType;

/**
 * Created by wb on 2016/3/3.
 */
@Service
public class TrainReceiptProcesor extends AbstractDefaultReceipt {

	@Autowired
	private TfPackageMapper packageMapper;

	@Autowired
	private TfLessonMapper lessonMapper;

	@Autowired
	private TfReceiptMapper receiptMapper;

	@Autowired
	private TfSignMapper signMapper;

	public void vaildReceipt(Integer packageId, Integer couponId, Integer memberId) throws SugarException {

		TfPackage tfPackage = packageMapper.selectByPrimaryKey(packageId);

		if (tfPackage.getEndtime().getTime() < new Date().getTime()) {
			throw new SugarException(5011, "报名时间已过");
		}
		if (tfPackage.getNumberlimit() - tfPackage.getRealnumber() <= 0) {
			throw new SugarException(5012, "报名人数已满");
		}
		vaildCoupon(couponId, memberId);

	}

	@Transactional
	public TfReceipt purchase(Integer packageId, Integer couponId, Integer memberId) throws SugarException {

		TfReceipt receipt = new TfReceipt();
		TfPackage tfPackage = packageMapper.selectByPrimaryKey(packageId);
		receipt.setNumber(tfPackage.getPriceint());
		receipt.setLessons(tfPackage.getLessons());
		receipt.setCreatetime(new Date());
		receipt.setPaidtime(new Date());
		receipt.setSign(0);
		receipt.setGymid(tfPackage.getGymid());
		receipt.setPackageid(packageId);
		receipt.setMemberid(memberId);
		receipt.setType(PackageType.PRIVATE_PACKAGE.getIndex());
		if (couponId != null) {
			changeReceiptAndCouponStatus(couponId, receipt);
		} else {
			receipt.setPaidinbeanint(receipt.getNumber());
		}
		receipt.setStatus(OrderStatus.PAYED.getIndex());
		receiptMapper.insertSelective(receipt);
		tfPackage.setRealnumber(tfPackage.getRealnumber() + 1);
		packageMapper.updateByPrimaryKeySelective(tfPackage);
		addSign(receipt.getId(), packageId, memberId);
		return receipt;
	}

	@Override
	@Transactional
	public void addSign(Integer receipt, Integer packageId, Integer memberId) throws SugarException {

		TfLesson query = new TfLesson();
		query.setPackageid(packageId);
		List<TfLesson> lessons = lessonMapper.select(query);
		for (TfLesson lesson : lessons) {
			TfSign sign = new TfSign();
			sign.setRid(receipt);
			sign.setUid(memberId);
			sign.setCode((int) ((Math.random() * 9 + 1) * 100000));
			sign.setIssign(0);
			sign.setLessonid(lesson.getId());
			signMapper.insertSelective(sign);
		}
	}

	@Override
	@Transactional
	public void scanSign(Integer signId, Integer memberId) throws SugarException {
		validSign(signId, memberId);
		TfSign sign = signMapper.selectByPrimaryKey(signId);
		TfReceipt receipt = receiptMapper.selectByPrimaryKey(sign.getRid());
		if (receipt.getSign() + 1 == receipt.getLessons()) {
			receipt.setStatus(OrderStatus.OVERED.getIndex());
		}
		receipt.setSign(receipt.getSign() + 1);
		receiptMapper.updateByPrimaryKeySelective(receipt);
	}

	@Override
	public int queryPrice(int packageId) throws SugarException {
		return packageMapper.selectByPrimaryKey(packageId).getPriceint();
	}
}
