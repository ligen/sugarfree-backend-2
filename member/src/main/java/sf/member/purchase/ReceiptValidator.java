package sf.member.purchase;

import sf.core.exception.SugarException;

/**
 * Created by wb on 2016/3/3.
 */
public interface ReceiptValidator {

	/**
	 * 校验订单
	 *
	 * @param packageId
	 * @param couponId
	 * @throws SugarException
	 */
	public abstract void vaildReceipt(Integer packageId, Integer couponId, Integer memberId) throws SugarException;

}
