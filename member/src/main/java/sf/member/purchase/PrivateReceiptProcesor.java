package sf.member.purchase;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sf.core.entity.TfLesson;
import sf.core.entity.TfPackage;
import sf.core.entity.TfReceipt;
import sf.core.entity.TfSign;
import sf.core.exception.SugarException;
import sf.core.mapper.TfLessonMapper;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TfSignMapper;
import sf.member.common.OrderStatus;
import sf.member.common.PackageType;

/**
 * Created by wb on 2016/3/3.
 */
@Service
public class PrivateReceiptProcesor extends AbstractDefaultReceipt {

	@Autowired
	private TfPackageMapper packageMapper;

	@Autowired
	private TfLessonMapper lessonMapper;

	@Autowired
	private TfReceiptMapper receiptMapper;

	@Autowired
	private TfSignMapper signMapper;

	public void vaildReceipt(Integer packageId, Integer couponId, Integer memberId) throws SugarException {

		if (packageId == null) {
			throw new SugarException(5000, "私教包对应lessonId不能为空");
		}
		if (lessonMapper.selectByPrimaryKey(packageId) == null) {
			throw new SugarException(5001, "传入lessonId有误");
		}
		vaildCoupon(couponId, memberId);
	}

	@Transactional
	public TfReceipt purchase(Integer packageId, Integer couponId, Integer memberId) throws SugarException {

		TfReceipt receipt = new TfReceipt();
		receipt.setLessonid(packageId);
		TfLesson lesson = lessonMapper.selectByPrimaryKey(packageId);
		TfPackage tfPackage = packageMapper.selectByPrimaryKey(lesson.getPackageid());
		receipt.setNumber(lesson.getPrice());
		receipt.setLessons(lesson.getLessons());
		receipt.setCreatetime(new Date());
		receipt.setPaidtime(new Date());
		receipt.setMemberid(memberId);
		receipt.setGymid(tfPackage.getGymid());
		receipt.setLessons(lesson.getLessons());
		receipt.setSign(0);
		receipt.setPackageid(tfPackage.getId());
		receipt.setType(PackageType.PRIVATE_PACKAGE.getIndex());
		if (couponId != null) {
			changeReceiptAndCouponStatus(couponId, receipt);
		} else {
			receipt.setPaidinbeanint(receipt.getNumber());
		}
		receipt.setStatus(OrderStatus.PAYED.getIndex());
		receiptMapper.insert(receipt);
		addSign(receipt.getId(), packageId, memberId);
		return receipt;
	}

	@Override
	@Transactional
	public void addSign(Integer receipt, Integer packageId, Integer memberId) throws SugarException {
		TfLesson lesson = lessonMapper.selectByPrimaryKey(packageId);
		Integer lessons = lesson.getLessons();
		for (Integer i = 0; i < lessons; i++) {
			TfSign sign = new TfSign();
			sign.setLessonid(lesson.getId());
			sign.setCode((int) ((Math.random() * 9 + 1) * 100000));
			sign.setRid(receipt);
			sign.setUid(memberId);
			sign.setIssign(0);
			signMapper.insertSelective(sign);
		}
	}

	@Override
	@Transactional
	public void scanSign(Integer signId, Integer memberId) throws SugarException {
		validSign(signId, memberId);
		TfSign sign = signMapper.selectByPrimaryKey(signId);
		TfReceipt receipt = receiptMapper.selectByPrimaryKey(sign.getRid());
		TfLesson lesson = lessonMapper.selectByPrimaryKey(sign.getLessonid());
		if (receipt.getSign() + 1 == lesson.getLessons()) {
			receipt.setStatus(OrderStatus.OVERED.getIndex());
		}
		receipt.setSign(receipt.getSign() + 1);
		receiptMapper.updateByPrimaryKeySelective(receipt);
	}

	@Override
	public int queryPrice(int packageId) throws SugarException {

		int price = 0;
		TfLesson tfLesson = lessonMapper.selectByPrimaryKey(packageId);
		if (tfLesson != null) {
			price = tfLesson.getPrice();
		}
		return price;
	}

}
