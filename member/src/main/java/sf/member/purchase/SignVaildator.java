package sf.member.purchase;

import sf.core.exception.SugarException;

/**
 * Created by wb on 2016/3/7.
 */
public interface SignVaildator {

	/**
	 * 验证签到
	 * 
	 * @param signId
	 * @param memberId
	 * @throws SugarException
	 */
	public void validSign(Integer signId, Integer memberId) throws SugarException;

}
