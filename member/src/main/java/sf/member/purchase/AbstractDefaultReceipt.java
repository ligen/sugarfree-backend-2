package sf.member.purchase;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sf.core.entity.TfCoupon;
import sf.core.entity.TfReceipt;
import sf.core.entity.TfSign;
import sf.core.exception.SugarException;
import sf.core.mapper.TfCouponMapper;
import sf.core.mapper.TfSignMapper;
import sf.core.mapper.TuMemberMapper;
import sf.member.common.CouponStatus;

/**
 * Created by wb on 2016/3/3.
 */
public abstract class AbstractDefaultReceipt implements CouponValidator, ReceiptValidator, SignVaildator {

	@Autowired
	private TfCouponMapper couponMapper;

	@Autowired
	private TuMemberMapper memberMapper;

	@Autowired
	private TfSignMapper signMapper;

	/**
	 * @param couponId
	 * @return
	 * @throws SugarException
	 */
	public void vaildCoupon(Integer couponId, Integer memberId) throws SugarException {

		if (memberMapper.selectByPrimaryKey(memberId) == null) {
			throw new SugarException(5021, "传入用户有误");
		}
		if (couponId == null) {
			return;
		}
		/* 验证优惠券是否有效 */
		TfCoupon coupon = couponMapper.selectByPrimaryKey(couponId);
		if (coupon == null) {
			throw new SugarException(5022, "优惠券不存在");
		}
		if (coupon.getStatus() == CouponStatus.EXPIRED.getIndex()) {
			throw new SugarException(5023, "优惠券已过期");
		}
		if (coupon.getStatus() == CouponStatus.USED.getIndex()) {
			throw new SugarException(5024, "优惠券已使用");
		}
	}

	/**
	 * 更改含有优惠券订单
	 *
	 * @param couponId
	 * @param receipt
	 */
	@Transactional
	public void changeReceiptAndCouponStatus(Integer couponId, TfReceipt receipt) {

		TfCoupon coupon = couponMapper.selectByPrimaryKey(couponId);
		Integer beanInt = coupon.getBeansint();
		receipt.setCouponid(coupon.getId());
		receipt.setPaidinbeanint(receipt.getNumber() - beanInt);
		receipt.setPaidincouponint(beanInt);
		coupon.setStatus(new Integer(CouponStatus.USED.getIndex()).byteValue());
		couponMapper.updateByPrimaryKeySelective(coupon);
		if (beanInt >= receipt.getNumber()) {
			receipt.setPaidinbeanint(0);
		} else {
			receipt.setPaidinbeanint(receipt.getNumber() - beanInt);
		}
	}

	/**
	 * 购买
	 *
	 * @param packageId
	 * @param couponId
	 * @throws SugarException
	 */
	public abstract TfReceipt purchase(Integer packageId, Integer couponId, Integer memberId) throws SugarException;

	/**
	 * 生成签到码
	 * 
	 * @param receipt
	 * @param packageId
	 * @param memberId
	 * @throws SugarException
	 */
	public abstract void addSign(Integer receipt, Integer packageId, Integer memberId) throws SugarException;

	/**
	 * 扫码签到
	 * 
	 * @param signId
	 * @param memberId
	 * @throws SugarException
	 */
	public abstract void scanSign(Integer signId, Integer memberId) throws SugarException;

	@Override
	@Transactional
	public void validSign(Integer signId, Integer memberId) throws SugarException {

		TfSign sign = signMapper.selectByPrimaryKey(signId);
		if (sign == null) {
			throw new SugarException(7000, "参数传递有误");
		}
		if (sign.getUid() != memberId) {
			throw new SugarException(7001, "非法用户");
		}
		if (sign.getIssign() == 1) {
			throw new SugarException(7002, "已经签到过了");
		}
		sign.setIssign(1);
		sign.setSigndate(new Date());
		signMapper.updateByPrimaryKeySelective(sign);
	}

	public void buy(Integer packageId, Integer couponId, Integer memberId) throws SugarException {
		vaildReceipt(packageId, couponId, memberId);
		purchase(packageId, couponId, memberId);
	}

	public abstract int queryPrice(int packageId) throws SugarException;
}
