package sf.member.purchase;

import sf.core.exception.SugarException;

/**
 * Created by wb on 2016/3/3.
 */
public interface CouponValidator {

	/**
	 * 优惠券校验
	 * 
	 * @param memberId
	 * @param couponId
	 * @throws SugarException
	 */
	public void vaildCoupon(Integer memberId, Integer couponId) throws SugarException;

}
