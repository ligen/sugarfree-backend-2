package sf.member.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

@SuppressWarnings("rawtypes")
public interface MemmberPackageMapper {

	/**
	 * 查询场馆主页
	 *
	 * @param lat
	 * @param lng
	 * @param distance
	 * @param regionId
	 * @return
	 */
	public List<Map<String, Object>> queryGym(@Param("lat") Double lat, @Param("lng") Double lng,
			@Param("distance") Integer distance, @Param("regionId") Integer regionId);

	/**
	 * 查询课程包主页
	 *
	 * @return
	 */
	public List<Map<String, Object>> queryPackage(@Param("lat") Double lat, @Param("lng") Double lng,
			@Param("regionId") Integer regionId, @Param("distance") Integer distance,
			@Param("classType") Integer classType);

	/**
	 * 查询订单
	 *
	 * @return
	 */
	public List queryReceipt(@Param("memberId") Integer memberId, @Param("gymId") Integer gymId,
			@Param("type") Integer type, @Param("status") Integer status);

	/**
	 * 查询订单明细
	 *
	 * @param rid
	 * @return
	 */
	public List queryReceiptDetail(@Param("id") Integer rid);

	/**
	 * 查询评论
	 *
	 * @param rid
	 * @param packageId
	 * @param gymId
	 * @return
	 */
	public List queryComment(@Param("uid") Integer uid, @Param("rid") Integer rid,
			@Param("packageId") Integer packageId, @Param("gymId") Integer gymId);

	/**
	 * 查询场馆设备图片
	 *
	 * @param values
	 * @return
	 */
	public List queryGymDevices(List<Integer> values);

	/**
	 * 查询课程表收藏
	 *
	 * @param uid
	 * @return
	 */
	public List queryPackageFavor(@Param("uid") Integer uid);

	/**
	 * 查询场馆收藏
	 *
	 * @param uid
	 * @return
	 */
	public List queryGymFavor(@Param("uid") Integer uid);

	/**
	 * 查询banners
	 * 
	 * @return
	 */
	public List queryBanners();

	/**
	 * 查询教练资质
	 * 
	 * @param values
	 * @return
	 */
	public String queryCoachLevel(List<Integer> values);

	/**
	 * 查询图片
	 * 
	 * @param type
	 * @param xid
	 * @return
	 */
	public String queryImages(@Param("type") int type, @Param("xid") int xid);

}
