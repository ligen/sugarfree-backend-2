package sf.member.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sf.core.entity.TuCoach;
import sf.core.exception.SugarException;
import sf.core.mapper.TuCoachMapper;
import sf.core.mapper.TuGymMapper;
import sf.member.mapper.MemmberPackageMapper;

/**
 * Created by wb on 2016/3/10.
 */
@SuppressWarnings("rawtypes")
@Service
public class CommonBaseService {

	@Autowired
	private TuGymMapper gymMapper;

	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Autowired
	private TuCoachMapper coachMapper;

	/**
	 * 查询场馆设备
	 * 
	 * @param gymId
	 * @return
	 * @throws SugarException
	 */

	public List queryGymDeviceById(Integer gymId) throws SugarException {

		Integer devices = gymMapper.selectByPrimaryKey(gymId).getDevices();
		List<Integer> integers = queryDeviceSequence(devices);
		return memmberPackageMapper.queryGymDevices(integers);
	}

	private List<Integer> queryDeviceSequence(int result) {

		List<Integer> list = new ArrayList<Integer>();
		int j = 0;
		for (int i = 0; i < 8; ++i) {
			j = (result >>> i) % 2; // 取出右移的那一位
			j = j << i; // j * 2的次幂 = 当前的十进制数
			if (j > 0) {
				list.add(j);
			}
		}
		return list;
	}

	public Map queryCoachLevels(Integer coachId) throws SugarException {

		TuCoach coach = coachMapper.selectByPrimaryKey(coachId);
		Map<String, Object> map = new HashMap<String, Object>();
		if (coach == null) {
			return map;
		}
		map.put("id", coach.getId());
		map.put("name", coach.getName());
		map.put("avatar", coach.getAvatar());
		map.put("description", coach.getDescription());
		Integer levels = coach.getLevels();
		if (levels == null) {
			return map;
		}
		List<Integer> integers = queryDeviceSequence(levels);
		map.put("levels", memmberPackageMapper.queryCoachLevel(integers));
		return map;
	}

}
