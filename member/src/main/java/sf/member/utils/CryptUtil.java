package sf.member.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Created by ligen on 16-3-2.
 */
public class CryptUtil {

	// public static class Token {
	// String token;
	//
	// public Token(String token) {
	// this.token = token;
	// }
	//
	// public String getToken() {
	// return token;
	// }
	// }
	//
	// private static final JWSHeader JWS_HEADER = new
	// JWSHeader(JWSAlgorithm.HS256);
	// private static final String TOKEN_SECRET =
	// "LigenLiuchengwanLiyongYuleWutunHuangshisui";
	// public static final String AUTH_HEADER_KEY = "Authorization";
	// public static final String SUBJECT_NAME = "uid";
	//
	// public static Token createToken(String sub) throws JOSEException {
	// JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
	// .subject(sub)
	// .issueTime(DateTime.now().toDate())
	// .expirationTime(DateTime.now().plusDays(14).toDate())
	// .build();
	//
	// JWSSigner signer = new MACSigner(TOKEN_SECRET);
	// SignedJWT jwt = new SignedJWT(JWS_HEADER, claimsSet);
	// jwt.sign(signer);
	//
	// return new Token(jwt.serialize());
	// }
	//
	// public static JWTClaimsSet decodeToken(String authHeader) throws
	// ParseException, JOSEException {
	//
	// SignedJWT signedJWT = SignedJWT.parse(authHeader);
	// if(signedJWT.verify(new MACVerifier(TOKEN_SECRET))){
	// return signedJWT.getJWTClaimsSet();
	// } else {
	// throw new JOSEException("Signature verified failed");
	// }
	// }
	//
	// public static String getSubject(String authHeader) throws ParseException,
	// JOSEException {
	// return decodeToken(authHeader).getSubject();
	// }

	public static String hash(String raw, String salt) {
		return DigestUtils.md5Hex(raw + salt);
	}

}
