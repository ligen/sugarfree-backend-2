package sf.member.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import sf.core.common.DefaultPageList;
import sf.core.controller.BaseController;
import sf.core.entity.TfReceipt;
import sf.core.entity.TfSign;
import sf.core.exception.SugarException;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TfSignMapper;
import sf.member.common.PackageType;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.purchase.PrivateReceiptProcesor;
import sf.member.purchase.TrainReceiptProcesor;

/**
 * Created by wb on 2016/3/7.
 */
@SuppressWarnings("rawtypes")
@RequestMapping("/scan")
@ResponseBody
@Controller
public class SignCodeController extends BaseController {

	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Autowired
	private TfSignMapper signMapper;

	@Autowired
	private TfReceiptMapper receiptMapper;

	@Autowired
	private PrivateReceiptProcesor privateReceiptProcesor;

	@Autowired
	private TrainReceiptProcesor trainReceiptProcesor;

	/**
	 * 查询场馆订单
	 *
	 * @param gymId
	 * @param page
	 * @param limit
	 * @return
	 * @throws SugarException
	 */
	@RequestMapping("/list")
	public DefaultPageList queryReceipt(@RequestParam(required = true) Integer memberId,
			@RequestParam(required = true) Integer gymId,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		return createPageList(pages, memmberPackageMapper.queryReceipt(memberId, gymId, null, null));
	}

	/**
	 * 查询场馆订单详情
	 *
	 * @param id
	 * @return
	 * @throws SugarException
	 */
	@RequestMapping("/list/{id}")
	public List queryReceiptById(@PathVariable Integer id) throws SugarException {
		return memmberPackageMapper.queryReceiptDetail(id);
	}

	@RequestMapping("/sign")
	public void sign(@RequestParam(required = true) Integer signId, @RequestParam(required = true) Integer memberId)
			throws SugarException {
		TfSign sign = signMapper.selectByPrimaryKey(signId);
		TfReceipt receipt = receiptMapper.selectByPrimaryKey(sign.getRid());
		PackageType packageType = PackageType.getInstance(receipt.getType());
		switch (packageType) {
		case PRIVATE_PACKAGE: {
			privateReceiptProcesor.scanSign(signId, memberId);
			break;
		}
		case TRAIN_PACKAGE: {
			trainReceiptProcesor.scanSign(signId, memberId);
			break;
		}
		}
	}

}
