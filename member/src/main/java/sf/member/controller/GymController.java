package sf.member.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import sf.core.common.DefaultPageList;
import sf.core.controller.BaseController;
import sf.core.entity.TfPackage;
import sf.core.entity.TuCoach;
import sf.core.entity.TuContract;
import sf.core.entity.TuGym;
import sf.core.exception.SugarException;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TuCoachMapper;
import sf.core.mapper.TuContractMapper;
import sf.core.mapper.TuGymMapper;
import sf.member.common.ImageStatus;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.service.CommonBaseService;

/**
 * Created by wb on 2016/3/7.
 */

@RequestMapping("/gym")
@Controller
public class GymController extends BaseController {

	// <<<<<<< HEAD
	//
	// @Autowired
	// private MemmberPackageMapper memmberPackageMapper;
	//
	// @Autowired
	// private TuGymMapper gymMapper;
	//
	// @Autowired
	// private TfCommentMapper commentMapper;
	//
	// @Autowired
	// private TfPackageMapper packageMapper;
	//
	// @Autowired
	// private TuContractMapper contractMapper;
	//
	// @Autowired
	// private TuCoachMapper coachMapper;
	//
	// @Autowired
	// private TfLessonMapper lessonMapper;
	//
	// @Autowired
	// private CommonBaseService commonBaseService;
	//
	//
	// /**
	// * 查询场馆主页
	// *
	// * @param
	// * @return
	// */
	// @RequestMapping("/index")
	// public ModelAndView queryGymJsp(@RequestParam(required = false) Double
	// lat, @RequestParam(required = false) Double lng,
	// @RequestParam(required=false) String address,@RequestParam(required =
	// false) Integer regionId,
	// @RequestParam(required = false,defaultValue = "1000") Integer distance,
	// @RequestParam(required = false, defaultValue = "1") Integer page,
	// @RequestParam(required = false, defaultValue = "3") Integer limit) throws
	// SugarException {
	// Page<Object> pages = PageHelper.startPage(page, limit);
	// List<Map<String, Object>> maps = memmberPackageMapper.queryGym(lat, lng,
	// distance, regionId);
	// ModelAndView mv = new ModelAndView();
	// mv.addObject("pageAndList", createPageList(pages, maps));
	// mv.addObject("address", address);
	// mv.addObject("banners", memmberPackageMapper.queryBanners());
	// mv.setViewName("gymList");
	// return mv;
	// }
	//
	// /**
	// * 查询场馆主页
	// *
	// * @param
	// * @return
	// */
	// @RequestMapping("/extends")
	// @ResponseBody
	// public DefaultPageList queryGym(@RequestParam(required = false) Double
	// lat,
	// @RequestParam(required = false) Double lng, @RequestParam(required =
	// false) Integer regionId,
	// @RequestParam(required = false,defaultValue = "1000") Integer distance,
	// @RequestParam(required = false, defaultValue = "1") Integer page,
	// @RequestParam(required = false, defaultValue = "3") Integer limit) throws
	// SugarException {
	//
	// Page<Object> pages = PageHelper.startPage(page, limit);
	// List<Map<String, Object>> maps = memmberPackageMapper.queryGym(lat, lng,
	// distance, regionId);
	// return createPageList(pages, maps);
	// }
	//
	//
	// /* @RequestMapping("/{id}")
	// public Map<String, Object> queryGymByDetail(@PathVariable Integer id)
	// throws SugarException {
	// Map<String, Object> mv = new HashMap<String, Object>();
	// mv.put("detail", queryGymById(id));
	// mv.put("packages", queryGymPackageById(id, 1, 10));
	// mv.put("coach", queryGymCoachById(id));
	// mv.put("comment", queryGymComentById(id, 1, 10));
	// return mv;
	// }*/
	//
	// @RequestMapping("/{id}")
	// public ModelAndView queryGymByDetail(@PathVariable Integer id) throws
	// SugarException {
	// ModelAndView mv = new ModelAndView();
	// mv.addObject("detail", queryGymById(id));
	// mv.addObject("packages", queryGymPackageById(id, 1, 10));
	// mv.addObject("coach", queryGymCoachById(id));
	// mv.addObject("comments", queryGymComentById(id, 1, 10));
	// mv.setViewName("gymDetail");
	// return mv;
	// }
	//
	//
	// public Map queryGymById(Integer id) throws SugarException {
	// Map map = new HashMap();
	// TuGym gym = gymMapper.selectByPrimaryKey(id);
	// map.put("id", gym.getId());
	// map.put("name", gym.getName());
	// map.put("address", gym.getAddress());
	// map.put("logo", gym.getLogo());
	// map.put("description", gym.getDescription());
	// map.put("lat", gym.getLat());
	// map.put("lng", gym.getLng());
	// map.put("envelopUrls",
	// memmberPackageMapper.queryImages(ImageStatus.GYM_ENVOLOP,gym.getId()));
	// map.put("gymUrls",
	// memmberPackageMapper.queryImages(ImageStatus.TYPE_GYM,gym.getId()));
	// return map;
	// }
	//
	// /*@RequestMapping("/{id}")
	// public Map queryGymDetail(@PathVariable Integer id) throws SugarException
	// {
	//
	// Map map = new HashMap();
	// map.put("detail", queryGymById(id));
	// map.put("packages", queryGymPackageById(id, 1, 10));
	// map.put("coach", queryGymCoachById(id));
	// map.put("comment", queryGymComentById(id, 1, 10));
	// return map;
	// }*/
	//
	// @RequestMapping("/{id}/package")
	// public DefaultPageList queryGymPackageById(@PathVariable Integer id,
	// @RequestParam(required = false, defaultValue = "1") Integer page,
	// @RequestParam(required = false, defaultValue = "10") Integer limit)
	// throws SugarException {
	// Page<Object> pages = PageHelper.startPage(page, limit);
	// TfPackage query = new TfPackage();
	// query.setGymid(id);
	// List<TfPackage> packages = packageMapper.select(query);
	// List list = new ArrayList();
	// for (TfPackage tfPackage : packages) {
	// Map map = new HashMap();
	// map.put("id", tfPackage.getId());
	// map.put("title", tfPackage.getTitle());
	// map.put("priceint", tfPackage.getPriceint());
	// map.put("begintime", tfPackage.getBegintime());
	// map.put("endtime", tfPackage.getEndtime());
	// map.put("classtype", tfPackage.getClasstype());
	// list.add(map);
	// }
	// return createPageList(pages, list);
	// }
	//
	// @RequestMapping("/{id}/comment")
	// @ResponseBody
	// public DefaultPageList queryGymComentById(@PathVariable Integer id,
	// @RequestParam(required = false, defaultValue = "1") Integer page,
	// @RequestParam(required = false, defaultValue = "3") Integer limit) throws
	// SugarException {
	// Page<Object> pages = PageHelper.startPage(page, limit);
	// List List = memmberPackageMapper.queryComment(null, null, id);
	// return createPageList(pages, List);
	// }
	//
	//
	// public Map queryGymComentById(@PathVariable Integer id) throws
	// SugarException {
	//
	// TuGym tuGym = gymMapper.selectByPrimaryKey(id);
	// TuContract tuContract =
	// contractMapper.selectByPrimaryKey(tuGym.getContactid());
	// TuCoach coach = coachMapper.selectByPrimaryKey(tuContract.getCoachid());
	// Map map = new HashMap();
	// map.put("id", coach.getId());
	// map.put("name", coach.getName());
	// map.put("avatar", coach.getAvatar());
	// map.put("description", coach.getDescription());
	// return map;
	// }
	//
	//
	// public Map queryGymCoachById(Integer id) throws SugarException {
	// TuGym tuGym = gymMapper.selectByPrimaryKey(id);
	// TuContract tuContract =
	// contractMapper.selectByPrimaryKey(tuGym.getContactid());
	// return commonBaseService.queryCoachLevels(tuContract.getCoachid());
	// }
	// =======
	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Autowired
	private TuGymMapper gymMapper;

	@Autowired
	private TfPackageMapper packageMapper;

	@Autowired
	private TuContractMapper contractMapper;

	@Autowired
	private TuCoachMapper coachMapper;

	@Autowired
	private CommonBaseService commonBaseService;

	/**
	 * 查询场馆主页
	 *
	 * @param
	 * @return
	 */
	@RequestMapping("/index")
	public ModelAndView queryGymJsp(@RequestParam(required = false) Double lat,
			@RequestParam(required = false) Double lng, @RequestParam(required = false) String address,
			@RequestParam(required = false) Integer regionId,
			@RequestParam(required = false, defaultValue = "1000") Integer distance,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		List<Map<String, Object>> maps = memmberPackageMapper.queryGym(lat, lng, distance, regionId);
		ModelAndView mv = new ModelAndView();
		mv.addObject("pageAndList", createPageList(pages, maps));
		mv.addObject("address", address);
		mv.addObject("banners", memmberPackageMapper.queryBanners());
		mv.setViewName("gymList");
		return mv;
	}

	/**
	 * 查询场馆主页
	 *
	 * @param
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping("/extends")
	@ResponseBody
	public DefaultPageList queryGym(@RequestParam(required = false) Double lat,
			@RequestParam(required = false) Double lng, @RequestParam(required = false) Integer regionId,
			@RequestParam(required = false, defaultValue = "1000") Integer distance,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {

		Page<Object> pages = PageHelper.startPage(page, limit);
		List<Map<String, Object>> maps = memmberPackageMapper.queryGym(lat, lng, distance, regionId);
		return createPageList(pages, maps);
	}

	@RequestMapping("/{id}")
	public Map<String, Object> queryGymByDetail(@PathVariable Integer id) throws SugarException {
		Map<String, Object> mv = new HashMap<String, Object>();
		mv.put("detail", queryGymById(id));
		mv.put("packages", queryGymPackageById(id, 1, 10));
		mv.put("coach", queryGymCoachById(id));
		mv.put("comment", queryGymComentById(id, 1, 10));
		return mv;
	}

	/*
	 * @RequestMapping("/{id}") public ModelAndView
	 * queryGymByDetail(@PathVariable Integer id) throws SugarException {
	 * ModelAndView mv = new ModelAndView(); mv.addObject("detail",
	 * queryGymById(id)); mv.addObject("packages", queryGymPackageById(id, 1,
	 * 10)); mv.addObject("coach", queryGymCoachById(id));
	 * mv.addObject("comments", queryGymComentById(id, 1, 10));
	 * mv.setViewName("gymDetail"); return mv; }
	 */

	public Map<String, Object> queryGymById(Integer id) throws SugarException {
		Map<String, Object> map = new HashMap<String, Object>();
		TuGym gym = gymMapper.selectByPrimaryKey(id);
		map.put("id", gym.getId());
		map.put("name", gym.getName());
		map.put("address", gym.getAddress());
		map.put("logo", gym.getLogo());
		map.put("description", gym.getDescription());
		map.put("lat", gym.getLat());
		map.put("lng", gym.getLng());
		map.put("envelopUrls", memmberPackageMapper.queryImages(ImageStatus.GYM_ENVOLOP, gym.getId()));
		map.put("gymUrls", memmberPackageMapper.queryImages(ImageStatus.TYPE_GYM, gym.getId()));
		return map;
	}

	/*
	 * @RequestMapping("/{id}") public Map queryGymDetail(@PathVariable Integer
	 * id) throws SugarException {
	 * 
	 * Map map = new HashMap(); map.put("detail", queryGymById(id));
	 * map.put("packages", queryGymPackageById(id, 1, 10)); map.put("coach",
	 * queryGymCoachById(id)); map.put("comment", queryGymComentById(id, 1,
	 * 10)); return map; }
	 */

	@SuppressWarnings("rawtypes")
	@RequestMapping("/{id}/package")
	public DefaultPageList queryGymPackageById(@PathVariable Integer id,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		TfPackage query = new TfPackage();
		query.setGymid(id);
		List<TfPackage> packages = packageMapper.select(query);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (TfPackage tfPackage : packages) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", tfPackage.getId());
			map.put("title", tfPackage.getTitle());
			map.put("priceint", tfPackage.getPriceint());
			map.put("begintime", tfPackage.getBegintime());
			map.put("endtime", tfPackage.getEndtime());
			map.put("classtype", tfPackage.getClasstype());
			list.add(map);
		}
		return createPageList(pages, list);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/{id}/comment")
	@ResponseBody
	public DefaultPageList queryGymComentById(@PathVariable Integer id,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		List List = memmberPackageMapper.queryComment(null, null, null, id);
		return createPageList(pages, List);
	}

	public Map<String, Object> queryGymComentById(@PathVariable Integer id) throws SugarException {

		TuGym tuGym = gymMapper.selectByPrimaryKey(id);
		TuContract tuContract = contractMapper.selectByPrimaryKey(tuGym.getContactid());
		TuCoach coach = coachMapper.selectByPrimaryKey(tuContract.getCoachid());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", coach.getId());
		map.put("name", coach.getName());
		map.put("avatar", coach.getAvatar());
		map.put("description", coach.getDescription());
		return map;
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> queryGymCoachById(Integer id) throws SugarException {
		TuGym tuGym = gymMapper.selectByPrimaryKey(id);
		TuContract tuContract = contractMapper.selectByPrimaryKey(tuGym.getContactid());
		return commonBaseService.queryCoachLevels(tuContract.getCoachid());
	}

}
