package sf.member.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import sf.core.common.DefaultPageList;
import sf.core.controller.BaseController;
import sf.core.entity.TfFavor;
import sf.core.exception.SugarException;
import sf.core.mapper.TfFavorMapper;
import sf.member.mapper.MemmberPackageMapper;

/**
 * Created by wb on 2016/3/11.
 */
@RequestMapping("/favor")
@ResponseBody
@Controller
public class FavorController extends BaseController {

	public static final int GYM_TYPE = 0;

	public static final int PACKAGE_TYPE = 1;

	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Autowired
	private TfFavorMapper favorMapper;

	@RequestMapping("/add")
	public void add(@RequestParam int uid, @RequestParam Integer type, @RequestParam int xid) throws SugarException {

		TfFavor query = new TfFavor();
		query.setUid(uid);
		query.setType(type.byteValue());
		query.setXid(xid);
		TfFavor tfFavor = favorMapper.selectOne(query);
		if (tfFavor != null) {
			throw new SugarException(9000, "已经收藏过");
		}
		favorMapper.insertSelective(query);
	}

	@RequestMapping("/delete")
	public void delete(@RequestParam int id) throws SugarException {
		favorMapper.deleteByPrimaryKey(id);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping("/list")
	public DefaultPageList list(@RequestParam(required = true) int uid, @RequestParam(required = true) Integer type,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		List list = null;
		if (type.equals(GYM_TYPE)) {
			list = memmberPackageMapper.queryGymFavor(uid);
		} else if (type.equals(PACKAGE_TYPE)) {
			list = memmberPackageMapper.queryPackageFavor(uid);
		}
		return createPageList(pages, list);
	}
}
