package sf.member.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import sf.core.common.DefaultPageList;
import sf.core.controller.BaseController;
import sf.core.entity.TfLesson;
import sf.core.entity.TfPackage;
import sf.core.entity.TuGym;
import sf.core.exception.SugarException;
import sf.core.mapper.TfLessonMapper;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TuGymMapper;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.service.CommonBaseService;

/**
 * Created by wb on 2016/3/7.
 */
@SuppressWarnings("rawtypes")
@RequestMapping("/package")
@ResponseBody
@Controller
public class PackageController extends BaseController {

	public static final String pattern = "yyyyMMddHH:mm:ss";

	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Autowired
	private TuGymMapper gymMapper;

	@Autowired
	private TfPackageMapper packageMapper;

	@Autowired
	private TfLessonMapper lessonMapper;

	@Autowired
	private CommonBaseService commonBaseService;

	/**
	 * 查询课程包主页
	 *
	 * @return
	 * @throws UnsupportedEncodingException
	 */

	@RequestMapping(value = "/index", method = RequestMethod.POST)
	public ModelAndView queryPackageJsp(@RequestParam(required = false) Double lat,
			@RequestParam(required = false) Double lng, @RequestParam(required = false) String address,
			@RequestParam(required = false) Integer regionId,
			@RequestParam(required = false, defaultValue = "1000") Integer distance,
			@RequestParam(required = false) Integer classType,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "3") Integer limit)
					throws SugarException, UnsupportedEncodingException {

		Page<Object> pages = PageHelper.startPage(page, limit);
		List<Map<String, Object>> maps = memmberPackageMapper.queryPackage(lat, lng, regionId, distance, classType);
		ModelAndView mv = new ModelAndView();
		mv.addObject("pageAndList", createPageList(pages, maps));
		mv.addObject("address", address);
		mv.addObject("banners", memmberPackageMapper.queryBanners());
		mv.setViewName("packageList");
		return mv;
	}

	@RequestMapping(value = "/index1", method = RequestMethod.GET)
	public ModelAndView queryPackageJsp1(@RequestParam(required = false) Double lat,
			@RequestParam(required = false) Double lng, @RequestParam(required = false) String address,
			@RequestParam(required = false) Integer regionId,
			@RequestParam(required = false, defaultValue = "1000") Integer distance,
			@RequestParam(required = false) Integer classType,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "3") Integer limit)
					throws SugarException, UnsupportedEncodingException {

		Page<Object> pages = PageHelper.startPage(page, limit);
		List<Map<String, Object>> maps = memmberPackageMapper.queryPackage(lat, lng, regionId, distance, classType);
		ModelAndView mv = new ModelAndView();
		mv.addObject("pageAndList", createPageList(pages, maps));
		mv.addObject("address", address);
		mv.addObject("banners", memmberPackageMapper.queryBanners());
		mv.setViewName("packageList");
		return mv;
	}

	/*
	 * @RequestMapping(value="/index",method= RequestMethod.POST) public Map
	 * queryPackageJsp(@RequestParam(required = false) Double
	 * lat, @RequestParam(required = false) Double lng,
	 * 
	 * @RequestParam(required=false) String address, @RequestParam(required =
	 * false) Integer regionId,
	 * 
	 * @RequestParam(required = false,defaultValue = "1000") Integer
	 * distance, @RequestParam(required = false) Integer classType,
	 * 
	 * @RequestParam(required = false, defaultValue = "1") Integer page,
	 * 
	 * @RequestParam(required = false, defaultValue = "10") Integer limit)
	 * throws SugarException, UnsupportedEncodingException {
	 * 
	 * Page<Object> pages = PageHelper.startPage(page, limit); List<Map<String,
	 * Object>> maps = memmberPackageMapper.queryPackage(lat, lng, regionId,
	 * distance, classType); Map mv = new HashMap(); mv.put("pageAndList",
	 * createPageList(pages, maps)); mv.put("address", address);
	 * mv.put("banners", memmberPackageMapper.queryBanners()); return mv; }
	 */

	/**
	 * 查询课程包主页
	 *
	 * @return
	 */
	@RequestMapping("/exetends")
	@ResponseBody
	public DefaultPageList queryPackage(@RequestParam(required = false) Double lat,
			@RequestParam(required = false) Double lng, @RequestParam(required = false) Integer regionId,
			@RequestParam(required = false, defaultValue = "1000") Integer distance,
			@RequestParam(required = false) Integer classType,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "3") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		List<Map<String, Object>> maps = memmberPackageMapper.queryPackage(lat, lng, regionId, distance, classType);
		return createPageList(pages, maps);
	}

	/*
	 * @RequestMapping("/{id}") public ModelAndView
	 * queryPackageDetail(@PathVariable Integer id) throws SugarException{
	 * 
	 * ModelAndView mv = new ModelAndView();
	 * mv.addObject("packageInfo",queryPackageById(id));
	 * mv.addObject("lessons",queryPackageLessonsById(id,1,10));
	 * mv.addObject("coach",queryPackageCoachById(id));
	 * mv.addObject("devices",queryPackageDevice(id));
	 * mv.addObject("comments",queryPackageCommentById(id,1,10));
	 * mv.addObject("gym",queryGymById(id)); mv.setViewName("packageDetail");
	 * return mv; }
	 */

	@RequestMapping("/{id}")
	public Map queryPackageDetail(@PathVariable Integer id) throws SugarException {
		Map<String, Object> mv = new HashMap<String, Object>();
		mv.put("packageInfo", queryPackageById(id));
		mv.put("lessons", queryPackageLessonsById(id, 1, 3));
		mv.put("coach", queryPackageCoachById(id));
		mv.put("devices", queryPackageDevice(id));
		mv.put("comments", queryPackageCommentById(id, 1, 3));
		return mv;
	}

	/**
	 * 查询设备
	 * 
	 * @param id
	 * @return
	 * @throws SugarException
	 */
	private List queryPackageDevice(Integer id) throws SugarException {

		Integer gymid = packageMapper.selectByPrimaryKey(id).getGymid();
		return commonBaseService.queryGymDeviceById(gymid);
	}

	/**
	 * 查询课程包信息
	 * 
	 * @param id
	 * @return
	 * @throws SugarException
	 */
	private Map queryPackageById(Integer id) throws SugarException {
		TfPackage tfPackage = packageMapper.selectByPrimaryKey(id);
		Map<String, Object> map = new HashMap<String, Object>();
		if (tfPackage == null) {
			return map;
		}
		map.put("id", tfPackage.getId());
		map.put("title", tfPackage.getTitle());
		map.put("classtype", tfPackage.getClasstype());
		map.put("endtime", tfPackage.getEndtime());
		map.put("numberlimit", tfPackage.getNumberlimit());
		map.put("realnumber", tfPackage.getRealnumber());
		map.put("description", tfPackage.getDescription());
		map.put("priceInt", tfPackage.getPriceint());
		return map;
	}

	/**
	 * 查询场馆
	 * 
	 * @param id
	 * @return
	 * @throws SugarException
	 */
	public Map queryGymById(Integer id) throws SugarException {
		Map<String, Object> map = new HashMap<String, Object>();
		TfPackage tfPackage = packageMapper.selectByPrimaryKey(id);
		if (tfPackage == null) {
			return map;
		}
		TuGym tuGym = gymMapper.selectByPrimaryKey(tfPackage.getGymid());
		if (tuGym == null) {
			return map;
		}
		map.put("lat", tuGym.getLat());
		map.put("lng", tuGym.getLng());
		map.put("address", tuGym.getAddress());
		return map;
	}

	@RequestMapping("/{id}/lessons")
	@ResponseBody
	public DefaultPageList queryPackageLessonsById(@PathVariable Integer id,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		TfLesson query = new TfLesson();
		query.setPackageid(id);
		List<TfLesson> lessons = lessonMapper.select(query);
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (TfLesson lesson : lessons) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", lesson.getId());
			map.put("title", lesson.getTitle());
			map.put("description", lesson.getDescription());
			map.put("begintime", lesson.getBegintime());
			map.put("endtime", lesson.getEndtime());
			map.put("price", lesson.getPrice());
			map.put("image", lesson.getImage());
			map.put("lessons", lesson.getLessons());
			list.add(map);
		}
		return createPageList(pages, list);
	}

	public Map queryPackageCoachById(Integer id) throws SugarException {
		TfPackage tfpackage = packageMapper.selectByPrimaryKey(id);
		return commonBaseService.queryCoachLevels(tfpackage.getCoachid());
	}

	@RequestMapping("/{id}/comment")
	@ResponseBody
	public DefaultPageList queryPackageCommentById(@PathVariable Integer id,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		List list = memmberPackageMapper.queryComment(null, null, id, null);
		return createPageList(pages, list);
	}
}
