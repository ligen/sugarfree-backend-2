package sf.member.controller;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;

/**
 * Created by wb on 2016/3/4.
 */

import com.github.pagehelper.PageHelper;

import sf.core.common.DefaultPageList;
import sf.core.controller.BaseController;
import sf.core.entity.TfComment;
import sf.core.entity.TfReceipt;
import sf.core.exception.SugarException;
import sf.core.mapper.TfCommentMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.service.AuthService;
import sf.core.service.StorageService;
import sf.member.common.ImageStatus;
import sf.member.common.OrderStatus;
import sf.member.common.PackageType;
import sf.member.mapper.MemmberPackageMapper;

/***
 * 评论
 */
@RequestMapping("/comment")
@ResponseBody
@Controller
public class CommentController extends BaseController {

	@Autowired
	private TfCommentMapper commentMapper;

	@Autowired
	private TfReceiptMapper receiptMapper;

	@Autowired
	private StorageService storageService;

	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Resource
	private AuthService<Integer> authService;

	/**
	 * 评论校验
	 * 
	 * @param rid
	 * @param memberId
	 * @return
	 * @throws SugarException
	 */
	public TfReceipt vaildCommet(Integer rid, Integer memberId) throws SugarException {

		TfReceipt receipt = receiptMapper.selectByPrimaryKey(rid);
		if (receipt == null) {
			throw new SugarException(6000, "订单不存在");
		}
		if (receipt.getStatus() != OrderStatus.OVERED.getIndex()) {
			throw new SugarException(6001, "订单没有结束");
		}
		if (receipt.getMemberid() != memberId) {
			throw new SugarException(6002, "非法用户");
		}
		return receipt;
	}

	/***
	 * 创建评论
	 *
	 * @param rid
	 * @throws SugarException
	 */
	@RequestMapping(value = "/create")
	public void create(@RequestParam(required = true) Integer rid, @RequestParam(required = true) Integer content,
			@RequestParam(required = true) Integer liked, HttpServletRequest request) throws SugarException {

		Integer uid = authService.getSubject(request);
		TfReceipt receipt = vaildCommet(rid, uid);
		TfComment comment = commentMapper.selectByPrimaryKey(rid);
		if (comment != null) {
			throw new SugarException(6003, "订单已经评论");
		}
		comment = new TfComment();
		comment.setRid(rid);
		comment.setUid(uid);
		comment.setPackageid(receipt.getPackageid());
		comment.setGymid(receipt.getGymid());
		comment.setLiked(liked);
		comment.setCreatedate(new Date());
		comment.setContent(content);
		commentMapper.insert(comment);
	}

	/**
	 * 评论图片上传
	 *
	 * @param rid
	 * @return
	 */
	@RequestMapping("/upload")
	public String upload(@RequestParam(required = true) Integer rid, HttpServletRequest request) throws SugarException {

		int status = 0;
		Integer uid = authService.getSubject(request);
		TfReceipt receipt = vaildCommet(rid, uid);
		if (receipt.getType() == PackageType.PRIVATE_PACKAGE.getIndex()) {
			status = ImageStatus.TYPE_PRIVATE_COMMENT_IMAGE;
		} else if (receipt.getType() == PackageType.TRAIN_PACKAGE.getIndex()) {
			status = ImageStatus.TYPE_TRAIN_COMMENT_IMAGE;
		}
		return storageService.getUploadImageToken(status, rid);
	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/list")
	public DefaultPageList queryComment(@RequestParam(required = false) Integer rid,
			@RequestParam(required = false) Integer packageId, @RequestParam(required = false) Integer gymId,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit, HttpServletRequest request)
					throws SugarException {

		Integer uid = authService.getSubject(request);
		Page<Object> pages = PageHelper.startPage(page, limit);
		return createPageList(pages, memmberPackageMapper.queryComment(uid, rid, packageId, gymId));
	}

}
