package sf.member.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import sf.core.controller.BaseController;
import sf.core.entity.TfPackage;
import sf.core.entity.TfReceipt;
import sf.core.entity.TuGym;
import sf.core.entity.TuMember;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TuGymMapper;
import sf.core.mapper.TuMemberMapper;

@RequestMapping("/invitation")
@Controller
public class InvitationController extends BaseController {

	public final static String IS_SHARED_PAGE = "1";

	@Resource
	TfReceiptMapper receiptMapper;

	@Resource
	TuMemberMapper memberMapper;

	@Resource
	TuGymMapper gymMapper;

	@Resource
	TfPackageMapper packageMapper;

	@RequestMapping("/show")
	public ModelAndView invitationShow(@RequestParam(required = true) Integer receiptId) {
		ModelAndView mv = new ModelAndView();
		TfReceipt receipt = receiptMapper.selectByPrimaryKey(receiptId);

		mv.addObject("memberInfo", queryMemberInfoByReceipt(receipt));
		mv.addObject("packageInfo", queryPackageInfoByReceipt(receipt));
		mv.addObject("gymInfo", queryGymInfoByReceipt(receipt));
		mv.setViewName("invitation");
		return mv;
	}

	@RequestMapping("/share/show")
	public ModelAndView invitationShareShow(@RequestParam(required = true) Integer invitationId) {
		ModelAndView mv = new ModelAndView();
		Integer receiptId = invitationId;
		TfReceipt receipt = receiptMapper.selectByPrimaryKey(receiptId);

		mv.addObject("memberInfo", queryMemberInfoByReceipt(receipt));
		mv.addObject("packageInfo", queryPackageInfoByReceipt(receipt));
		mv.addObject("gymInfo", queryGymInfoByReceipt(receipt));
		mv.addObject("isSharedPage", IS_SHARED_PAGE);
		mv.setViewName("invitation");
		return mv;
	}

	private Map<String, Object> queryMemberInfoByReceipt(TfReceipt receipt) {
		TuMember member = memberMapper.selectByPrimaryKey(receipt.getMemberid());
		Map<String, Object> memberMap = new HashMap<String, Object>();
		memberMap.put("name", member.getName());
		return memberMap;
	}

	private Map<String, Object> queryPackageInfoByReceipt(TfReceipt receipt) {
		TfPackage _package = packageMapper.selectByPrimaryKey(receipt.getPackageid());

		Map<String, Object> packageMap = new HashMap<String, Object>();
		packageMap.put("title", _package.getTitle());
		packageMap.put("beginTime", _package.getBegintime());
		packageMap.put("endTime", _package.getEndtime());
		packageMap.put("shareUrl", _package.getShareurl());
		return packageMap;
	}

	private Map<String, Object> queryGymInfoByReceipt(TfReceipt receipt) {
		TuGym gym = gymMapper.selectByPrimaryKey(receipt.getGymid());
		Map<String, Object> gymMap = new HashMap<String, Object>();
		gymMap.put("name", gym.getName());
		gymMap.put("address", gym.getAddress());
		return gymMap;
	}

}
