package sf.member.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import sf.core.common.DefaultPageList;
import sf.core.controller.BaseController;
import sf.core.entity.TfPackage;
import sf.core.exception.SugarException;
import sf.core.mapper.TfPackageMapper;
import sf.core.service.AuthService;
import sf.member.common.PackageType;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.purchase.PrivateReceiptProcesor;
import sf.member.purchase.TrainReceiptProcesor;

@SuppressWarnings("rawtypes")
@RequestMapping("/receipt")
@ResponseBody
@Controller
public class ReceiptController extends BaseController {

	@Autowired
	private PrivateReceiptProcesor privateReceiptProcesor;

	@Autowired
	private TrainReceiptProcesor trainReceiptProcesor;

	@Autowired
	private MemmberPackageMapper memmberPackageMapper;

	@Resource
	private AuthService<Integer> authService;

	@Autowired
	private TfPackageMapper packageMapper;

	/**
	 * 创建订单
	 *
	 * @param packageId
	 * @param couponId
	 * @throws SugarException
	 */
	@RequestMapping(value = "create", method = RequestMethod.POST)
	public void create(@RequestParam(required = true) Integer packageId,
			@RequestParam(required = false) Integer lessonId, @RequestParam(required = false) Integer couponId,
			HttpServletRequest request) throws SugarException {

		Integer memberId = authService.getSubject(request);
		TfPackage tfPackage = packageMapper.selectByPrimaryKey(packageId);
		PackageType packageType = PackageType.getInstance(tfPackage.getClasstype());
		switch (packageType) {
		case PRIVATE_PACKAGE: {
			privateReceiptProcesor.buy(lessonId, couponId, memberId);
			break;
		}
		case TRAIN_PACKAGE: {
			trainReceiptProcesor.buy(packageId, couponId, memberId);
			break;
		}
		}
	}

	/**
	 * 查询订单
	 *
	 * @return
	 */
	@RequestMapping(value = "list", method = RequestMethod.GET)
	public DefaultPageList queryReceipt(HttpServletRequest request, @RequestParam(required = true) Integer type,
			@RequestParam(required = true) Integer status,
			@RequestParam(required = false, defaultValue = "1") Integer page,
			@RequestParam(required = false, defaultValue = "10") Integer limit) throws SugarException {
		Page<Object> pages = PageHelper.startPage(page, limit);
		Integer memberId = authService.getSubject(request);
		return createPageList(pages, memmberPackageMapper.queryReceipt(memberId, null, type, status));
	}

	/**
	 * 查询订单明细
	 *
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public List queryReceiptDetail(@PathVariable Integer id) throws SugarException {
		return memmberPackageMapper.queryReceiptDetail(id);
	}

}
