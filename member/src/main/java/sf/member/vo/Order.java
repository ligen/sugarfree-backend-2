package sf.member.vo;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;

import sf.core.entity.TfOrder;

/**
 * Created by ligen on 16-3-8.
 */
public class Order {

	private Integer id;
	private String subject;

	@NotNull
	private Integer uid;

	@NotNull
	private Integer amount;

	private Integer cost;
	private Long sn;

	public Order(Integer uid, Integer amount, Integer cost, Long sn, String subject) {
		this.uid = uid;
		this.amount = amount;
		this.cost = cost;
		this.sn = sn;
		this.subject = subject;
	}

	public TfOrder toPo() {
		TfOrder tfOrder = new TfOrder();
		tfOrder.setSubject(subject);
		tfOrder.setBeans(amount);
		tfOrder.setCost(cost);
		tfOrder.setSerialnumber(sn);
		tfOrder.setMemberid(uid);
		return tfOrder;
	}

	public static Order fromPo(TfOrder tfOrder) {
		Order order = new Order(tfOrder.getMemberid(), tfOrder.getBeans(), tfOrder.getCost(), tfOrder.getSerialnumber(),
				tfOrder.getSubject());

		return order;

	}

	public Integer getUid() {
		return uid;
	}

	public Integer getId() {
		return id;
	}

	public String getSubject() {
		return StringUtils.isEmpty(subject) ? String.format("%s颗糖豆", amount) : subject;
	}

	public Integer getAmount() {
		return amount;
	}

	public Integer getCost() {
		return cost;
	}

	public Long getSn() {
		return sn;
	}

}
