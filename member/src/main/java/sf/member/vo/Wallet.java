package sf.member.vo;

/**
 * Created by ligen on 16-3-11.
 */
public class Wallet {

	Double beans;

	public Wallet(Double beans) {
		this.beans = (beans == null ? 0 : beans);
	}

	public Double getBeans() {
		return beans;
	}
}
