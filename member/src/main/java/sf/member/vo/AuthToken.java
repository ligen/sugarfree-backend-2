package sf.member.vo;

/**
 * Created by ligen on 16-3-11.
 */
public class AuthToken {

	private String token;

	private Long expire;

	public AuthToken(String token, Long expireAt) {
		this.token = token;
		this.expire = expireAt;
	}

	public String getToken() {
		return token;
	}

	public Long getExpire() {
		return expire;
	}
}
