package sf.member.vo;

/**
 * Created by ligen on 16-3-16.
 */
public class UploadToken {
	private String token;
	private String domain;

	public UploadToken(String token, String domain) {
		this.token = token;
		this.domain = domain;
	}

	public UploadToken(String token) {
		this(token, "7xlk50.com2.z0.glb.qiniucdn.com");
	}

	public String getToken() {
		return token;
	}

	public String getDomain() {
		return domain;
	}
}
