package sf.member.vo;

/**
 * Created by ligen on 16-3-7.
 */
public class Result<T> {
	private T result;

	public Result(T result) {
		this.result = result;
	}

	public T getResult() {
		return result;
	}
}
