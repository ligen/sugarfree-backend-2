package sf.member.vo;

import java.text.ParseException;

/**
 * Created by ligen on 16-3-10.
 */
public interface Convertable<P> {
	void v2p(P p) throws ParseException;

	void p2v(P p);
}
