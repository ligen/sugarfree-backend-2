package sf.member.vo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang3.StringUtils;

import sf.core.entity.TuMember;

/**
 * Created by ligen on 16-3-10.
 */
public class Profile implements Convertable<TuMember> {

	private final static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

	private String avatar;

	private String name;

	private Short height;

	private Short weight;

	private Double bmi;

	private Byte gender;

	// @Pattern(value = "[0-9]{4}/[0-9]{2}/[0-9]{2}")
	private String birthday;

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Short getHeight() {
		return height;
	}

	public void setHeight(Short height) {
		this.height = height;
	}

	public Short getWeight() {
		return weight;
	}

	public void setWeight(Short weight) {
		this.weight = weight;
	}

	public Double getBmi() {
		return bmi;
	}

	public void setBmi(Double bmi) {
		this.bmi = bmi;
	}

	public Byte getGender() {
		return gender;
	}

	public void setGender(Byte gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	@Override
	public void v2p(TuMember member) throws ParseException {
		member.setGender(gender);
		member.setHeight(height);
		member.setWeight(weight);
		member.setName(name);
		member.setAvatar(avatar);
		member.setBirthday(StringUtils.isEmpty(birthday) ? null : dateFormat.parse(birthday));
	}

	@Override
	public void p2v(TuMember member) {

		setGender(member.getGender());
		setHeight(member.getHeight());
		setWeight(member.getWeight());
		setName(member.getName());
		setAvatar(member.getAvatar());
		setBirthday(member.getBirthday() == null ? "" : dateFormat.format(member.getBirthday()));
	}
}
