package sf.member.vo;

/**
 * Created by ligen on 16-3-11.
 */
public class Token {

	private String token;

	public Token(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}
}
