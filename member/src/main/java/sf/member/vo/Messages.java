package sf.member.vo;

import java.util.Collection;

/**
 * Created by ligen on 16-3-14.
 */
public class Messages {

	private Collection<Message> system;
	private Collection<Message> course;
	private Long timestamp;

	public Messages(Collection<Message> system, Collection<Message> course, Long timestamp) {
		this.system = system;
		this.course = course;
		this.timestamp = timestamp;
	}

	public Collection<Message> getSystem() {
		return system;
	}

	public Collection<Message> getCourse() {
		return course;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public static class Message {

		private String content;
		private Long time;
		private Integer id;

		public Message(String content, Long time, Integer id) {
			this.content = content;
			this.time = time;
			this.id = id;
		}

		public String getContent() {
			return content;
		}

		public Long getTime() {
			return time;
		}

		public Integer getId() {
			return id;
		}
	}
}
