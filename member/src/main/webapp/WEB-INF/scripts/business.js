//package
function packageListPageReady(){
	loadBanners();
}

function loadBanners(){
	//appendBanners();
	var swiper = new Swiper('.swiper-container', {
	    pagination: '.swiper-pagination',
	    nextButton: '.swiper-button-next',
	    prevButton: '.swiper-button-prev',
	    paginationClickable: true,
	    spaceBetween: 0,
	    centeredSlides: true,
	    autoplay: 2500,
	    autoplayDisableOnInteraction: false
	});
}

function loadPackageMore(){
	var lat = $("#lat").val();
	var lng = $("#lng").val();
	var page = parseInt($("#page").val()) + 1;
	$.ajax({
		type:"GET",
		url:appUrl + "package/exetends?lat=" + lat + "&lng=" + lng + "&page=" + page,
		dataType:"json",
		success:function(msg){
			console.info("msg",msg);
			setPageInfoToHidden(msg.page);
			appendPackages(msg.list)
		},
		error:function(msg){
			
		}
	});
}

function setPageInfoToHidden(pageInfo){
	$("#page").val(pageInfo.page);
}

function appendPackages(list){
	if(list.length==0){
		$(".load-more").hide();
		return false;
	}
	
	var itemsHtml = '';
	for (var i = 0; i < list.length; i++) {
			var itemHtml = '<div class="box-item" onclick="showPackageDetail('+ list[i].id +')">'
										+		'<div class="item-header">'
										+		'<p class="first-tag">'
										+		'			<span>训练营</span>'
										+		'		</p>';
			if(list[i].classType===2){
					itemHtml+=		'			<p class="second-tag">'
										+		'				<span>私教课</span>'
										+		'			</p>';
			}
					itemHtml+=		'		<span class="rg-tag">报名截止:' + strDateToFormatStrDate(list[i].endTime) + '</span>'
										+		'	</div>'
										+		'	<img alt="lesson1" src="' + list[i].logo + '">'
										+		'	<div class="item-description">'
										+		list[i].title
										+		'	</div>'
										+		'	<div class="item-bottom">'
										+		'		<p class="shiquxian">'
										+		'			<span>'+ list[i].shiQuXian +'</span>'
										+		'		</p>'
										+		'		<p class="juli">'
										+		'			<span>距你<'+ 1 +'km</span>'
										+		'		</p>'
										+		'		<p class="item-bottm-right">'
										+		'			<span class="price-int">'+ list[i].priceInt +'</span>'
										+		'		</p>'
										+		'	</div>'
										+		'</div>'
		itemsHtml += itemHtml;
	}
	
	 //console.info(itemsHtml);
	 $("#list-wrap").append(itemsHtml);
}

//detail = 10 packageDetail  	detail = 20 gymDetail
function showPackageDetail(id){
	location.href = "#packageDetail," + appUrl  + "package/" + id + "," + id ; 
}




function packageDetailPageReady(packageId){
	var firstPage = 1;
	loadLessonMore(packageId,firstPage);
	loadCommentMore(packageId,firstPage);
}

function loadLessonMore(packageId,page){
	var limit = 3;
	if(!page){
		page = parseInt($("#lessonPage").val()) + 1;
	}
	$.ajax({
		type:"GET",
		url:appUrl + "package/" + packageId +"/lessons?page="+page+"&limit=" + limit,
		dataType:"json",
		success:function(msg){
			console.info("msg",msg);
			$("#lessonPage").val(msg.page.page);
			appendLessons(msg.list);
		},
		error:function(msg){
			
		}
	});
}

function appendLessons(lessonList){
	var limit = 3;
	if(lessonList.length == 0){
		$(".package-info .extends").hide();
		return ;
	}
	
	var itemsHtml = '';
	for (var i = 0; i < lessonList.length; i++) {
		itemsHtml += '<div class="lesson-item">'
				  +	'	<p class="title">'
				  +	'		<span>课时' + parseInt((($("#lessonPage").val() - 1) * limit) + parseInt(i+1)) + ' 「&nbsp;'+ lessonList[i].title +'&nbsp;」</span>'
				  +	'	</p>'
				  +	'	<p class="time">'
				  +	'		<span>' + dateFormat(lessonList[i].begintime,1) + '～' + dateFormat(lessonList[i].endtime,2) + '</span>'					
				  +	'	</p>'
				  +	'	<img alt="lesson1" src="'+ lessonList[i].image +'">'
				  +	'	<p class="describe">'
				  +	lessonList[i].description
				  +	'	</p>'
				  +	'	</div>';
	}
	
	$(".lesson-list").append(itemsHtml);
}


function loadPackageCommentMore(packageId,page){
	var limit = 3;
	if(!page){
		page =parseInt( $("#commentPage").val()) + 1;
	}
	$.ajax({
		type:"GET",
		url:appUrl + "package/"+ packageId +"/comment?packageId" + packageId + "&page=" + page + "&limit=" + limit,
		dataType:"json",
		success:function(msg){
			console.info("msg",msg);
			$("#commentPage").val(msg.page.page);
			appendCommentToPackageCommentList(msg.list)
		},
		error:function(msg){
			
		}
	});
}

function appendCommentToPackageCommentList(commentList){
	
	if(commentList.length==0){
		$(".evaluate-info .extends").hide();
		return ;
	}
	var commentsHtml = '';
	for(var i = 0 ; i < commentList.length ; i ++){
		var imagesHtml = '';
		if(commentList[i].urls){
			var urlArray = commentList[i].urls.split(",");
			for(var j=0 ; j < urlArray.length ; j++){
				imagesHtml += '<img alt="" src="'+ urlArray[j] +'">';
			}
		}
		var commentHtml = '<div class="evaluate-item-divider"></div>'
											  + '<div class="evaluate-item">'
											  + '	<div class="evaluate-header">'
											  + '			<img alt="头像" src="'+ commentList[i].avatar +'">'
											  + '			<span class="user-alias">'
											  +	 		commentList[i].userName
											  + '			</span>'
											  + '		</div>'
											  + '		<div class="evaluate-content">'
											  +	 		commentList[i].content
											  + '		</div>'
											  + '		<div class="evaluate-content-images">'
											  +			imagesHtml
											  + '		</div>'
											  + '		<div class="evaluate-subscript">'
											  + '			<span class="evaluate-tag">' + commentList[i].title + '</span>'
											  + '			<span class="evaluate-time">'+ dateFormat(commentList[i].createDate,1) +'</span>'
											  + '		</div>';					
		
		commentsHtml += commentHtml;
	}
	
	$(".evaluate-info .evaluate-list").append(commentsHtml);
}










//gym
function gymListPageReady(){
	//loadBanners();
}


function loadGymMore(){
	var lat = $("#lat").val();
	var lng = $("#lng").val();
	var regionId = $("#lat").val();
	var page = parseInt($("#page").val()) + 1;
	var limit = $("#limit").val();
	var distance = $("#distance").val();
	
	var searchParam = "lat=" + lat 
					+ "&lng=" + lng
					+ "&regionId=" + regionId
					+ "&page=" + page
					+ "&limit=" + limit
					+ "&distance=" + distance;
	$.ajax({
		type:"GET",
		url:appUrl + "gym/extends?"
				   + searchParam,
		dataType:"json",
		success:function(msg){
			console.info("msg",msg);
			appendGymToGymListWrap(msg.list);
		},
		error:function(msg){
			
		}
	});
}


function appendGymToGymListWrap(gymList){
	
	if(gymList.length==0){
		$("#gym-list .load-more").hide();
		return ;
	}
	
	var gymsHtml = '';
	for(var i = 0 ; i < gymList.length ; i ++){
		
		var gymHtml =		'<div class="box-item" onclick="showGymDetail(' + gymList[i].id + ')">'
								   +	'		<p class="gym-image">'
								   +	'				<img alt="gymImage" src="' + gymList[i].logo + '">'
								   +	'		</p>'
								   +	'			<p>' + gymList[i].name + '</p>'
								   +	'			<p class="address-name">' + gymList[i].address + '</p>'
								   +	'			<p class="gym-other-info">'
								   +	'				<span class="gym-star">' + gymList[i].liked + '</span>'
								   +	'				<span class="gym-evaluate">评论&nbsp;' + gymList[i].count + '</span>'
								   +	'				<span class="gym-distance">距你XXXkm</span>'
								   +	'			</p>'
								   +	'			<div class="separator"></div>'
								   +	'			<p class="gym-tags">'
								   +	'				<span class="fist-tag">训练营</span>'
								   +	'				<span class="second-tag">私教课</span>'
								   +	'				<span class="book-number">' + gymList[i].signCount + '人已报名</span>'
								   +	'			</p>'
								   +	'		</div>';
		
		gymsHtml += gymHtml;
	}
	
	$("#gym-list .list-wrap").append(gymsHtml);
	
}

function showGymDetail(id){
	location.href= "#gymDetail," + appUrl + "gym/" + id +"," + id;
}


function loadGymCommentMore(gymId,page){
	var limit = 3;
	if(!page){
		page =parseInt( $("#commentPage").val()) + 1;
	}
	$.ajax({
		type:"GET",
		url:appUrl + "gym/"+ gymId +"/comment?page=" + page + "&limit=" + limit,
		dataType:"json",
		success:function(msg){
			console.info("msg",msg);
			$("#commentPage").val(msg.page.page);
			appendCommentToGymCommentList(msg.list)
		},
		error:function(msg){
			
		}
	});
}

function appendCommentToGymCommentList(commentList){
	
	if(commentList.length==0){
		$(".evaluate-info .extends").hide();
		return ;
	}
	var commentsHtml = '';
	for(var i = 0 ; i < commentList.length ; i ++){
		
		var imagesHtml = '';
		if(commentList[i].urls){
			var urlArray = commentList[i].urls.split(",");
			for(var j=0 ; j < urlArray.length ; j++){
				imagesHtml += '<img alt="" src="'+ urlArray[j] +'">';
			}
		}
		var commentHtml = '<div class="evaluate-item-divider"></div>'
											  + '<div class="evaluate-item">'
											  + '	<div class="evaluate-header">'
											  + '			<img alt="头像" src="'+ commentList[i].avatar +'">'
											  + '			<span class="user-alias">'
											  +	 		commentList[i].userName
											  + '			</span>'
											  + '		</div>'
											  + '		<div class="evaluate-content">'
											  +	 		commentList[i].content
											  + '		</div>'
											  + '		<div class="evaluate-content-images">'
											  + 			imagesHtml
											  + '		</div>'
											  + '		<div class="evaluate-subscript">'
											  + '			<span class="evaluate-tag">' + commentList[i].title + '</span>'
											  + '			<span class="evaluate-time">'+ dateFormat(commentList[i].createDate,1) +'</span>'
											  + '		</div>';					
		
		commentsHtml += commentHtml;
	}
	
	$(".evaluate-info .evaluate-list").append(commentsHtml);
}




//comment
function fetchComment(gymId){
	$.ajax({
		type:"GET",
		url:appUrl + "index/gym/" + gymId + "/comment",
		dataType:"json",
		success:function(msg){
			console.info("msg",msg);
			appendLessons();
		},
		error:function(msg){
			
		}
	});
}





function quickMove(targetId){
	$(".menu-bar span").removeClass("ac");
	$("." + targetId + " span").addClass("ac");
     $("body,html").animate({scrollTop:$("#" + targetId).offset().top -250});
}



//invitation
function sendInvitation(){
	location.href="#invitation," + appUrl + "invitation/shared/show";
}




