function strDateToFormatStrDate(strDate){
	var formatStrDate  =  strDate.substr(0,4) + "/"
										   +  strDate.substr(4,2) + "/"
										   + strDate.substr(6) ;
	
	return formatStrDate;
}


function dateFormat(date,pattren){
	switch (pattren) {
	case 1://03.04 14:16
		var date = new Date(date);
		var dateStr = fillZero(parseInt(date.getMonth() + 1)) + '.' + fillZero(date.getDate()) + ' ' + fillZero(date.getHours()) + ':' + fillZero(date.getMinutes());
		return dateStr;
		break;
	case 2://14:16
		var date = new Date(date);
		var dateStr = fillZero(date.getHours()) + ':' + fillZero(date.getMinutes());
		return dateStr;
		break;

	default:
		break;
	}
}

function fillZero(intVal){
	if(intVal < 10){
		intVal = "0" + intVal;
	}
	return intVal;
} 