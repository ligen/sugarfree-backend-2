<%@ page contentType="text/html;charset=utf-8" %> 
<%@ page import="java.util.*"%>
<%@ page import="sf.core.util.*"%>
<%@ page import="sf.core.common.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
 	String baseUrl = request.getScheme()+"://"+ request.getServerName()+":" + 
					 request.getServerPort() + request.getContextPath() + "/"; 
	
	Map packageInfo = (Map)request.getAttribute("packageInfo");
	DefaultPageList lessonPageAndList =  request.getAttribute("lessons")==null?null:(DefaultPageList)request.getAttribute("lessons");
	List lessonList = lessonPageAndList.getList();
	BasePage lessonPageInfo = 	lessonPageAndList.getPage();
	
	Map coach =request.getAttribute("coach") == null?null: (Map)request.getAttribute("coach");
	
	List deviceList =  request.getAttribute("devices")==null?null:(List)request.getAttribute("devices");
	
	DefaultPageList commentPageAndList =  request.getAttribute("comments")==null?null:(DefaultPageList)request.getAttribute("comments");
	List commentList = commentPageAndList.getList();
	BasePage commentPageInfo = 	commentPageAndList.getPage();
	
	Map gymInfo =  (Map)request.getAttribute("gym");
%>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>首页</title>
	<link href="../styles/common.css"  rel="stylesheet"  type="text/css">
	<link href="../styles/package-detail.css"  rel="stylesheet"  type="text/css">
</head>
<body>
	<div id="content-wrap">
		<div id="package-detail">
			<div class="header">
				<p class="title">
					<%=packageInfo.get("title") %>
				</p>
				
				<p class="package-tags">
						<span class="package-tag-group">训练营</span>
						<%if((Integer)packageInfo.get("classtype")==2){%>
							<span class="package-tag-private">私教课</span>
						<%} %>
				</p> 
				
				<p class="package-other-info">
					<span class="package-endtime">报名截止日期：<%=DateUtil.diyFormat((Date)packageInfo.get("endtime"), "yyyy/MM/dd")  %></span>
					<span class="number-limit">上课剩余人数：<%=packageInfo.get("numberlimit") %>人</span>
				</p>
			</div>
			
			
			<div class="menu-bar">
				<ul>
					<li class="anchor-detail-info"  onclick="quickMove('anchor-detail-info')">
						<span class="ac">详情</span>
						<span class="bottom-hr ac"></span>
					</li>
					<li class="report anchor-report-info" onclick="quickMove('anchor-report-info')">
						<span>报告（<%=commentPageInfo.getTotalSize() %>）</span>
						<span class="bottom-hr"></span>
					</li>
					<li class="anchor-contact-info" onclick="quickMove('anchor-contact-info')">
						<span>联系</span>
						<span class="bottom-hr"></span>
					</li>
				</ul>
			</div>
			<span id="anchor-detail-info"></span>
			<div class="package-info">
				<p class="package-intro">
					<%=packageInfo.get("description") %>
				</p>
				<div class="lesson-list">
				<%
					if(null != lessonList && lessonList.size()>0){
						for(int i=0 ; i < lessonList.size() ; i++){
							Map lessonMap = (Map)lessonList.get(i);
				%>
						<div class="lesson-item">
							<p class="title">
									<span>课时<%=i+1 %>&nbsp;「<%=lessonMap.get("title") %>」</span>					
							</p>
							<p class="time">
									<span><%=DateUtil.diyFormat((Date)lessonMap.get("begintime"), "MM-dd HH:mm")  %>～<%=DateUtil.diyFormat((Date)lessonMap.get("endtime"), "HH:mm")  %></span>					
							</p>
							
							<img alt="lesson1" src="<%=lessonMap.get("image")%>">
							<p class="describe">
								<%=lessonMap.get("description")%>
							</p>
						</div>
				<%			
						}
					}
				%>
					<%-- 
					<div class="lesson-item">
						<p class="title">
								<span>课时1&nbsp;「心肺适应」</span>					
						</p>
						<p class="time">
								<span>3-28 19：00～20：00</span>					
						</p>
						
						<img alt="lesson1" src="../images/lesson1.jpg">
						<p class="describe">
							 两种极速减脂内容结合在一起，针对全身每个位置极速瘦身，体验燃烧脂肪的快感
						</p>
					</div>
					--%>
				</div><!-- lesson-list -->
				
			   <div class="extends">
			       <span onclick="loadLessonMore(<%=packageInfo.get("id") %>)">
			       		查看更多
			       </span>
			   </div>
				
				<div class="param-area">
				<input id="lessonPage" type="hidden" value="<%=lessonPageInfo.getPage() %>">
				</div><!-- param-area end-->
				</div><!-- package-info -->
			
			<%if(null != coach && coach.size() > 0){%>
				<div class="coach-info">
					<div class="logo">
						<img alt="" src="<%=coach.get("avatar")%>">
					</div>
					
					<div class="introduce">
						<p class="coach-name"><%=coach.get("name") %></p>
						<p class="coach-intro">
							<%=coach.get("description") %>
						</p>
						<p class="coach-tags">
							<span class="tag">国家运动员</span>
						</p>
					</div>
				</div>
			<%} %>

			
			<div class="gym-info">
			<% if(null != deviceList && deviceList.size() > 0){%>
				<div class="device">
					<% for(Object deviceObj :deviceList){
						Map deviceMap = (Map)deviceObj;
					%>
						<p class="device-item">
							<span class="device-icon">
								<img alt="" src="<%=deviceMap.get("devices")%>">
							</span>
							<span class="device-name"><%=deviceMap.get("name")%></span>
						</p>
					<%} %>
					<%--
					<p class="device-item">
						<span class="device-icon-wifi"></span>
						<span class="device-name">WIFI</span>
					</p>
					<p class="device-item">
						<span class="device-icon-yinyongshui"></span>
						<span class="device-name">饮用水</span>
					</p> 
					--%>
				</div>
			<%}%>
	
				
				<span id="anchor-contact-info"></span>
				<div class="contact">
					<p class="gym-address">
						<%=gymInfo.get("address") %>
					</p>
					<p class="gym-contacts">
						<a href="#gymMap,<%=gymInfo.get("lat") %>,<%=gymInfo.get("lng") %>,<%=gymInfo.get("address") %>,<%=gymInfo.get("logo") %>"  class="gym-contact-map" 
							 onclick="window.HomeInterface.showNavigation('100,100,一号线,http://7xlk50.com2.z0.glb.qiniucdn.com/2_274_0BYAAPLYINyAyg8U-3c6fd1a0-b334-4316-affc-eca8b6f81c73')">
							<!-- 地图 -->
						</a>
						
						<a href="tel:13370276502" class="gym-contact-phone">
						<!-- 电话 -->
						</a>
					</p>
				</div><!-- contact end -->
			</div>
			
			<span id="anchor-report-info"></span>
			<div class="evaluate-info">
				<p class="evaluate-title">「&nbsp;体验报告&nbsp;」</p>
				<div class="evaluate-list">
				<%
				if(null != commentList && commentList.size() > 0){
						for(int j=0; j < commentList.size() ; j++){
							Map commentMap = (Map)commentList.get(j);
				%>
					<div class="evaluate-item">
							<div class="evaluate-header">
								<img alt="头像" src="<%=commentMap.get("avatar")%>">
								<span class="user-alias">
									<%=commentMap.get("userName")%>
								</span>
							</div>
							<div class="evaluate-content">
								<%=commentMap.get("content")%>
							</div>
							
							<div class="evaluate-content-images">
								<%
									String urls = commentMap.get("urls")==null?"":commentMap.get("urls").toString();
									String[] urlArray = urls.split(",");
									for(String url  : urlArray){
								%>
										<img alt="" src="<%=url	%>">
								<%
									}
								%>
							</div>
							
							<div class="evaluate-subscript">
								<span class="evaluate-tag"><%=commentMap.get("title") %></span>
								<span class="evaluate-time"><%=DateUtil.diyFormat((Date)commentMap.get("createDate"),"MM.dd HH:mm") %></span>
							</div>					
						</div><!-- evaluate-item end -->
						<div class="evaluate-item-divider"></div>
				<%
						} 
					}
				%>
					
					
					<%--
					<div class="evaluate-item">
						<div class="evaluate-header">
							<img alt="头像" src="../images/lesson1.jpg">
							<span class="user-alias">
								小丸子
							</span>
						</div>
						<div class="evaluate-content">
							体验报告内容
						</div>
						
						<div class="evaluate-content-images">
							<img alt="" src="../images/lesson1.jpg">
							<img alt="" src="../images/lesson1.jpg">
							<img alt="" src="../images/lesson1.jpg">
						</div>
						
						<div class="evaluate-subscript">
							<span class="evaluate-tag">无糖减肥训练营第一期</span>
							<span class="evaluate-time">02.20 19:40</span>
						</div>					
					</div> 
					--%>
					
				</div><!--  evaluate-list  -->
				<%
					if(null != commentList && commentList.size() > 0){
				 %>
				 <div class="extends">
				        <span onclick="loadPackageCommentMore(<%=packageInfo.get("id") %>)">查看更多</span>
				        <!-- 
				        <img src="images/xiala.png">
				         -->
				   </div>
				 <%
					}else{
				%>
					<span class="no-comment-info">暂无评论信息！<span>
				 <%
					}
				%>
				
				
				<div class="param-area">
					<input id="commentPage" type="hidden" value="<%=commentPageInfo.getPage() %>">
				</div><!-- param-area end-->
				
			</div><!-- evaluate-info -->
			<div class="btn-space">
				&nbsp;
			</div>
			<div class="book">
				<p class="book-price">
					<span class="total-price">总价</span>
					<span class="tdz"> 
						<!-- 豆 --> 
					</span>
					<span class="bxx"><!-- X --></span>	
					<span class="price-int"><%=packageInfo.get("priceInt") %></span>
					<span class="comments">(每颗糖豆40元)</span>  
				</p>
				
				<p class="book-btn">
					<a href='#book,<%=(Integer)packageInfo.get("classtype")%>,<%=packageInfo.get("id")%>,1' onclick="window.HomeInterface.book()">
						预约
					</a>

					
				</p>
			</div><!-- book -->
			
		</div><!-- id package-detail end -->
	</div><!-- content-wrap end -->
	
	
	
	<script type="text/javascript">
	    var appUrl = "<%=baseUrl%>";
	</script>
	
	<script src="../scripts/lib/jquery.min.js" type="text/javascript"></script>
	<script src="../scripts/device-adapter.js" type="text/javascript"></script>
	<script src="../scripts/business.js" type="text/javascript"></script>
	<script src="../scripts/utils.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			// packageDetailPageReady(<%=packageInfo.get("id") %>);
			
			var menuBarTop = $(".menu-bar").offset().top;
			window.onscroll = function () { 
				var scrollTop = document.documentElement.scrollTop || document.body.scrollTop; 
				if(scrollTop > menuBarTop){
					$(".menu-bar").addClass("fixed-top");
				}else{
					$(".menu-bar").removeClass("fixed-top");
				}
			};
		});
	</script>
	
</body>
</html>
