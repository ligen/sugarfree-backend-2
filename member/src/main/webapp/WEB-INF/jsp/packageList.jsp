<%@ page contentType="text/html;charset=utf-8" %> 
<%@ page import="java.util.*"%>
<%@ page import="sf.core.util.*"%>
<%@ page import="sf.core.common.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
 	String baseUrl = request.getScheme()+"://"+ request.getServerName()+":" + 
					 request.getServerPort() + request.getContextPath() + "/"; 
	
	DefaultPageList pageAndList = (DefaultPageList)request.getAttribute("pageAndList");
	BasePage pageInfo = 	pageAndList.getPage();
	List packageList = 	pageAndList.getList();
%>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>首页</title>
	<link href="../styles/lib/swiper-3.3.1.min.css"  rel="stylesheet"  type="text/css">
	<link href="../styles/common.css"  rel="stylesheet"  type="text/css">
	<link href="../styles/package-list.css"  rel="stylesheet"  type="text/css">
</head>
<body>
	<div id="content-wrap">
		<%@include file="banner.jsp" %>
		<div id="list-wrap" class="list-wrap">
				<%
					for(Object packageObj : packageList){
						Map map = (Map)packageObj;
				%>
					<!-- 
					<div class="box-item" onclick="showPackageDetail(<%=map.get("id")%>)"> -->
					<div class="box-item" onclick="showPackageDetail(<%=map.get("id")%>)">
						<div class="item-header">
							<p class="first-tag">
								<span>训练营</span>
							</p>
							<%if((Integer)map.get("classType") == 2){ %>
								<p class="second-tag">
									<span>私教课</span>
								</p>
							<%} %>
							<span class="rg-tag">报名截止:<%=DateUtil.diyFormat(DateUtil.diyParse(map.get("endTime").toString(), "yyyyMMdd"),  "yyyy/MM/dd") %></span>
						</div>
						<img alt="lesson1" src="<%=map.get("logo") %>">
						
						<div class="item-description">
							<%=map.get("title") %>
						</div>
						
						<div class="item-bottom">
							<p class="shiquxian">
								<span><%=map.get("shiQuXian") %></span>
							</p>
							
							<p class="juli">
								<span>距你<1km</span>
							</p>
							
							<p class="item-bottm-right">
								<span class="price-int"><%=map.get("priceInt")%></span>
							</p>
						</div>
					</div>
				<%
					}
				 %>
				<!-- box-item end -->
				
				<%--
				<div class="box-item" onclick="showPackageDetail(1)">
					<div class="item-header">
						<p class="first-tag">
							<img alt="" src="../images/first-tag.png">
							<span>训练营</span>
						</p>
						<p class="second-tag">
							<img alt="" src="../images/second-tag.jpg">
							<span>私教课</span>
						</p>
						<span class="rg-tag">报名截止:2016/02/02</span>
					</div>
					<img alt="lesson1" src="../images/lesson1.jpg">
					
					<div class="item-description">
						Title
					</div>
					
					<div class="item-bottom">
						<p>
							<img class="icon" alt="" src="../images/weizhi.png">
							<span>黄浦区</span>
						</p>
						
						<p>
							<img class="icon" alt="" src="../images/juli.png">
							<span>距你<1km</span>
						</p>
						
						<p class="item-bottm-right">
							<img class="tdz" alt="" src="../images/tdz.png">
							<span>x57</span>
						</p>
					</div>
				</div><!-- temp --> --%>
		</div><!-- list-wrap end -->
		
		<div class="load-more" onclick="loadPackageMore();">
				<span>加载更多</span>
		</div>
	</div><!-- content-wrap end -->
	<div class="param-area">
		<input id="lat" type="hidden" value="">
		<input id="lng" type="hidden" value="">
		<input id="regionId" type="hidden" value="">
		<input id="classType" type="hidden" value="">
		<input id="page" type="hidden" value="<%=pageInfo.getPage() %>">
		<input id="limit" type="hidden" value="">
		<input id="distance" type="hidden" value="">
	</div><!-- param-area end-->
	
	<script type="text/javascript">
	    var appUrl = "<%=baseUrl%>";
	</script>
	
	<script src="../scripts/lib/jquery.min.js" type="text/javascript"></script>
	<script src="../scripts/device-adapter.js" type="text/javascript"></script>
	<script src="../scripts/lib/swiper-3.3.1.min.js" type="text/javascript"></script>
	<script src="../scripts/utils.js" type="text/javascript"></script>
	<script src="../scripts/business.js" type="text/javascript"></script>
	<script src="../scripts/app-communication.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			packageListPageReady();
		});
	</script>
</body>
</html>
