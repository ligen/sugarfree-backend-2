<%@ page contentType="text/html;charset=utf-8" %> 
<%@ page import="java.util.*"%>
<%@ page import="sf.core.util.*"%>
<%@ page import="sf.member.controller.InvitationController"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
	String baseUrl = request.getScheme()+"://"+ request.getServerName()+":" + 
			 request.getServerPort() + request.getContextPath() + "/"; 

	Map memberInfo = request.getAttribute("memberInfo") == null?null: (Map)request.getAttribute("memberInfo");
	Map packageInfo = request.getAttribute("packageInfo") == null?null: (Map)request.getAttribute("packageInfo");
	Map gymInfo = request.getAttribute("gymInfo") == null?null: (Map)request.getAttribute("gymInfo");
	String isSharedPage =  request.getAttribute("isSharedPage") == null?"": request.getAttribute("isSharedPage").toString();
%>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>支付成功</title>
	<link href="<%=baseUrl %>styles/lib/swiper-3.3.1.min.css"  rel="stylesheet"  type="text/css">
	<link href="<%=baseUrl %>styles/common.css"  rel="stylesheet"  type="text/css">
		<link href="<%=baseUrl %>styles/invitation.css"  rel="stylesheet"  type="text/css">
</head>
<body>
	<div id="content-wrap">
		<div id="invitation">
			<div class="invitation-motto">
				你离成功更进一步啦！邀请好友一起来吧！
			</div><!-- invitation-motto end -->
			<div class="invitation-wrap">
				
				<div class="invitation-content">
					<p class="invitation-title">
						邀请函
					</p>
					
					<p class="invitation-person">
						<%=memberInfo.get("name") %>
					</p>
					
					<p class="invitation-project">
						<span class="invitation-project-label">邀您体验</span>
						<span class="invitation-project-name"><%=packageInfo.get("title") %></span>
					</p>
					
					<p class="invitation-time">
						<span class="invitation-time-label">时间</span>
						<span class="invitation-time-name">
							<%=DateUtil.diyFormat( (Date)packageInfo.get("beginTime"),"yyyy/MM/dd") %>&nbsp;-&nbsp;<%=DateUtil.diyFormat( (Date) packageInfo.get("endTime"),"yyyy/MM/dd") %>
						</span>
					</p>
					
					<p class="invitation-gym">
						<span class="invitation-gym-label">场馆</span>
						<span class="invitation-gym-name"><%=gymInfo.get("name") %></span>
					</p>
					
					<p class="invitation-address">
						<span class="invitation-address-label">地点</span>
						<span class="invitation-address-name"><%=gymInfo.get("address") %></span>
					</p>
				</div><!-- invitation-content -->
			</div><!-- invitation-wrap -->
			
	
			
			<%
				 if(InvitationController.IS_SHARED_PAGE.equals(isSharedPage)){
			%>
				<div class="link">
					<a href="<%=packageInfo.get("shareUrl") %>">我要报名</a>
				</div>
			<%			 
				 }else{
			%>
				<div class="link">
					<a onclick="sendInvitation();">发送邀请函</a>
				</div>
			<%					 
				 }
			%>
		</div><!-- invitation end -->
	</div><!-- content-wrap end -->
	
	<script type="text/javascript">
	    var appUrl = "<%=baseUrl%>";
	</script>
	<script src="<%=baseUrl %>/scripts/lib/jquery.min.js" type="text/javascript"></script>
	<script src="<%=baseUrl %>/scripts/device-adapter.js" type="text/javascript"></script>
	<script src="<%=baseUrl %>/scripts/lib/swiper-3.3.1.min.js" type="text/javascript"></script>
	<script src="<%=baseUrl %>/scripts/business.js" type="text/javascript"></script>
	<script type="text/javascript">
	</script>
</body>
</html>