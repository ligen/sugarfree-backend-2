<%@ page contentType="text/html;charset=utf-8" %> 
<%@ page import="java.util.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
	String address = request.getAttribute("address") == null?"": request.getAttribute("address").toString();
	List banners =   request.getAttribute("banners") ==null?null:(List)request.getAttribute("banners");
%>
<div class="banner">
    <!-- Swiper -->
    <div class="swiper-container">
        <div class="swiper-wrapper">
        	<%
        		if(banners!=null){
        			for(Object bannerObj : banners){
            			Map banerMap = (Map)bannerObj;
            %>
            	<div class="swiper-slide">
	            	<a href="<%=banerMap.get("url")%>">
	            		<img alt="" src="<%=banerMap.get("image")%>">
	            	</a>
	            </div>
        	<%
        			} 
        		}
        	%>
        	<!-- 
            <div class="swiper-slide">
            	<a href="http://192.168.2.67:9999/member/package/333">
            		<img alt="" src="../images/lesson1.jpg">
            	</a>
            </div>
            <div class="swiper-slide">
            	  	<a href="http://192.168.2.67:9999/member/package/333">
	            		<img alt="" src="../images/lesson1.jpg">
	            	</a>	
            </div>
            <div class="swiper-slide">
            	   <a href="http://192.168.2.67:9999/member/package/333">
	            		<img alt="" src="../images/lesson1.jpg">
	            	</a>	
            </div>
             -->
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
        <!-- Add Arrows 
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>-->
    </div>
</div>

<a class="self-position-cli" href="#map" onclick="window.HomeInterface.getLocation()">
	<div class="self-position">
			<span class="position-icon"><%=address %></span>
			<span class="position-more"></span>
	</div>
</a>


