<%@ page contentType="text/html;charset=utf-8" %> 
<%@ page import="java.util.*"%>
<%@ page import="sf.core.util.*"%>
<%@ page import="sf.core.common.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
 	String baseUrl = request.getScheme()+"://"+ request.getServerName()+":" + 
					 request.getServerPort() + request.getContextPath() + "/"; 
	

	Map gymDetail = (Map)request.getAttribute("detail");
	DefaultPageList packagePageAndList =  request.getAttribute("packages")==null?null:(DefaultPageList)request.getAttribute("packages");
	List packageList = packagePageAndList.getList();
	BasePage lessonPageInfo = 	packagePageAndList.getPage();
	
	Map coach =request.getAttribute("coach") == null?null: (Map)request.getAttribute("coach");
	
	DefaultPageList commentPageAndList =  request.getAttribute("comments")==null?null:(DefaultPageList)request.getAttribute("comments");
	List commentList = commentPageAndList.getList();
	BasePage commentPageInfo = 	commentPageAndList.getPage();
%>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>场馆详情</title>
	<link href="../styles/common.css"  rel="stylesheet"  type="text/css">
	<link href="../styles/gym-detail.css"  rel="stylesheet"  type="text/css">
</head>
<body>
	<div id="content-wrap">
		<div id="gym-detail">
			<div class="gym-brand">
				<div class="gym-brand-image">
					<img alt="" src="<%=gymDetail.get("logo")%>">
				</div>
				<div class="logo">
					<img alt="" src="<%=gymDetail.get("logo")%>">
				</div>
				<p class="gym-name">
					<%=gymDetail.get("name")%>
				</p>
				
				<p class="gym-address">
					<%=gymDetail.get("address")%>
				</p>
			</div><!-- gym-brand end -->
			
			<div class="gym-detail-content">			
				<div class="menu-bar">
					<ul>
						<li class="anchor-detail-info"  onclick="quickMove('anchor-detail-info')">
							<span class="ac">详情</span>
							<span class="bottom-hr ac"></span>
						</li>
						<li class="anchor-lessons-info"  onclick="quickMove('anchor-lessons-info')">
							<span>课程</span>
							<span class="bottom-hr"></span>
						</li>
						<li class="anchor-contact-info"  onclick="quickMove('anchor-contact-info')">
							<span>电话</span>
							<span class="bottom-hr"></span>
						</li>
						
						<li class="evaluate anchor-report-info"  onclick="quickMove('anchor-report-info')">
							<span>报告(<%=commentPageInfo.getTotalSize() %>)</span>
							<span class="bottom-hr"></span>
						</li>
					</ul>
				</div><!-- menu-bar end -->
				
				<span id="anchor-detail-info"></span>	
				<div class="gym-info">
					<p class="gym-intro">
						<%=gymDetail.get("description")%>
					</p>
					
					<div class="img-list">
					
						<%
							String envelopUrls = gymDetail.get("envelopUrls")==null?"":gymDetail.get("envelopUrls").toString();
							String[] envelopUrlArray = envelopUrls.split(",");
							for(String envelopUrl : envelopUrlArray){
						%>
							<img alt="" src="<%=envelopUrl %>">
						<%
							}
						%>
					</div><!-- img-list end -->
					
					
					
					<span id="anchor-lessons-info"></span>
					<div class="gym-have-lesson">
						<p class="title">
							「&nbsp;场馆课程&nbsp;」
						</p>
						<div class="separator"></div>
						<div class="lesson-list">
							
						<% 
							for(Object packageObj : packageList){
								Map packageMap = (Map)packageObj;
						%>
							<div class="lesson-item">
								<p class="title-and-price">
									<span class="lesson-title">
										<%=packageMap.get("title") %>
									</span>
									<span class="lesson-price hx-tdz"><%=packageMap.get("priceint") %></span>
								</p>
								<p class="lesson-time-range">
									<%=DateUtil.diyFormat( (Date)packageMap.get("endtime"),"yyyy/MM/dd") %>~<%=DateUtil.diyFormat( (Date) packageMap.get("endtime"),"yyyy/MM/dd") %>
								</p>
								
								<p class="lesson-tags">
									<span class="first-tag">训练营</span>
									<%
										if((Integer)packageMap.get("classtype") == 2){
									 %>
									<span class="second-tag">私教课</span>
								<%} %>
									<span class="detail-btn" onclick="showPackageDetail(<%=packageMap.get("id")%>)">详情</span>
								</p>
							</div><!-- lesson-item end -->
						<% 
							} 
						%>
						</div><!-- lesson-list end -->
						
					</div><!-- gym-have-lesson -->
					
					<div class="gym-have-coach-info">
						<div class="coach-list">
							<div class="coach-item">
								<div class="coach-avatar">
									<img alt="" src="../images/lesson1.jpg">
								</div>
								<div class="coach-introduce">
									<p class="coach-name"><%=coach.get("name") %></p>
									<p class="coach-intro">
										<%=coach.get("description") %>
									</p>
									<p class="coach-tags">
										<span class="tag">国家运动员</span>
										<span class="tag">国家运动员</span>
									</p>
								</div>
				
							</div><!-- coach-item end -->
							
						</div><!-- coach-list end-->
					</div><!-- gym-have-coach-info -->
					
					<span id="anchor-contact-info"></span>
					<div class="gym-contact-info">
						<p class="address-name">
							<%=gymDetail.get("address")%>
						</p>
						
						<p class="contact-method">
							<a href="#gymMap,<%=gymDetail.get("lat")%>,<%=gymDetail.get("lng")%>,<%=gymDetail.get("address")%>,<%=gymDetail.get("logo")%>"  class="map" 
							onclick="window.HomeInterface.showNavigation('<%=gymDetail.get("lat")%>,<%=gymDetail.get("lng")%>,<%=gymDetail.get("address")%>,<%=gymDetail.get("logo")%>')">
							<!-- 地图 -->
							</a>
							<a href="tel:13370276502" class="tel">
							<!-- 电话 -->
							</a>
						</p>
					</div><!-- gym-contact-info end -->
					
				</div><!-- gym-info end-->
				
				<span id="anchor-report-info"></span>	
				<div class="evaluate-info">
					<p class="evaluate-title">「&nbsp;体验报告&nbsp;」</p>
					<div class="evaluate-list">
					<% 
						if( null != commentList && commentList.size() > 0){
							for(Object commentObj : commentList){
								Map commentMap =(Map) commentObj;
					%>
						<div class="evaluate-item">
								<div class="evaluate-header">
									<img alt="" src="<%=commentMap.get("avatar") %>">
									<span class="user-alias">
										<%=commentMap.get("userName") %>
									</span>
								</div>
								
								<div class="evaluate-content">
									<%=commentMap.get("content") %>
								</div>
								
							  <div class="evaluate-content-images">
									<%
										String urls = commentMap.get("urls")==null?"":commentMap.get("urls").toString();
										String[] urlArray = urls.split(",");
										for(String url  : urlArray){
									%>
											<img alt="" src="<%=url	%>">
									<%
										}
									%>
								</div>
								
								<div class="evaluate-subscript">
									<span class="evaluate-tag"><%=commentMap.get("title") %></span>
									<span class="evaluate-time"><%=DateUtil.diyFormat((Date)commentMap.get("createDate"),"MM.dd HH:mm") %></span>
								</div>					
						</div>
						<div class="evaluate-item-divider"></div>
					<%
							}
						}
					%>
					</div><!--  evaluate-list  -->
					
				<%
					if(null != commentList && commentList.size() > 0){
				 %>
					 <div class="extends" onclick="loadGymCommentMore(<%=gymDetail.get("id")%>)">
					        <span>查看全部</span>
					        <!-- 
					        <img src="images/xiala.png">
					         -->
					</div>
			<%
				}else{
			%>
					<span class="no-comment-info">暂无评论信息！<span>
		   <%
				}
		  %>
			
				<div class="param-area">
					<input id="commentPage" type="hidden" value="<%=commentPageInfo.getPage() %>">
				</div><!-- param-area end-->
				
				
				</div><!-- evaluate-info end-->
			</div><!-- gym-detail-content end -->
		</div><!-- id gym-detail end -->
	</div><!-- content-wrap end -->
	
	<script type="text/javascript">
	    var appUrl = "<%=baseUrl%>";
	</script>
	
	<script src="../scripts/lib/jquery.min.js" type="text/javascript"></script>
	<script src="../scripts/device-adapter.js" type="text/javascript"></script>
	<script src="../scripts/business.js" type="text/javascript"></script>
	<script src="../scripts/utils.js" type="text/javascript"></script>
	<script type="text/javascript">
		var menuBarTop = $(".menu-bar").offset().top;
		window.onscroll = function () { 
			var scrollTop = document.documentElement.scrollTop || document.body.scrollTop; 
			if(scrollTop > menuBarTop){
				$(".menu-bar").addClass("fixed-top");
			}else{
				$(".menu-bar").removeClass("fixed-top");
			}
		};
	</script>
</body>
</html>
