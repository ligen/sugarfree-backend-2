<%@ page contentType="text/html;charset=utf-8" %> 
<%@ page import="sf.core.common.*"%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%
 	String baseUrl = request.getScheme()+"://"+ request.getServerName()+":" + 
					 request.getServerPort() + request.getContextPath() + "/"; 

	DefaultPageList pageAndList = (DefaultPageList)request.getAttribute("pageAndList");
	BasePage pageInfo = 	pageAndList.getPage();
	List packageList = 	pageAndList.getList();
%>
<html>
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<title>首页</title>
	<link href="../styles/lib/swiper-3.3.1.min.css"  rel="stylesheet"  type="text/css">
	<link href="../styles/common.css"  rel="stylesheet"  type="text/css">
		<link href="../styles/gym-list.css"  rel="stylesheet"  type="text/css">
</head>
<body>
	<div id="content-wrap">
		<%@include file="banner.jsp" %>
		
		<div id="gym-list">
			<div  class="list-wrap">
				<%
					for(Object packageObj : packageList){
						Map gymMap = (Map)packageObj;
				
				%>
					<div class="box-item" onclick="showGymDetail(<%=gymMap.get("id")%>)">
						<p class="gym-image">
							<img alt="<%=gymMap.get("name")%>" src="<%=gymMap.get("urls")==null?"":gymMap.get("urls").toString().split(",")[0]%>">
						</p>
						<p><%=gymMap.get("name")%></p>
						<p class="address-name"><%=gymMap.get("address")%></p>
						<p class="gym-other-info">
							<span class="gym-star"><%=gymMap.get("liked")==null?"":String.format("%.2f",  gymMap.get("liked"))%></span>
							<span class="gym-evaluate">评论&nbsp;<%=gymMap.get("count")%></span>
							<span class="gym-distance">距你XXXkm</span>
						</p>
						<div class="separator"></div>
						<p class="gym-tags">
							<span class="fist-tag">训练营</span>
							<span class="second-tag">私教课</span>
							<span class="book-number"><%=gymMap.get("signCount")%>人已报名</span>
						</p>
					</div>
				<%
					}
				 %>
				 <%--
				<div class="box-item" onclick="showDetail()">
					<p class="gym-image">
						<img alt="lesson1" src="../images/lesson1.jpg">
					</p>
					<p>Birdman健身工作室-新天地店全部设施1天使用权GYM</p>
					<p class="address-name">黄浦区中山南路969号谷泰滨江大厦510室</p>
					<p class="gym-other-info">
						<span class="gym-star">4.7</span>
						<span class="gym-evaluate">评论&nbsp;24</span>
						<span class="gym-distance">距你898m</span>
					</p>
					<div class="separator"></div>
					<p class="gym-tags">
						<span class="fist-tag">训练营</span>
						<span class="second-tag">私教课</span>
						<span class="book-number">196人已报名</span>
					</p>
				</div>
				 --%>
				<!-- box-item end -->
				
			</div><!-- list-wrap end -->
			
			<div class="load-more" onclick="loadGymMore();">
				<span>加载更多</span>
			</div>
		</div><!-- gym-list end -->
		
	</div><!-- content-wrap end -->
	<div class="param-area">
		<input id="lat" type="hidden" value="">
		<input id="lng" type="hidden" value="">
		<input id="regionId" type="hidden" value="">
		<input id="page" type="hidden" value="<%=pageInfo.getPage() %>">
		<input id="limit" type="hidden" value="">
		<input id="distance" type="hidden" value="">
	</div><!-- param-area end-->
	<script type="text/javascript">
	    
	    var appUrl = "<%=baseUrl%>";
	</script>
	<script src="../scripts/lib/jquery.min.js" type="text/javascript"></script>
	<script src="../scripts/device-adapter.js" type="text/javascript"></script>
	<script src="../scripts/lib/swiper-3.3.1.min.js" type="text/javascript"></script>
	<script src="../scripts/business.js" type="text/javascript"></script>
	<script type="text/javascript">
		gymListPageReady();
	</script>
</body>
</html>
