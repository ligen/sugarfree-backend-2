package sf.member.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by ligen on 16-3-2.
 */
public class CryptUtilTest {

    @Test
    public void test_hash() throws Exception{

        String raw = "abc";
        String salt = "123";

        String hashed = CryptUtil.hash(raw, salt);

        Assert.assertEquals(hashed, CryptUtil.hash(raw, salt));

    }
}
