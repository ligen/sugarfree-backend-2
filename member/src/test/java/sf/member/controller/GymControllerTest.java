package sf.member.controller;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import sf.core.mapper.TfCouponMapper;
import sf.core.mapper.TfLessonMapper;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TfSignMapper;
import sf.core.mapper.TuGymMapper;
import sf.core.mapper.TuMemberMapper;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.purchase.PrivateReceiptProcesor;
import sf.member.purchase.TrainReceiptProcesor;

/**
 * Created by wb on 2016/3/8.
 */
public class GymControllerTest extends AbstractControllerTest<GymController> {

	@InjectMocks
	private GymController controller;

	@Mock
	private PrivateReceiptProcesor privateReceiptProcesor;

	@Mock
	private TrainReceiptProcesor trainReceiptProcesor;

	@Mock
	private MemmberPackageMapper memmberPackageMapper;

	@Mock
	private TfPackageMapper packageMapper;

	@Mock
	private TfLessonMapper lessonMapper;

	@Mock
	private TfReceiptMapper receiptMapper;

	@Mock
	private TfCouponMapper couponMapper;

	@Mock
	private TuMemberMapper memberMapper;

	@Mock
	private TfSignMapper signMapper;

	@Mock
	private TuGymMapper gymMapper;

	/// @Test
	public void testQueryIndxeGymJsp() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders.get("/gym/index").param("lat", "1").param("lng", "1").param("distance", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}

	// @Test
	public void testQueryIndxeGym() throws Exception {
		mockMvc.perform(
				MockMvcRequestBuilders.post("/gym/extends").param("lat", "1").param("lng", "1").param("distance", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testQueryIndexGymById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/gym/1").param("id", "1")).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	// @Test
	public void testQueryIndexGymPackageById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/gym/1/package")).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	// @Test
	public void testQueryIndexGymComentById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/gym/1/comment")).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Override
	protected GymController getInjected() {
		return controller;
	}
}