package sf.member.controller;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import sf.core.entity.TfFavor;
import sf.core.mapper.TfFavorMapper;
import sf.member.mapper.MemmberPackageMapper;

/**
 * Created by wb on 2016/3/11.
 */
public class FavorControllerTest extends AbstractControllerTest<FavorController> {

	@InjectMocks
	private FavorController controller;

	@Mock
	private MemmberPackageMapper memmberPackageMapper;

	@Mock
	private TfFavorMapper favorMapper;

	@Test
	public void testAdd() throws Exception {

		mockito.when(favorMapper.selectByPrimaryKey("1")).thenReturn(new TfFavor());

		mockMvc.perform(
				MockMvcRequestBuilders.post("/favor/add").param("uid", "1").param("type", "1").param("xid", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testDelete() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/favor/delete").param("id", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testList() throws Exception {

		mockMvc.perform(MockMvcRequestBuilders.post("/favor/list").param("uid", "1").param("type", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Override
	protected FavorController getInjected() {
		return controller;
	}
}