package sf.member.controller;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * Created by ligen on 16-3-3.
 */
import com.google.gson.Gson;

/**
 * Created by ligen on 15-12-1.
 */

@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractControllerTest<T> {

	protected MockMvc mockMvc;
	protected Mockito mockito = new Mockito();
	protected Gson gson = new Gson();

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(getInjected()).build();
	}

	protected abstract T getInjected();
}
