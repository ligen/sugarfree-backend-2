package sf.member.controller;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import sf.core.entity.TfReceipt;
import sf.core.mapper.TfCommentMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.service.AuthService;
import sf.core.service.StorageService;
import sf.member.common.OrderStatus;
import sf.member.common.PackageType;
import sf.member.mapper.MemmberPackageMapper;

/**
 * Created by wb on 2016/3/9.
 */
@SuppressWarnings({"static-access","rawtypes"})
public class CommentControllerTest extends AbstractControllerTest<CommentController> {

	@InjectMocks
	private CommentController controller;

	@Mock
	private TfCommentMapper commentMapper;

	@Mock
	private TfReceiptMapper receiptMapper;

	@Mock
	private StorageService storageService;

	@Mock
	private MemmberPackageMapper memmberPackageMapper;
	
	@Mock
    private AuthService authService;
	
	TfReceipt receipt = new TfReceipt();
	
	
	@Test
	public void testCreate() throws Exception {
		receipt.setStatus(OrderStatus.OVERED.getIndex());
		receipt.setMemberid(1);
		mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(1);
		mockito.when(receiptMapper.selectByPrimaryKey(mockito.anyInt())).thenReturn(receipt);
		mockMvc.perform(MockMvcRequestBuilders.post("/comment/create").param("rid", "15").param("memberId", "1")
				.param("content", "1").param("liked", "1")).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	
	
	

	@Test
	public void testUpload() throws Exception {

		mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(1);
		TfReceipt receipt = new TfReceipt();
		receipt.setStatus(OrderStatus.OVERED.getIndex());
		receipt.setMemberid(1);
		receipt.setType(PackageType.PRIVATE_PACKAGE.getIndex());
		mockito.when(receiptMapper.selectByPrimaryKey(mockito.anyInt())).thenReturn(receipt);
		mockMvc.perform(MockMvcRequestBuilders.post("/comment/upload").param("rid", "15").param("memberId", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());

	}

	@Test
	public void testQueryComment() throws Exception {
		mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(1);
		mockMvc.perform(MockMvcRequestBuilders.get("/comment/list")).andDo(MockMvcResultHandlers.print()).andReturn();
	}

	@Override
	protected CommentController getInjected() {
		return controller;
	}
}