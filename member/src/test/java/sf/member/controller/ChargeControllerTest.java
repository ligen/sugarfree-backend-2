package sf.member.controller;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import sf.core.mapper.TfOrderMapper;
import sf.core.service.AuthService;
import sf.core.service.PaymentService;

/**
 * Created by ligen on 16-3-8.
 */
public class ChargeControllerTest extends AbstractControllerTest<ChargeController> {

	@Mock
	PaymentService paymentService;

	@Mock
	TfOrderMapper orderMapper;

	@Mock
	AuthService<Integer> authService;

	@InjectMocks
	ChargeController controller;

	@Override
	protected ChargeController getInjected() {
		return controller;
	}

	@Test
	public void test_how_much() throws Exception {
		Integer beans = 10;
		Integer mockUnitPrice = 10;
		mockito.when(paymentService.getUnitPrice()).thenReturn(mockUnitPrice);
		mockMvc.perform(MockMvcRequestBuilders.post("/charge/howMuch").param("amount", Integer.toString(beans)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content()
						.string("{\"result\":" + Integer.toString(beans * mockUnitPrice) + "}"));
	}

	@Test
	public void test_charge() throws Exception {
		Integer uid = 1;
		Integer beans = 10;
		Integer mockUnitPrice = 10;
		Long sn = System.currentTimeMillis();
		String subject = "Just a test";
		String mockChannel = "unknown";
		Integer deviceType = 0;

		mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(uid);
		mockito.when(paymentService.getUnitPrice()).thenReturn(mockUnitPrice);
		mockito.when(paymentService.getAppId(paymentService.DEVICE_TYPE_ANDROID)).thenReturn("");

		mockMvc.perform(MockMvcRequestBuilders.post("/charge").param("uid", Integer.toString(uid))
				.param("amount", Integer.toString(beans)).param("channel", mockChannel)
				.param("deviceType", Integer.toString(deviceType))).andExpect(MockMvcResultMatchers.status().isOk());

		mockito.verify(orderMapper, mockito.only()).insert(mockito.any());
	}
}
