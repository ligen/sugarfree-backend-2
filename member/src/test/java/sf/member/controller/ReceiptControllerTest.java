package sf.member.controller;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import sf.core.mapper.TfPackageMapper;
import sf.core.service.AuthService;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.purchase.PrivateReceiptProcesor;
import sf.member.purchase.TrainReceiptProcesor;

/**
 * Created by wb on 2016/3/9.
 */

public class ReceiptControllerTest extends AbstractControllerTest<ReceiptController> {

	@Mock
	private PrivateReceiptProcesor privateReceiptProcesor;

	@Mock
	private TrainReceiptProcesor trainReceiptProcesor;

	@Mock
	private MemmberPackageMapper memmberPackageMapper;

	@Mock
	private AuthService<Integer> authService;

	@Mock
	private TfPackageMapper packageMapper;

	@InjectMocks
	private ReceiptController controller;

	@Test
	public void testCreate() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/receipt/create").param("packageId", "1").param("couponId", "1"))
				.andDo(MockMvcResultHandlers.print()).andReturn();
	}

	@Test
	public void testQueryReceipt() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/receipt/list").param("type", "1").param("status", "1"))
				.andDo(MockMvcResultHandlers.print()).andReturn();
	}

	@Test
	public void testQueryReceiptDetail() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("receipt/11")).andDo(MockMvcResultHandlers.print()).andReturn();
	}

	@Override
	protected ReceiptController getInjected() {
		// TODO Auto-generated method stub
		return controller;
	}
}