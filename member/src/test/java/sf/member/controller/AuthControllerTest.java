package sf.member.controller;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import sf.core.entity.TuMember;
import sf.core.mapper.TuMemberMapper;
import sf.core.service.KeyValueService;
import sf.core.service.SmsService;

/**
 * Created by ligen on 16-3-3.
 */

public class AuthControllerTest extends AbstractControllerTest<AuthController> {

	@Mock
	KeyValueService keyValueService;

	@Mock
	SmsService smsService;

	@Mock
	TuMemberMapper memberMapper;


	@InjectMocks
	AuthController controller;

	@Override
	protected AuthController getInjected() {
		return controller;
	}

	@Test
	public void test_login() throws Exception {
		int uid = 8888;
		Long mobile = 12345678901L;
		TuMember mockedMember = new TuMember();
		mockedMember.setId(uid);
		mockedMember.setMobile(mobile);
		mockito.when(memberMapper.selectOne(mockito.any(TuMember.class))).thenReturn(mockedMember);
		mockMvc.perform(MockMvcRequestBuilders.post("/auth/login").param("mobile", Long.toString(mobile))
				.param("password", "abc")).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void test_passcode() throws Exception {
		String mobile = "18801904960";
		mockito.when(keyValueService.hasKey(mobile)).thenReturn(false);
		mockMvc.perform(MockMvcRequestBuilders.post("/auth/passcode").param("mobile", mobile))
				.andExpect(MockMvcResultMatchers.status().isOk());

		mockito.verify(smsService).sendVerificationCode(mockito.anyString(), mockito.anyString());
	}
}
