package sf.member.controller;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import sf.core.entity.TfMessage;
import sf.core.entity.TuMember;
import sf.core.mapper.TfMessageMapper;
import sf.core.mapper.TfOrderMapper;
import sf.core.mapper.TuMemberMapper;
import sf.core.service.AuthService;
import sf.core.service.KeyValueService;
import sf.core.service.StorageService;
import sf.member.vo.Profile;
import tk.mybatis.mapper.entity.Config;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.mapperhelper.EntityHelper;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ligen on 16-3-3.
 */
public class MineControllerTest extends AbstractControllerTest<MineController>{

    @Mock
    TuMemberMapper memberMapper;
    @Mock
    KeyValueService keyValueService;
    @Mock
    TfOrderMapper orderMapper;
    @Mock
    AuthService authService;
    @Mock
    StorageService storageService;
    @Mock
    TfMessageMapper messageMapper;

    @InjectMocks MineController controller;

    @Override
    protected MineController getInjected() {
        return controller;
    }

    @Test
    public void test_check_mobile() throws Exception {

        TuMember mockMember = new TuMember();
        mockMember.setMobile((long) 123);
        mockito.when(memberMapper.selectByPrimaryKey(mockito.any(TuMember.class)))
                .thenReturn(mockMember);

        mockMvc.perform(MockMvcRequestBuilders.post("/me/mobile/check")
                        .param("mobile", "123")
                        .param("passcode", "456")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_set_password() throws Exception {
        TuMember mockMember = new TuMember();
        mockMember.setMobile((long) 123);
        mockito.when(memberMapper.selectByPrimaryKey(mockito.any(TuMember.class)))
                .thenReturn(mockMember);

        mockMvc.perform(MockMvcRequestBuilders.post("/me/password")
                        .param("uid", "1")
                        .param("mobile", "123")
                        .param("password", "abc")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_get_orders() throws Exception {


        mockMvc.perform(MockMvcRequestBuilders.post("/me/orders")
                        .param("uid", "1")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void test_get_profile() throws Exception{

        mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(1);
        mockito.when(memberMapper.selectByPrimaryKey(1)).thenReturn(new TuMember());

        mockMvc.perform(MockMvcRequestBuilders.get("/me/profile")).andExpect(MockMvcResultMatchers.status().isOk())
        .andDo(MockMvcResultHandlers.log());
    }

    @Test
    public void test_set_profile() throws Exception{

        mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(1);
        mockito.when(memberMapper.selectByPrimaryKey(1)).thenReturn(new TuMember());

        Profile profile = new Profile();
//        profile.setGender((byte) 0);
        profile.setName("ligen");
        profile.setHeight((short) 178);
//        profile.setBirthday("1996/3/5");

        mockMvc.perform(MockMvcRequestBuilders.post("/me/profile")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(gson.toJson(profile))
        ).andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.log());
    }

    @Test
    public void test_get_upload_token() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.get("/me/upload"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test_get_wallet() throws Exception {

        mockito.when(authService.getSubject(mockito.any(HttpServletRequest.class))).thenReturn(1);
        mockito.when(memberMapper.selectByPrimaryKey(1)).thenReturn(new TuMember());

        mockMvc.perform(MockMvcRequestBuilders.get("/me/wallet"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test_suggest() throws Exception {

        mockMvc.perform(MockMvcRequestBuilders.post("/me/suggest")
            .param("content", "good job")
        ).andExpect(MockMvcResultMatchers.status().isOk())
            .andDo(MockMvcResultHandlers.print());
    }

    @Test
    public void test_get_messages() throws Exception{

        EntityHelper.initEntityNameMap(TfMessage.class, new Config());

        long timestamp = System.currentTimeMillis() - 10;

        mockMvc.perform(MockMvcRequestBuilders.get("/me/messages")
            .param("timestamp", Long.toString(timestamp))
        ).andExpect(MockMvcResultMatchers.status().isOk());

        mockito.verify(messageMapper, mockito.times(2)).selectByExample(mockito.any(Example.class));

    }
}
