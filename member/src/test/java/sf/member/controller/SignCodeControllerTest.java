package sf.member.controller;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import sf.core.mapper.TfCouponMapper;
import sf.core.mapper.TfLessonMapper;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TfSignMapper;
import sf.core.mapper.TuMemberMapper;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.purchase.PrivateReceiptProcesor;
import sf.member.purchase.TrainReceiptProcesor;

/**
 * Created by wb on 2016/3/8.
 */
public class SignCodeControllerTest extends AbstractControllerTest<SignCodeController> {

	@InjectMocks
	private SignCodeController controller;

	@Mock
	private PrivateReceiptProcesor privateReceiptProcesor;

	@Mock
	private TrainReceiptProcesor trainReceiptProcesor;

	@Mock
	private MemmberPackageMapper memmberPackageMapper;

	@Mock
	private TfPackageMapper packageMapper;

	@Mock
	private TfLessonMapper lessonMapper;

	@Mock
	private TfReceiptMapper receiptMapper;

	@Mock
	private TfCouponMapper couponMapper;

	@Mock
	private TuMemberMapper memberMapper;

	@Mock
	private TfSignMapper signMapper;

	@Test
	public void testQueryReceipt() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/scan/list").param("gymId", "1").param("memberId", "1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testQueryReceiptById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/scan/list/1")).andDo(MockMvcResultHandlers.print())
				.andExpect(MockMvcResultMatchers.status().isOk());
	}

	@Test
	public void testSign() throws Exception {

		System.out.println(mockMvc + "----------");
		mockMvc.perform(MockMvcRequestBuilders.post("/scan/sign").param("signId", "1").param("memberId", "1"))
				.andDo(MockMvcResultHandlers.print()).andReturn();
	}

	@Override
	protected SignCodeController getInjected() {
		return controller;
	}
}