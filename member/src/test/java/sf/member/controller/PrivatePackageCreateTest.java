package sf.member.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import sf.core.mapper.TfCouponMapper;
import sf.core.mapper.TfLessonMapper;
import sf.core.mapper.TfPackageMapper;
import sf.core.mapper.TfReceiptMapper;
import sf.core.mapper.TfSignMapper;
import sf.core.mapper.TuMemberMapper;
import sf.member.mapper.MemmberPackageMapper;
import sf.member.purchase.PrivateReceiptProcesor;
import sf.member.purchase.TrainReceiptProcesor;

/**
 * Created by wb on 2016/3/7.
 */
/*
 * @RunWith(SpringJUnit4ClassRunner.class)
 * 
 * @WebAppConfiguration
 * 
 * @ContextConfiguration(locations = {"classpath:spring-context.xml"})
 */
public class PrivatePackageCreateTest extends AbstractControllerTest<ReceiptController> {

	@InjectMocks
	private ReceiptController controller;

	@Mock
	// @InjectMocks
	private PrivateReceiptProcesor privateReceiptProcesor;

	@Mock
	private TrainReceiptProcesor trainReceiptProcesor;

	@Mock
	private MemmberPackageMapper memmberPackageMapper;

	@Mock
	private TfPackageMapper packageMapper;

	@Mock
	private TfLessonMapper lessonMapper;

	@Mock
	private TfReceiptMapper receiptMapper;

	@Mock
	private TfCouponMapper couponMapper;

	@Mock
	private TuMemberMapper memberMapper;

	@Mock
	private TfSignMapper signMapper;

	protected Mockito mockito = new Mockito();

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void test_create() throws Exception {

		Integer packageId = 1222;
		Integer couponId = 1222;
		Integer memberId = 1222;
		MvcResult result1 = mockMvc
				.perform(MockMvcRequestBuilders.post("/receipt/create").param("classType", "1").param("packageId", "1")
						.param("couponId", "1").param("memberId", "1"))
				.andDo(MockMvcResultHandlers.print()).andReturn();
	}

	@Override
	protected ReceiptController getInjected() {
		return controller;
	}
}
